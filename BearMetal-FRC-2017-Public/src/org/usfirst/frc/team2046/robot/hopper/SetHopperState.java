package org.usfirst.frc.team2046.robot.hopper;

import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;

import edu.wpi.first.wpilibj.command.Command;

public class SetHopperState extends Command implements TogglableCommand {

	private final Hopper.State hopperState;
	private final Hopper hopper;

	public SetHopperState(Hopper.State hopperState) {
		this.hopperState = hopperState;
		this.hopper = Hopper.getInstance();

		requires(hopper);
	}

	@Override
	protected void initialize() {
		hopper.logDebug("Turning hopper " + ((hopperState == Hopper.State.ON) ? "on." : "off."));

		hopper.setHopperState(hopperState);
	}

	@Override
	protected boolean isFinished() {
		return true;
	}

	@Override
	public boolean isToggled() {
		return hopper.getState() == hopperState;
	}

	@Override
	public Command toCommand() {
		return this;
	}
}
