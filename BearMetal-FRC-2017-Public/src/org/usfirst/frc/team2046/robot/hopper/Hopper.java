/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */package org.usfirst.frc.team2046.robot.hopper;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.usfirst.frc.team2046.robot.util.BearSubsystem;

import com.ctre.CANTalon;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class Hopper extends BearSubsystem {
	private final Logger logger = Logger.getLogger(Hopper.class.getName());
	
	public enum State{
		OFF,
		REVERSED,
		ON;
	}
	
	private final CANTalon queueMotor;
	private final CANTalon thumperMotor;
	
	private static Hopper singleton = null;
	private double hopperPower = 1.0;
	private double thumperPower = 1.0;
	private State hopperState;
	
	public static Hopper getInstance() {
		if (singleton == null) {
			singleton = new Hopper();
		}
		return singleton;
	}
	
	private Hopper() {
		super(Hopper.class);
		
		CANTalon queueMotor = null;
		CANTalon thumperMotor = null;
				
		try {
			queueMotor = new CANTalon(RobotMap.QUEUER_MOTOR);
			queueMotor.changeControlMode(TalonControlMode.PercentVbus);
			queueMotor.setCurrentLimit(30);
			queueMotor.setInverted(true);
			queueMotor.EnableCurrentLimit(true);
			queueMotor.enable();
			
			thumperMotor = new CANTalon(RobotMap.THUMPER_MOTOR);
			thumperMotor.changeControlMode(TalonControlMode.PercentVbus);
			thumperMotor.enable();
		}
		catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}
		
		this.queueMotor = queueMotor;
		this.thumperMotor = thumperMotor;
	}
	
	public void setHopperState(State hopperState){
		synchronized(this){
			switch(hopperState){
			case ON:
				logger.log(Level.INFO, "Setting hopper state to on.");
				setQueueMotor(hopperPower);
				setThumperMotor(thumperPower);
				break;
			case REVERSED:
				logger.log(Level.INFO, "Setting hopper state to reversed.");
				setQueueMotor(-hopperPower);
				setThumperMotor(thumperPower);
				break;
			case OFF:
			default:
				logger.log(Level.INFO, "Setting hopper state to off.");
				setQueueMotor(0.0);
				setThumperMotor(0.0);
				break;
			}
			this.hopperState = hopperState;
		}
	}
	
	public State getState(){
		return hopperState;
	}
		
	private void setQueueMotor(double percentVoltage) {
		logger.log(Level.INFO, "Setting queue motor to: " + percentVoltage + "%V");
		queueMotor.set(percentVoltage);
	}
	
	private void setThumperMotor(double percentVoltage){
		logger.log(Level.INFO, "Setting thumper motor to: " + percentVoltage + "%V");
		thumperMotor.set(percentVoltage);
	}

    public void initDefaultCommand() {
        setDefaultCommand(null);
    }

	@Override
	public void initSubsystem() {
		SmartDashboard.putData("Hopper", new Command() {

			@Override
			protected boolean isFinished() {
				toggle();
				return true;
			}
			
			private void toggle() {
				switch(hopperState){
				case OFF:
					setHopperState(State.ON);
					break;
				case ON:
					setHopperState(State.REVERSED);
					break;
				case REVERSED:
				default:
					setHopperState(State.OFF);
					break;
				}
			}
			
		});
		
		hopperState = State.OFF;
	}

	@Override
	public void update() {
		SmartDashboard.putNumber("Queue Motor", queueMotor.get());
	}

	@Override
	protected void runningUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void debugUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void startDebug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stopDebug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reset() {
		setHopperState(State.OFF);
	}
}

