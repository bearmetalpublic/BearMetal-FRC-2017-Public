package org.usfirst.frc.team2046.robot;

import java.util.logging.Level;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class StartRumbleCommand extends Command {
	private static final Logger logger = Logger.getLogger(StartRumbleCommand.class.getSimpleName());
	
	private final OI.Controller[] controllers;
	private final long ms;

    public StartRumbleCommand(long ms, OI.Controller...controllers) {
    	this.controllers = controllers;
    	this.ms = ms;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	for(OI.Controller controller : controllers){
    		if (controller == OI.Controller.DRIVER) {
        		OI.getInstance().startDriverLeftRumble(ms);
        		OI.getInstance().startDriverRightRumble(ms);
        	}
        	else if (controller == OI.Controller.MANIPULATOR) {
        		OI.getInstance().startManipulatorLeftRumble(ms);
        		OI.getInstance().startManipulatorRightRumble(ms);
        	}
        	else {
        		logger.log(Level.WARNING, "Invalid controller.");
        	}
    	}
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
