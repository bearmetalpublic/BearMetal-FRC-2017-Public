package org.usfirst.frc.team2046.robot.vision;

import edu.wpi.first.wpilibj.PWM.PeriodMultiplier;
import edu.wpi.first.wpilibj.SafePWM;

public class LEDRing {
	private static final int PWM_LOADING = 0;
	private static final int PWM_RAINBOW = 1000;
	private static final int PWM_GOLD = 1500;
	
	private final SafePWM output;
	
	public LEDRing(int PWMPort) {
		output = new SafePWM(PWMPort);
		output.setSafetyEnabled(false);
		output.setPeriodMultiplier(PeriodMultiplier.k4X);
	}
	
	public void set(int value) {
		output.setRaw(value);
	}
	
	public int get() {
		return output.getRaw();
	}
	
	public void setLoading() {
		set(PWM_LOADING);
	}
	
	public void setRainbow() {
		set(PWM_RAINBOW);
	}
	
	public void setGold() {
		set(PWM_GOLD);
	}
}
