/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.vision;

public class RobotMap {
	public static final int CAMERA_FORWARDS = 1; // Camera
	public static final int CAMERA_BACKWARDS = 0; // Camera
	public static final int LED_FRONT = 0; // PWM
	public static final int LED_BACK = 1; // PWM
	public static final int LED_FRONT_BIG = 0; // DIO
	
	public static final double CAMERA_FOV_X = Math.toRadians(60.0);
	public static final double CAMERA_FOV_Y = Math.toRadians(34.0);
	
	public static final double CAMERA_GEAR_HEIGHT = 0.55; // meters from ground to center of camera
	public static final double CAMERA_GEAR_ANGLE = Math.toRadians(-20.0); // 20 degrees, in radians from horizonal
	public static final double TARGET_GEAR_HEIGHT = 0.337; // meters from ground to center of vision target by the gear spring
	public static final double CAMERA_GEAR_OFFSET = 0.254; // meters from the center X of the robot to the camera X

	public static final double CAMERA_BOILER_HEIGHT = 0.6; // meters from ground to center of camera
	public static final double CAMERA_BOILER_ANGLE = Math.toRadians(25.0); // 25 degrees, in radians from horizonal
	public static final double TARGET_BOILER_HEIGHT = 2.1082; // meters from ground to center of vision target area
	public static final double CAMERA_BOILER_OFFSET = 1 * 0.0254; // meters from the center X of the robot to the camera X
	
	public static final double BOILER_MAX_CONTOURS = 2;
	public static final double BOILER_MIN_CONTOURS = 2;
	public static final double GEAR_MAX_CONTOURS = 3;
	public static final double GEAR_MIN_CONTOURS = 2;
	
	public static final double BOILER_MAX_DISTANCE = 5;
	public static final double BOILER_MIN_DISTANCE = 0.5;
	public static final double GEAR_MAX_DISTANCE = 2;
	public static final double GEAR_MIN_DISTANCE = 0;
}
