package org.usfirst.frc.team2046.robot.vision;

import edu.wpi.first.wpilibj.command.Command;

@Deprecated
public class CalibrateRobotStateCommand extends Command {
	
//	private static final Logger logger = Logger.getLogger(CalibrateRobotStateCommand.class.getSimpleName());
//	private final double targetBaseX;
//	private final double targetBaseY;
	
    public CalibrateRobotStateCommand(double targetBaseX, double targetBaseY) {
//    	this.targetBaseX = targetBaseX;
//    	this.targetBaseY = targetBaseY;
    }

    // Called just before this Command runs the first time
    protected void initialize() {
//  TODO  	if (!tracker.canSeeTarget()) {
//    		logger.log(Level.WARNING, "Back camera cannot see target!");
//    		return;
//    	}
//    	Pose2D pose = new Pose2D(0, 0, 0);
//    	RobotState.getInstance().getPose(pose);
//    	
//    	//pose.heading -= tracker.getDeltaYaw();
//    	double newx = (tracker.getDistance() + tracker.getTrackerData().getCameraOffset()) * Math.cos(pose.heading + tracker.getDeltaYaw()) + targetBaseX;
//    	double newy = (tracker.getDistance() + tracker.getTrackerData().getCameraOffset()) * Math.sin(pose.heading + tracker.getDeltaYaw()) + targetBaseY;
//    	
//    	logger.log(Level.INFO, "Corrected pose position by (" + (newx - pose.x) + ", " + (newy - pose.y) + ") and heading delta " + (-tracker.getDeltaYaw()) + " radians.");
//    	logger.log(Level.INFO, "Back camera distance = " + tracker.getDistance() + " meters.");
    	//logger.log(Level.INFO, "Heading is " + pose.heading + ", cos(heading) is " + Math.cos(pose.heading) + ", x length is " + (tracker.getDistance() + tracker.getTrackerData().getCameraOffset()));
    	
//    	Scheduler.getInstance().add(new RobotState.ResetRobotState(newx, newy, pose.heading - tracker.getDeltaYaw()));
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
