/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.usfirst.frc.team2046.robot.autonomous.Autonomous;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.chassis.Chassis;
import org.usfirst.frc.team2046.robot.climber.Climber;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.gear.GearManipulator;
import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.shooter.Shooter;
import org.usfirst.frc.team2046.robot.util.RobotLogger;
import org.usfirst.frc.team2046.robot.util.SubsystemIF;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends IterativeRobot {
	
	private static final Logger logger = Logger.getLogger(Robot.class.getSimpleName());
	
	private Autonomous autonomous;
	
	private final List<SubsystemIF> subsystems = new ArrayList<>();
	
	public Robot() {
		RobotLogger.initialize();

		System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tl:%1$tM:%1$tS.%1$tL %4$s %2$s: %5$s - %n");
				
		try {
			subsystems.add(OI.getInstance());
			subsystems.add(Hopper.getInstance());
			subsystems.add(autonomous = Autonomous.getInstance());
			subsystems.add(Chassis.getInstance());
			subsystems.add(Climber.getInstance());
			subsystems.add(RobotState.getInstance());
			subsystems.add(Collector.getInstance());
			subsystems.add(GearManipulator.getInstance());
			subsystems.add(Shooter.getInstance());
			subsystems.add(Vision.getInstance());
			subsystems.add(FieldData.getInstance());

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed creating Robot", e);
		}
	}
	
    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit() {

    	logger.log(Level.INFO, "robotInit");

    	try {    		
    		// initialize sub-systems
    		for(SubsystemIF subsystem : subsystems) {
    			subsystem.init();
    		}
     	} catch (Exception e) {
    		logger.log(Level.SEVERE, "Failed to init Robot", e);
    	}
    }
	
    @Override
	public void robotPeriodic() {
		for(SubsystemIF subsystem : subsystems) {
			subsystem.update();
		}
		
		SmartDashboard.putNumber("Total Memory", Runtime.getRuntime().totalMemory());
		SmartDashboard.putNumber("Free Memory", Runtime.getRuntime().freeMemory());
	}

    @Override
	public void disabledInit(){
    	logger.log(Level.INFO, "disabled");
    	for(SubsystemIF subsystem : subsystems) {
			subsystem.reset();
		}
    }
	
    @Override
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
	}

    @Override
    public void autonomousInit() {
    	logger.log(Level.INFO, "autonomous started");
    	autonomous.start();
     }

    @Override
    public void autonomousPeriodic() {
        Scheduler.getInstance().run();
    }

    @Override
    public void teleopInit() {
    	logger.log(Level.INFO, "tele-op started");
    	if (autonomous != null) {
    		autonomous.end();
    	}
    }

    @Override
    public void teleopPeriodic() {
        Scheduler.getInstance().run();
    }
    
    @Override
    public void testInit() {
    	logger.log(Level.INFO, "test started");
    }
    
    @Override
    public void testPeriodic() {
        LiveWindow.run();
    }
    
    public static StartingSide getSide(){
		DriverStation.Alliance alliance = DriverStation.getInstance().getAlliance();
		
		StartingSide toReturn = null;
		switch(alliance){
		case Blue:
			toReturn = StartingSide.BlueAlliance;
			break;
		case Red:
			toReturn = StartingSide.RedAlliance;
			break;
		case Invalid:
		default:
			logger.log(Level.SEVERE, "Invalid alliance detected by driver station!");
			break;
		}
		
		return toReturn;
	}
    
    public static boolean isAutonomousActive(){
    	return edu.wpi.first.wpilibj.RobotState.isAutonomous();
    }
    
    public static boolean isTeleActive(){
    	return edu.wpi.first.wpilibj.RobotState.isOperatorControl();
    } 
}
