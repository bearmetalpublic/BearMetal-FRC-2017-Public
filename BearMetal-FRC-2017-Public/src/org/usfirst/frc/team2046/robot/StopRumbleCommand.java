package org.usfirst.frc.team2046.robot;

import java.util.logging.Level;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class StopRumbleCommand extends Command {
	private static final Logger logger = Logger.getLogger(StopRumbleCommand.class.getSimpleName());
	
	private final OI.Controller[] controllers;
    public StopRumbleCommand(OI.Controller...controllers) {
    	this.controllers = controllers;
    }

    @Override
    protected void initialize() {
    	for(OI.Controller controller : controllers){
    		if (controller == OI.Controller.DRIVER) {
        		OI.getInstance().stopDriverLeftRumble();
        		OI.getInstance().stopDriverRightRumble();
        	}
        	else if (controller == OI.Controller.MANIPULATOR) {
        		OI.getInstance().stopManipulatorLeftRumble();
        		OI.getInstance().stopManipulatorRightRumble();
        	}
        	else {
        		logger.log(Level.WARNING, "Invalid controller.");
        	}
    	}
    }
    
    @Override
    protected boolean isFinished() {
        return true;
    }
}
