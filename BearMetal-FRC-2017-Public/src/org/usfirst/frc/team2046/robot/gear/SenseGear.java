package org.usfirst.frc.team2046.robot.gear;

import java.util.logging.Level;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.command.Command;

public class SenseGear extends Command {

	private static final Logger logger = Logger.getLogger(SenseGear.class.getSimpleName());

	private final GearManipulator gearManipulator = GearManipulator.getInstance();
	
	@Override
	protected boolean isFinished() {
		boolean gearFound = gearManipulator.isGearVisible();
		if (gearFound) {
			logger.log(Level.INFO, "Gear is sensed!");
		}
		return gearFound;
	}
}
