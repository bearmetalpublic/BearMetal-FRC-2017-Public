package org.usfirst.frc.team2046.robot.gear;

import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;

public class PlaceGearCommand extends CommandGroup {
	public PlaceGearCommand(){
		addSequential(new DeployGobbler(true, false));
		addSequential(new GobblerIntake(GobblerIntake.State.REVERSED));
		addSequential(new BackAwayCommand());
		addSequential(new DeployGobbler(false, false));
		addSequential(new GobblerIntake(GobblerIntake.State.OFF));
	}
	
	private static class BackAwayCommand extends Command{
		private static final double BACKAWAY_TIMEOUT = 0.25;
		private static final double BACKAWAY_SPEED = 1.0;
		
		private final Chassis chassis;
		public BackAwayCommand(){
			super(BACKAWAY_TIMEOUT);
			
			chassis = Chassis.getInstance();
			requires(chassis);
		}
		
		@Override
		protected void initialize(){
			chassis.setSpeed(new RobotSpeed(BACKAWAY_SPEED, 0.0), Double.MAX_VALUE);
		}
		
		@Override
		protected boolean isFinished() {
			return isTimedOut();
		}
		
		@Override
		protected void end(){
			chassis.stopController();
		}
		
		@Override
		protected void interrupted(){
			end();
		}
	}
}
