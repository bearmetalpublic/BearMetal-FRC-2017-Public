package org.usfirst.frc.team2046.robot.gear;

import java.util.logging.Level;

import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;

import edu.wpi.first.wpilibj.command.Command;

public class DeployGobbler extends Command implements TogglableCommand {
	private static final double GEAR_MANIPULATOR_TIMEOUT = 1.0;
	private final GearManipulator gearManipulator;
	private final boolean deploy;
	private final boolean blockUntilDone;
	
	public DeployGobbler(boolean deploy, boolean blockUntilDone){
		setTimeout(GEAR_MANIPULATOR_TIMEOUT);
		gearManipulator = GearManipulator.getInstance();
		this.deploy = deploy;
		this.blockUntilDone = blockUntilDone;
		
		requires(gearManipulator);
	}
	
	@Override
	protected void initialize(){
		gearManipulator.deployGobbler(deploy);
		gearManipulator.logDebug(deploy ? "Deploy Gobbler" : "Retract Gobbler");
	}
	
	@Override
	protected boolean isFinished() {
		return isTimedOut() || gearManipulator.isGobblerPositionOnTarget() || !blockUntilDone;
	}
	
	@Override
	protected void end(){
		if (this.isTimedOut()) {
			gearManipulator.logDebug(Level.WARNING, deploy ? "Deploy timedout" : "Retract timedout");
			gearManipulator.logDebug("Current Gobbler Position: " + GearManipulator.getInstance().getDeployAngle());
		} else {
			gearManipulator.logDebug(deploy ? "Deployed Gobbler" : "Retracted Gobbler");		
		}
	}
	
	@Override
	protected void interrupted(){
		end();
	}

	@Override
	public boolean isToggled() {
		return gearManipulator.isGobblerPositionOnTarget();
	}

	@Override
	public Command toCommand() {
		return this;
	}

}
