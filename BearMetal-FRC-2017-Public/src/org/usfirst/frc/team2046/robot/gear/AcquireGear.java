package org.usfirst.frc.team2046.robot.gear;

import org.usfirst.frc.team2046.robot.util.TogglableCommandGroup;

public class AcquireGear extends TogglableCommandGroup{
	
	public AcquireGear() {
		addSequential(new GobblerIntake(GobblerIntake.State.ON));
		addTogglableSequential(new DeployGobbler(true, true));
		addSequential(new SenseGear());
		addSequential(new DeployGobbler(false, false));
		addSequential(new GobblerIntake(GobblerIntake.State.OFF));
	}
}
