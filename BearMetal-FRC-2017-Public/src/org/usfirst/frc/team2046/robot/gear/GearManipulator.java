package org.usfirst.frc.team2046.robot.gear;

import java.util.logging.Level;

import org.usfirst.frc.team2046.robot.OI;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;
import org.usfirst.frc.team2046.robot.util.RobotProperties;
import org.usfirst.frc.team2046.robot.util.XboxController;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class GearManipulator extends BearSubsystem {
	private static final GearManipulator instance = new GearManipulator();
	
	private static final double GEAR_LIGHT_SENSOR_THRESHOLD = 4.5; // Voltage, note that this is compared against a value that is oversampled by 2 bits.
	private static final double GEAR_SONAR_SENSOR_THRESHOLD = 1.0;
	private static final double DEPLOY_POSITION_TOLERANCE = 3.0 / 360.0;
	private static final double DEPLOY_SPEED_TOLERANCE = 5.0;
	
	private final CANTalon deployMotor;
	private final CANTalon rollerMotor;
	private final AnalogInput gearLightSensor;
	private final AnalogInput gearSonarSensor;
	private final RobotProperties properties;
	
	protected static double GEAR_ARM_UP_P = 4.0;
	protected static double GEAR_ARM_UP_I = 0.0;
	protected static double GEAR_ARM_UP_D = 0.0;
	protected static double GEAR_ARM_DOWN_P = 1.0;
	protected static double GEAR_ARM_DOWN_I = 0.0;
	protected static double GEAR_ARM_DOWN_D = 0.0;
	
	private static final double GEAR_ARM_DOWN = -0.25;
	private static final double GEAR_ARM_DOWN_PERSISTANT = -0.05;
	private static final double GEAR_ARM_UP = 0.4;
	private static final double GEAR_ARM_UP_PERSISTANT = 0.15;
	private static final long GEAR_ARM_PERSISTANCE_SWITCH_TIME = 1 * 1000;
	
	private volatile long lastCommandedTime = System.currentTimeMillis();
	private volatile boolean isDeployed, persistantValueSet, overrided;
	public static final GearManipulator getInstance() {
		return instance;
	}

	private GearManipulator() {
		super(GearManipulator.class);
		
		isDeployed = false;
		overrided = false;
		
		CANTalon deployMotor = null;
		CANTalon rollerMotor = null;
		AnalogInput gearLightSensor = null;
		AnalogInput gearSonarSensor = null;
		RobotProperties properties = null;
		try {
			rollerMotor = new CANTalon(RobotMap.GEAR_MANIPULATOR_ROLLER_MOTOR_CHANNEL);
			rollerMotor.changeControlMode(TalonControlMode.PercentVbus);
			rollerMotor.setCurrentLimit(10);
			rollerMotor.EnableCurrentLimit(true);
			rollerMotor.enable();
			
			deployMotor = new CANTalon(RobotMap.GEAR_MANIPULATOR_DEPLOY_MOTOR_CHANNEL);
			deployMotor.changeControlMode(TalonControlMode.PercentVbus);
			deployMotor.setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Absolute);
			deployMotor.reverseOutput(true);
			deployMotor.setCurrentLimit(4);
			deployMotor.EnableCurrentLimit(true);
			deployMotor.enableBrakeMode(true);
			deployMotor.enable();
			
			gearSonarSensor = new AnalogInput(RobotMap.GEAR_SONAR_SENSOR);
			gearSonarSensor.setOversampleBits(2);
			
			gearLightSensor = new AnalogInput(RobotMap.GEAR_LIGHT_SENSOR);
			gearLightSensor.setOversampleBits(2);
			
			properties = new RobotProperties(GearManipulator.class.getSimpleName());
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}
		this.deployMotor = deployMotor; 
		this.rollerMotor = rollerMotor;
		this.gearLightSensor = gearLightSensor;
		this.gearSonarSensor = gearSonarSensor;
		this.properties = properties;
	}

	public void initSubsystem() {
		properties.load();
		deployGobbler(false);
	}
	
	public void setRollerMotor(double value){
		logger.log(Level.INFO, "Setting roller motor to: " + value + "%V");
		
		rollerMotor.set(value);
	}
	
	public void toggleGobOverride(){
		if(Robot.isTeleActive()){
			overrided = !overrided;
			deployGobbler(isDeployed);
		}
	}
	
	public double getDeployAngle(){
		return deployMotor.getPulseWidthPosition() / 4096.0;
	}
	
	public double getDeploySetpoint(){
		return Double.parseDouble(properties.getProperty("deploySetpoint"));
	}
	public void setDeploySetpoint(double setpoint){
		properties.setProperty("deploySetpoint", Double.toString(setpoint));
		properties.save();
	}
	
	public double getRetractedSetpoint(){
		return Double.parseDouble(properties.getProperty("retractSetpoint"));
	}
	public void setRetractedSetpoint(double setpoint){
		properties.setProperty("retractSetpoint", Double.toString(setpoint));
		properties.save();
	}
	
	public void deployGobbler(boolean deploy){
		logger.log(Level.INFO, deploy ? "Deploying gobbler." : "Retracting gobbler.");
		isDeployed = deploy;
		
//		if(gearGobOverride){
//			deployMotor.changeControlMode(TalonControlMode.PercentVbus);
//			if(deploy){
//				deployMotor.set(-0.25);
//			}
//			else{
//				deployMotor.set(0.4);
//			}
//		}
//		else{
//			deployMotor.changeControlMode(TalonControlMode.Position);
//			deployMotor.set((deploy ? getDeploySetpoint() : getRetractedSetpoint()));
//			deployMotor.setP(deploy ? GEAR_ARM_DOWN_P : GEAR_ARM_UP_P);
//			deployMotor.setI(deploy ? GEAR_ARM_DOWN_I : GEAR_ARM_UP_I);
//			deployMotor.setD(deploy ? GEAR_ARM_DOWN_D : GEAR_ARM_UP_D);
//			if (!deployMotor.isControlEnabled()) {
//				deployMotor.enableControl();
//			}	
//		}
		
		deployGobbler(deploy, false);
	}
	
	public void deployGobbler(boolean deploy, boolean isPersistant){
		if(deploy){
			if(isPersistant){
				logger.log(Level.INFO, "Setting Gear Arm Down Persistant");
				deployMotor.set(GEAR_ARM_DOWN_PERSISTANT);
			}
			else{
				logger.log(Level.INFO, "Setting Gear Arm Down Non-Persistant");
				deployMotor.set(GEAR_ARM_DOWN);
				lastCommandedTime = System.currentTimeMillis();
			}
		}
		else{
			if(isPersistant){
				logger.log(Level.INFO, "Setting Gear Arm Up Persistant");
				deployMotor.set(GEAR_ARM_UP_PERSISTANT);
			}
			else{
				logger.log(Level.INFO, "Setting Gear Arm Up Non-Persistant");
				deployMotor.set(GEAR_ARM_UP);
				lastCommandedTime = System.currentTimeMillis();
			}
		}
		persistantValueSet = isPersistant;
	}
	
	public double getSonarSensorVoltage() {
		return gearSonarSensor.getVoltage();
	}
	
	public double getLightSensorVoltage(){
		return gearLightSensor.getVoltage();
	}
	
	public boolean isGearVisibleToSonar(){
		return getSonarSensorVoltage() < GEAR_SONAR_SENSOR_THRESHOLD;
	}
	
	public boolean isGearVisibleToLight(){
		return getLightSensorVoltage() < GEAR_LIGHT_SENSOR_THRESHOLD;
	}
	
	public boolean isGearVisible() {
		return isGearVisibleToSonar();
	}

	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new Command() {
			double pastTriggerValue = 0.0;
			double currentTriggerValue = 0.0;
			final double triggerThreshold = 0.5;
			
			{
				requires(GearManipulator.getInstance());
			}
			
			@Override
			protected void execute() {
				rollerMotor.set(OI.getInstance().manipulatorController.getAxisValue(XboxController.LEFT_TRIGGER_AXIS)
						 - OI.getInstance().manipulatorController.getAxisValue(XboxController.RIGHT_TRIGGER_AXIS));
				
				currentTriggerValue = OI.getInstance().driverController.getAxisValue(XboxController.LEFT_TRIGGER_AXIS);
				if(currentTriggerValue > triggerThreshold && currentTriggerValue > pastTriggerValue){
					Scheduler.getInstance().add(new AcquireGear());
				}
				pastTriggerValue = currentTriggerValue;
			}

			@Override
			protected boolean isFinished() {
				return false;
			}
		});
	}

	public boolean isGobblerPositionOnTarget() {
		return persistantValueSet;
	}
	
	public boolean isDeployed(){
		return isDeployed && isGobblerPositionOnTarget();
	}
	
	public boolean isCollecting(){
		return isDeployed() && rollerMotor.get() > 0;
	}
	
	public boolean isMoving(){
		return Math.abs(deployMotor.getSpeed()) > DEPLOY_SPEED_TOLERANCE;
	}

	private boolean wasCollecting;
	@Override
	protected void runningUpdate() {
		SmartDashboard.putBoolean("Gear Gobbler Deployed", isDeployed());
		SmartDashboard.putBoolean("Gear Gobbler Actually Deployed", persistantValueSet);
		SmartDashboard.putBoolean("Gear Gobbler Overrided", overrided);
		
		if(isCollecting() && !wasCollecting && !Robot.isAutonomousActive()){
			OI.getInstance().startDriverRightRumble();
			OI.getInstance().startDriverLeftRumble();
			OI.getInstance().startManipulatorRightRumble();
			OI.getInstance().startManipulatorLeftRumble();
		}
		else if (wasCollecting && !isCollecting()){
			OI.getInstance().stopDriverRightRumble();
			OI.getInstance().stopDriverLeftRumble();
			OI.getInstance().stopManipulatorRightRumble();
			OI.getInstance().stopManipulatorLeftRumble();
		}
		
		wasCollecting = isCollecting();
		
		if(!isGobblerPositionOnTarget() && !overrided && Robot.isTeleActive() && (lastCommandedTime + GEAR_ARM_PERSISTANCE_SWITCH_TIME) < System.currentTimeMillis()){
			logger.log(Level.INFO, "Switching to persistant pwm. Last Commanded Time = " + lastCommandedTime + " Current Time = " + System.currentTimeMillis());
			deployGobbler(isDeployed, true);
		}
		
		if(isGobblerPositionOnTarget() && isMoving()){
			logger.log(Level.INFO, "Gear gobbler detected to be moving! Switching to nonpersistant pwm.");
			deployGobbler(isDeployed);
		}
	}

	@Override
	protected void debugUpdate() {
		SmartDashboard.putNumber("Gear Gobbler Sensor Voltage", getSonarSensorVoltage());
		SmartDashboard.putNumber("Gear Gobbler Sonar Sensor Voltage", gearSonarSensor.getVoltage());
		SmartDashboard.putNumber("Gear Gobbler Deploy Angle", getDeployAngle());
		SmartDashboard.putNumber("Gear Gobbler Raw Deploy Angle", deployMotor.getPulseWidthPosition());
		SmartDashboard.putNumber("Gear Gobbler Deploy Speed", deployMotor.getSpeed());
	}

	@Override
	protected void startDebug() {
		SmartDashboard.putData("Calibrate Deploy Setpoint", new Command(){			
			{
				this.setRunWhenDisabled(true);
			}
			
			@Override
			protected boolean isFinished() {
				logger.log(Level.INFO, "Calibrating deploy setpoint. Set to: " + getDeployAngle());
				setDeploySetpoint(getDeployAngle());
				return true;
			}
		});
		SmartDashboard.putData("Calibrate Retracted Setpoint", new Command(){			
			{
				this.setRunWhenDisabled(true);
			}
			
			@Override
			protected boolean isFinished() {
				logger.log(Level.INFO, "Calibrating retracted setpoint. Set to: " + getDeployAngle());
				setRetractedSetpoint(getDeployAngle());
				return true;
			}
		});
	}

	@Override
	protected void stopDebug() {
	}

	@Override
	public void reset() {
		deployGobbler(false);
	}
}
