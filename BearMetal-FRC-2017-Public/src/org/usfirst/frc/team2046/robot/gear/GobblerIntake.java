package org.usfirst.frc.team2046.robot.gear;

import java.util.logging.Level;

import edu.wpi.first.wpilibj.command.Command;

public class GobblerIntake extends Command {	
	private static final double INTAKE_SPEED = 0.75;  
	private static final double INTAKE_OFF = 0.0;
	private static final double INTAKE_REVERSED_SPEED = -0.30;
	
	private final State state;
	private final GearManipulator gearManipulator;
	
	public enum State{
		ON(INTAKE_SPEED), OFF(INTAKE_OFF), REVERSED(INTAKE_REVERSED_SPEED);
		
		private final double percentVbus;
		private State(double percentVbus){
			this.percentVbus = percentVbus;
		}
	}
	
	public GobblerIntake(State state) {
		this.state = state;
		gearManipulator = GearManipulator.getInstance();
		
		requires(gearManipulator);
	}
	
	@Override
	protected void initialize(){
		gearManipulator.setRollerMotor(state.percentVbus);
		gearManipulator.logger.log(Level.INFO, state != State.OFF ? "Gobbler Intake On" : "Gobbler Intake Off");
	}

	@Override
	protected boolean isFinished() {
		return true;
	}
}
