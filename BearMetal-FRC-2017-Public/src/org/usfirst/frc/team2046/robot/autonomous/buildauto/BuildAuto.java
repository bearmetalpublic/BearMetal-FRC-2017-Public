package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.opencv.core.Point;
import org.tahomarobotics.robot.path.Path;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.SplinePath;
import org.usfirst.frc.team2046.robot.FieldData;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Scheduler;

public class BuildAuto extends CommandGroup{
	private static final Logger logger = Logger.getLogger(BuildAuto.class.getSimpleName());
	
	private static final double STARTING_POSE_REVERSED_X = 18.25 * 0.0254;
	private static final double STARTING_POSE_NORMAL_X = 16.929 * 0.0254;
	
	private static final double DEFAULT_SPEED = 2.0;
	private static final double FEEDER_GEAR_SPEED = 1.0;
	private static final double CENTER_GEAR_SPEED = 1.0;
	private static final double CENTER_GEAR_SHOOT_SPEED = 1.0;
	private static final double HOPPER_SPEED = 4.0;
	private static final double HOPPER_SHOOT_SPEED = 1.0;
	private static final double BOILER_SHOOT_SPEED = 2.0;
	private static final double HOPPER_PARALLEL_SPEED = 2.0;
	private static final double HOPPER_BUMP_SPEED = 0.5;
	
	private static final Path TO_FEEDER_GEAR = new SplinePath()
			.add(new Waypoint(STARTING_POSE_REVERSED_X, 92.126 * 0.0254, FEEDER_GEAR_SPEED))
			.add(new Waypoint(48.504 * 0.0254, 83.228 * 0.0254, FEEDER_GEAR_SPEED))
			.add(new Waypoint(83.362 * 0.0254, 67.624 * 0.0254, FEEDER_GEAR_SPEED))
			.add(new Waypoint(108.25 * 0.0254, 97.23 * 0.0254, FEEDER_GEAR_SPEED));
	
	private static final Path TO_CENTER_GEAR = new SplinePath()
			.add(new Waypoint(STARTING_POSE_REVERSED_X, 161.890 * 0.0254, CENTER_GEAR_SPEED))
			.add(new Waypoint(90.882 * 0.0254, 161.890 * 0.0254, CENTER_GEAR_SPEED));

	private static final Path CENTER_GEAR_TO_SHOOT = new SplinePath()
			.add(new Waypoint(90.882 * 0.0254, 161.890 * 0.0254, CENTER_GEAR_SHOOT_SPEED))
			.add(new Waypoint(61.024 * 0.0254, 172.992 * 0.0254, CENTER_GEAR_SHOOT_SPEED))
			.add(new Waypoint(42.244 * 0.0254, 194.331 * 0.0254, CENTER_GEAR_SHOOT_SPEED))
			.add(new Waypoint(28.071 * 0.0254, 220.866 * 0.0254, CENTER_GEAR_SHOOT_SPEED));
	
	private static final Path TO_BOILER_GEAR = TO_FEEDER_GEAR
			.getMirrorY(FieldData.Map.WIDTH_OF_FIELD / 2.0);
	
	private static final Path TO_HOPPER = new SplinePath()
			.add(new Waypoint(STARTING_POSE_NORMAL_X, 232.283 * 0.0254, HOPPER_SPEED))
			.add(new Waypoint(67.008 * 0.0254, 239.685 * 0.0254, HOPPER_SPEED))
			.add(new Waypoint(95.276 * 0.0254, 266.575 * 0.0254, HOPPER_SPEED))
			.add(new Waypoint(108.118 * 0.0254, 295.276 * 0.0254, HOPPER_SPEED))
			.add(new Waypoint(105.118 * 0.0254, 307.087 * 0.0254, HOPPER_SPEED));

	private static final Path HOPPER_TO_SHOOT = new SplinePath()
			.add(new Waypoint(105.118 * 0.0254, 307.087 * 0.0254, HOPPER_SHOOT_SPEED))
			.add(new Waypoint(112.42 * 0.0254, 296.50 * 0.0254, HOPPER_SHOOT_SPEED))
			.add(new Waypoint(120.890 * 0.0254, 293.55 * 0.0254, HOPPER_SHOOT_SPEED));
	
	private static final Path BOILER_GEAR_TO_SHOOT = new SplinePath()
			.add(new Waypoint(118.75 * 0.0254, 115.43 * 0.0254, BOILER_SHOOT_SPEED))
			.add(new Waypoint(106.02 * 0.0254, 102.70 * 0.0254, BOILER_SHOOT_SPEED))
			.add(new Waypoint(93.30 * 0.0254, 83.98 * 0.0254, BOILER_SHOOT_SPEED))
			.getMirrorY(FieldData.Map.WIDTH_OF_FIELD / 2.0);
	
	private static final Path TO_HOPPER_PARALLEL = new SplinePath()
			.add(new Waypoint(STARTING_POSE_NORMAL_X, 232.283 * 0.0254, HOPPER_PARALLEL_SPEED))
			.add(new Waypoint(73.677 * 0.0254, 252.661 * 0.0254, HOPPER_PARALLEL_SPEED))
			.add(new Waypoint(102.558 * 0.0254, 287.418 * 0.0254, HOPPER_PARALLEL_SPEED))
			.add(new Waypoint(126.404 * 0.0254, 309.048 * 0.0254, HOPPER_PARALLEL_SPEED))
			.add(new Waypoint(150.404 * 0.0254, 309.048 * 0.0254, HOPPER_PARALLEL_SPEED));
	
	private static final Path TO_HOPPER_BUMP = new SplinePath()
			.add(new Waypoint(STARTING_POSE_REVERSED_X, 54.907 * 0.0254, HOPPER_BUMP_SPEED))
			.add(new Waypoint(43.132 * 0.0254, 48 * 0.0254, HOPPER_BUMP_SPEED))
			.add(new Waypoint(62.588 * 0.0254, 18.398 * 0.0254, HOPPER_BUMP_SPEED))
			.add(new Waypoint(100.561 * 0.0254, 19.398 * 0.0254, HOPPER_BUMP_SPEED))
			.getMirrorY(FieldData.Map.WIDTH_OF_FIELD / 2.0);
		
	public enum AutoPath {
		PATH_TO_FEEDER_GEAR (TO_FEEDER_GEAR),
		PATH_TO_CENTER_GEAR (TO_CENTER_GEAR),
		PATH_TO_BOILER_GEAR (TO_BOILER_GEAR),
		PATH_TO_HOPPER (TO_HOPPER),
		PATH_TO_SHOOT_FROM_HOPPER (HOPPER_TO_SHOOT),
		PATH_TO_SHOOT_FROM_BOILER_GEAR (BOILER_GEAR_TO_SHOOT),
		PATH_TO_SHOOT_FROM_CENTER_GEAR (CENTER_GEAR_TO_SHOOT),
		PATH_TO_HOPPER_PARALLEL (TO_HOPPER_PARALLEL),
		PATH_TO_HOPPER_BUMP (TO_HOPPER_BUMP);
		
		private final Path path;
		private AutoPath(Path path){
			this.path = path;
		}
		
		public Path getPath(StartingSide side){
			return side == StartingSide.BlueAlliance 
					? path : path.getMirrorX(FieldData.Map.LENGTH_OF_FIELD / 2.0);
		}
	}
	
	public static enum Action{
		PLACE_GEAR_ON_CENTER (true),
		PLACE_GEAR_ON_FEEDER (true),
		PLACE_GEAR_ON_BOILER (true),
		PLACE_GEAR_ON_CENTER_AND_SHOOT (true),
		PLACE_GEAR_ON_CENTER_GO_TO_HOPPER_AND_SHOOT (true),
		PLACE_GEAR_ON_BOILER_AND_SHOOT (true),
		GO_TO_HOPPER_AND_SHOOT (false),
		GO_TO_HOPPER_PARALLEL_AND_SHOOT (true),
		GO_TO_FAR_HOPPER_BUMP_AND_SHOOT (true),
		GO_TO_CLOSE_HOPPER_BUMP_AND_SHOOT (true);
		
		private final boolean initialPathReversed;
		private Action(boolean initialPathReversed){
			this.initialPathReversed = initialPathReversed;
		}
	}
	
	public static final Point[] BLUE_FAR_HOPPER_AUTO_ASSIST_POINTS = {new Point(200, 0), new Point(200, 75), new Point(145, 100), new Point(145, 0)};
	public static final Point[] RED_FAR_HOPPER_AUTO_ASSIST_POINTS = {new Point(440, 0), new Point(440, 75), new Point(495, 100), new Point(495, 0)};
	public static final Point[] BLUE_CLOSE_HOPPER_AUTO_ASSIST_POINTS = {new Point(220, 0), new Point(220, 75), new Point(155, 105), new Point(155, 0)};
	public static final Point[] RED_CLOSE_HOPPER_AUTO_ASSIST_POINTS = {new Point(420, 0), new Point(420, 75), new Point(485, 105), new Point(485, 0)};
	
	public BuildAuto(StartingSide side, Action action, double speed){
		addParallel(new ReleaseHopper());
		
		ArrayList<Command> commands = createCommands(side, action, speed);
		for(Command command : commands){
			addSequential(command);
		}
	}
	
	public BuildAuto(StartingSide side, Action action){
		this(side, action, DEFAULT_SPEED);
	}
	
	public static ArrayList<Command> createCommands(StartingSide side, Action action, double speed){
		ArrayList<Command> commands = null;
		try{
			FieldData.getInstance().displayPaths.clear();
			Vision.getInstance().clearAutoSetupAssistance();
			
			logger.log(Level.INFO, "Attempting to build path.");
			commands = new ArrayList<Command>();
			ArrayList<AutoAction> actions = new ArrayList<AutoAction>();
			
			switch(action){
			case GO_TO_HOPPER_AND_SHOOT:
				actions.add(new RetrieveFuelFromHopper(AutoPath.PATH_TO_HOPPER.getPath(side)));
				actions.add(new ShootFuelFromHopper(AutoPath.PATH_TO_SHOOT_FROM_HOPPER.getPath(side), true, true));
				break;
			case GO_TO_HOPPER_PARALLEL_AND_SHOOT:
				actions.add(new ParallelRetrieveFuelFromHopperAndShoot(AutoPath.PATH_TO_HOPPER_PARALLEL.getPath(side), DEFAULT_SPEED));
				break;
			case GO_TO_FAR_HOPPER_BUMP_AND_SHOOT:
				if(side == StartingSide.BlueAlliance)
					Vision.getInstance().addAutoSetupAssistance(BLUE_FAR_HOPPER_AUTO_ASSIST_POINTS);
				else
					Vision.getInstance().addAutoSetupAssistance(RED_FAR_HOPPER_AUTO_ASSIST_POINTS);
				actions.add(new BumpRetrieveFuelFromFarHopperAndShoot(side));
				break;
			case GO_TO_CLOSE_HOPPER_BUMP_AND_SHOOT:
				if(side == StartingSide.BlueAlliance)
					Vision.getInstance().addAutoSetupAssistance(BLUE_CLOSE_HOPPER_AUTO_ASSIST_POINTS);
				else
					Vision.getInstance().addAutoSetupAssistance(RED_CLOSE_HOPPER_AUTO_ASSIST_POINTS);
				actions.add(new BumpRetrieveFuelFromCloseHopperAndShoot(side));
				break;
			case PLACE_GEAR_ON_BOILER:
				actions.add(new PlaceSideGear(AutoPath.PATH_TO_BOILER_GEAR.getPath(side)));
				break;
			case PLACE_GEAR_ON_BOILER_AND_SHOOT:
				actions.add(new PlaceSideGear(AutoPath.PATH_TO_BOILER_GEAR.getPath(side)));
				actions.add(new ShootFuelFromSideGearPeg(AutoPath.PATH_TO_SHOOT_FROM_BOILER_GEAR.getPath(side)));
				break;
			case PLACE_GEAR_ON_CENTER_GO_TO_HOPPER_AND_SHOOT:
				actions.add(new CenterPegHopperShoot());
				break;
			case PLACE_GEAR_ON_CENTER:
				actions.add(new PlaceCenterGear(AutoPath.PATH_TO_CENTER_GEAR.getPath(side)));
				break;
			case PLACE_GEAR_ON_CENTER_AND_SHOOT:
				actions.add(new PlaceCenterGear(AutoPath.PATH_TO_CENTER_GEAR.getPath(side)));
				actions.add(new ShootFuelFromCenterGearPeg(AutoPath.PATH_TO_SHOOT_FROM_CENTER_GEAR.getPath(side)));
				break;
			case PLACE_GEAR_ON_FEEDER:
				actions.add(new PlaceSideGear(AutoPath.PATH_TO_FEEDER_GEAR.getPath(side)));
				break;
			default:
				logger.log(Level.WARNING, "Invalid action.");
				break;
			}
			
			//Resetting the robot pose
			Pose2D startingPose = actions.get(0).getStartingPose(side, action.initialPathReversed);
			commands.add(new RobotState.ResetRobotState(startingPose));
			
			logger.log(Level.INFO, action.toString() + " chosen as autonomous.");
			logger.log(Level.INFO, "Actions size: " + actions.size());
			for(AutoAction act : actions){
				commands.add(act);
				if(act.getPath() != null){
					FieldData.getInstance().displayPaths.add(act.getPath());
					FieldData.getInstance().displayPaths.add(((SplinePath) act.getPath()).finalizeSpline());
				}
			}
			
			Scheduler.getInstance().add(new RobotState.ResetRobotState(startingPose));
			
		} catch (Exception e){
			logger.log(Level.SEVERE, "Failed to build commands!", e);
			e.printStackTrace();
		}
		return commands;
	}
}
