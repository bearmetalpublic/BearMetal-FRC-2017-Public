package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.chassis.DriveByPowerCommand;

public class RetrieveFuelFromHopper extends AutoAction{
	private static final double HOPPER_DELAY = 1.75;
	private static final double DRIVE_POWER = 0.25;
	private static final double DRIVE_PERCENTAGE = 0.5;
	private static final double MAX_ACCELERATION = 2.0;
	
	public RetrieveFuelFromHopper(Path path) {
		super(path);
		
		addSequential(new FollowPathCommand("RetrieveFuelFromHopper", getPath(), false, false, FollowPathCommand.LOOKAHEAD_DISTANCE, FollowPathCommand.NO_TIMEOUT, MAX_ACCELERATION));
		
		double lpower = DRIVE_POWER + (DRIVE_POWER * DRIVE_PERCENTAGE * (Robot.getSide() == StartingSide.BlueAlliance ? -1 : 1));
		double rpower = DRIVE_POWER + (DRIVE_POWER * DRIVE_PERCENTAGE * (Robot.getSide() == StartingSide.BlueAlliance ? 1 : -1));
		addSequential(new DriveByPowerCommand(HOPPER_DELAY, lpower, rpower));
	}
}
