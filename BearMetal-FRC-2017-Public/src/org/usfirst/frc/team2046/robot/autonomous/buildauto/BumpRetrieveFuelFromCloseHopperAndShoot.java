package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.chassis.DriveByPowerCommand;
import org.usfirst.frc.team2046.robot.chassis.DriveForwardCommand;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;

public class BumpRetrieveFuelFromCloseHopperAndShoot extends AutoAction {
	private static final double BUMP_TIME = 0.5;
	private static final double BUMP_POWER = 1.0;
	private static final double BUMP_SMOL_POWER = 0.75;
	
	private static final double TO_CLOSE_HOPPER_DISTANCE = 100.7 * 0.0254;
	private static final double TO_CLOSE_HOPPER_SPEED = 3.0;
	
	private static final double FORWARD_DISTANCE = 20.0 * 0.0254;
	private static final double FORWARD_SPEED = 2.0;
	
	private static final double STARTING_CLOSE_POSITION_X = 22.250 * 0.0254;
	private static final double STARTING_CLOSE_POSITION_Y = 274.750 * 0.0254;
	private static final double STARTING_CLOSE_POSITION_HEADING = Math.toRadians(15.7);
	
	public BumpRetrieveFuelFromCloseHopperAndShoot(StartingSide side) {
		addParallel(new StartShooting.Prep());
		addSequential(new DriveForwardCommand(TO_CLOSE_HOPPER_SPEED, TO_CLOSE_HOPPER_DISTANCE, true));	
		addParallel(new CollectorCommand(Collector.State.SLOW));
		
		double lpower = 0.0;
		double rpower = 0.0;
		if(side == StartingSide.BlueAlliance){
			lpower = BUMP_POWER;
			rpower = -BUMP_SMOL_POWER;
		}
		else{
			rpower = BUMP_POWER;
			lpower = -BUMP_SMOL_POWER;
		}
		
		addSequential(new DriveByPowerCommand(BUMP_TIME, lpower, rpower));
		addSequential(new DriveForwardCommand(FORWARD_SPEED, FORWARD_DISTANCE, false));
		
		addSequential(new StartShooting(true, true));
	}
	
	@Override
	public Pose2D getStartingPose(StartingSide side, boolean reversed){
		return new Pose2D(Math.abs(STARTING_CLOSE_POSITION_X - side.xOffset), STARTING_CLOSE_POSITION_Y, RobotState.normalizeAngle(-Math.abs(side.headingOffset - STARTING_CLOSE_POSITION_HEADING)));
	}
}
