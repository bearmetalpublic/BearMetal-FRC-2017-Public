package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.chassis.DriveByPowerCommand;
import org.usfirst.frc.team2046.robot.chassis.DriveForwardCommand;
import org.usfirst.frc.team2046.robot.chassis.TurnCommand;
import org.usfirst.frc.team2046.robot.chassis.ZeroEncoderDistance;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.hopper.SetHopperState;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;
import org.usfirst.frc.team2046.robot.util.WaitCommand;
import org.usfirst.frc.team2046.robot.vision.Vision.Mode;

public class BumpRetrieveFuelFromFarHopperAndShoot extends AutoAction {
	private static final double BUMP_TIME = 0.35;
	private static final double BUMP_POWER = 1.0;
	
	private static final double TO_FAR_HOPPER_DISTANCE = 132.7 * 0.0254;
	private static final double TO_FAR_HOPPER_SPEED = 3.0;
	private static final double TO_FAR_HOPPER_ACCELERATION = 2.0;
	
	private static final double TO_FAR_HOPPER_SHORT_DISTANCE = 132.7 * 0.0254;
	
	private static final double STARTING_FAR_POSITION_X = 21.750 * 0.0254;
	private static final double STARTING_FAR_POSITION_Y = 273.000 * 0.0254;
	private static final double STARTING_FAR_POSITION_HEADING = Math.toRadians(13.0);
	
	private static final double HOPPER_WAIT_TIME = 3.0;
	
	private static final double FORWARD_DISTANCE = 60.0 * 0.0254;
	private static final double FORWARD_SPEED = 2.0;
	public BumpRetrieveFuelFromFarHopperAndShoot(StartingSide side) {
		addSequential(new ZeroEncoderDistance());
		addParallel(new StartShooting.Prep());
		addSequential(new DriveForwardCommand(TO_FAR_HOPPER_SPEED, TO_FAR_HOPPER_SHORT_DISTANCE, TO_FAR_HOPPER_ACCELERATION, true, false));
		addParallel(new CollectorCommand(Collector.State.SLOW));
		
		double lpower = 0.0;
		double rpower = 0.0;
		if(side == StartingSide.BlueAlliance){
			rpower = -BUMP_POWER;
		}
		else{
			lpower = -BUMP_POWER;
		}
		
		addSequential(new DriveByPowerCommand(BUMP_TIME, lpower, rpower));
		addSequential(new DriveForwardCommand.AbsoluteDistance(TO_FAR_HOPPER_SPEED, TO_FAR_HOPPER_DISTANCE, true, true));
		addSequential(new StartShooting(true, true));
		
//		addSequential(new WaitCommand(HOPPER_WAIT_TIME));
//		addParallel(new CollectorCommand(Collector.State.SLOW));
//		addSequential(new SetHopperState(Hopper.State.OFF));
//		addSequential(new TurnCommand.ToAngle(false, false, side.headingOffset));
//		addSequential(new DriveForwardCommand(FORWARD_SPEED, FORWARD_DISTANCE, false));
//		addSequential(new StartShooting(true, true));
	}
	
	@Override
	public Pose2D getStartingPose(StartingSide side, boolean reversed){
		return new Pose2D(Math.abs(STARTING_FAR_POSITION_X - side.xOffset), STARTING_FAR_POSITION_Y, RobotState.normalizeAngle(-Math.abs(side.headingOffset - STARTING_FAR_POSITION_HEADING)));
	}
}
