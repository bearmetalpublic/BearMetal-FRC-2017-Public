package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.chassis.DriveForwardCommand;
import org.usfirst.frc.team2046.robot.chassis.TurnCommand;
import org.usfirst.frc.team2046.robot.gear.PlaceGearCommand;
import org.usfirst.frc.team2046.robot.vision.SetVisionCommand;
import org.usfirst.frc.team2046.robot.vision.Vision;

public class PlaceSideGear extends AutoAction {
	private static final double FORWARD_DISTANCE = 24 * 0.0254;
	private static final double FORWARD_SPEED = 1.0;
	
	public PlaceSideGear(Path path) {
		super(path);
		
		addSequential(new SetVisionCommand(Vision.Mode.GEAR_TARGETING));
		addSequential(new FollowPathCommand("PlaceGearPath", getPath(), true));
		addSequential(new TurnCommand.AlignByVision(false, false));
		addSequential(new DriveForwardCommand(FORWARD_SPEED, FORWARD_DISTANCE, true));
		addSequential(new PlaceGearCommand());
	}
}
