package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.FieldData;

public class StartingPose2D extends Pose2D{
	public static final double ROBOT_WIDTH = 35.564 * 0.0254;
	public static final double ROBOT_LENGTH = 34.439 * 0.0254;
	
	public static enum StartingSide{
		BlueAlliance (Math.PI, 0.0),
		RedAlliance (0.0, FieldData.Map.LENGTH_OF_FIELD);
		
		public final double headingOffset, xOffset;
		private StartingSide(double headingOffset, double xOffset){
			this.headingOffset = headingOffset;
			this.xOffset = xOffset;
		}
	}
	
	public StartingPose2D(StartingSide side, AutoAction action, boolean reversed) throws Exception {
		super(getStartingRobotPose(side, action, reversed));
	}
	
	public static Pose2D getStartingRobotPose(StartingSide side, AutoAction action, boolean reversed) throws Exception{
		double startingHeading, startingX, startingY;
		Waypoint initialWaypoint = action.getPath().getWaypoints().get(0);
		
		startingHeading = side.headingOffset + (reversed ? Math.PI : 0.0);
		startingX = initialWaypoint.x;	
		startingY = initialWaypoint.y;
		
		return new Pose2D(startingX, startingY, startingHeading);
	}
}