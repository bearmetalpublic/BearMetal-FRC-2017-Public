/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.autonomous;

import java.util.logging.Level;

import org.tahomarobotics.robot.path.DriveDashboardPathCommand;
import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.SplinePath;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.BuildAuto;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.BuildAuto.Action;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Autonomous extends BearSubsystem {
	private static final Autonomous autonomous = new Autonomous();
	
	private enum AutoCommand {
		None,
		TestPath,
		BuildPath; 
	}
	
	private class NoAuto extends Command {
		public NoAuto() {
			super("None");
		}
		
		@Override
		protected boolean isFinished() {
			logger.info("Ran nothing");
			return true;
		}
	};
	
	private final SendableChooser<AutoCommand> autoChooser = new SendableChooser<AutoCommand>();
	private final SendableChooser<Action> actionChooser = new SendableChooser<Action>();
	
	private Command autoCommand;
	
	private Autonomous() {
		super(Autonomous.class);
	}
	
	public static Autonomous getInstance() {
		return autonomous;
	}
	
	@Override
	public void initSubsystem() {
		autoChooser.addObject("None", AutoCommand.None);
		autoChooser.addObject("Test Path", AutoCommand.TestPath);
		autoChooser.addDefault("Build Auto", AutoCommand.BuildPath);
		
		actionChooser.addObject("Place Gear On Feeder Peg", Action.PLACE_GEAR_ON_FEEDER);
		actionChooser.addObject("Place Gear On Center Peg", Action.PLACE_GEAR_ON_CENTER);
		actionChooser.addObject("Place Gear On Center Peg, go to Hopper, and Shoot", Action.PLACE_GEAR_ON_CENTER_GO_TO_HOPPER_AND_SHOOT);
		actionChooser.addObject("Place Gear On Boiler Peg", Action.PLACE_GEAR_ON_BOILER);
		actionChooser.addObject("Place Gear On Boiler Peg, and Shoot", Action.PLACE_GEAR_ON_BOILER_AND_SHOOT);
		actionChooser.addDefault("Place Gear On Center Peg, and Shoot", Action.PLACE_GEAR_ON_CENTER_AND_SHOOT);
		actionChooser.addObject("Go to Hopper, and Shoot", Action.GO_TO_HOPPER_AND_SHOOT);
		actionChooser.addObject("Go to Hopper Parallel, and Shoot", Action.GO_TO_HOPPER_PARALLEL_AND_SHOOT);
		actionChooser.addObject("Go to Far Hopper, Bump, and Shoot", Action.GO_TO_FAR_HOPPER_BUMP_AND_SHOOT);
		actionChooser.addObject("Go to Close Hopper, Bump, and Shoot", Action.GO_TO_CLOSE_HOPPER_BUMP_AND_SHOOT);
		
		SmartDashboard.putData("Autonomous Command", autoChooser);
		SmartDashboard.putData("Autonomous Action Chooser", actionChooser);
		SmartDashboard.putData("Start SmartDashboard Path", new DriveDashboardPathCommand());
	}
	
	public void end() {
		if (autoCommand != null) {
			autoCommand.cancel();
		}
		
		Scheduler.getInstance().add(new StartShooting.Stop());
	}

	public void start() {
		logger.log(Level.INFO, "start");
		
		switch(command) {
		case TestPath:
			autoCommand = new CommandGroup(){
				{
					addSequential(new RobotState.ResetRobotState(18.25*0.0254, 2.340, Math.PI));
					addSequential(new FollowPathCommand("Test Drive", new SplinePath()
							.add(new Waypoint( 18.25*0.0254, 2.340, 1.0))
							.add(new Waypoint(138.25*0.0254, 2.340, 1.0)), true));
				}
			};
			break;
		case BuildPath:
			autoCommand = new BuildAuto(side, action, 0.5);
			break;
		case None:
		default:
			autoCommand = new NoAuto();
			break;
		}
		
		if (autoCommand != null) {
			autoCommand.start();
		}
	}
	
	private StartingSide side, prevSide;
	private Action action, prevAction;
	private AutoCommand command;
	@Override
	protected void runningUpdate() {
		side = Robot.getSide();
		action = actionChooser.getSelected();
		command = autoChooser.getSelected();
		
		if (command == AutoCommand.BuildPath &&
			(side != prevSide || action != prevAction)){
			autoCommand = new BuildAuto(side, action, 0.5);
		}
		
		prevSide = side;
		prevAction = action;
	}

	@Override
	protected void debugUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void startDebug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stopDebug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void reset() {
		Vision.getInstance().clearAutoSetupAssistance();
	}
}
