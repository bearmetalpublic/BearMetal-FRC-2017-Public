package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.chassis.DriveForwardCommand;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;

public class ParallelRetrieveFuelFromHopperAndShoot extends AutoAction {
	private final static double HIT_HOPPER_DISTANCE = 54 * 0.0254;
	public ParallelRetrieveFuelFromHopperAndShoot(Path path, double speed) {
		super(path);
		
		addSequential(new FollowPathCommand("ParallelRetrieveFuelFromHopper", getPath(), true));
		addSequential(new CollectorCommand(Collector.State.SLOW));
		addSequential(new StartShooting.Prep());
		addSequential(new DriveForwardCommand(speed, HIT_HOPPER_DISTANCE, false));
		addSequential(new StartShooting(true, true));
	}
}
