package org.usfirst.frc.team2046.robot.autonomous.buildauto;



import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.gear.PlaceGearCommand;

public class PlaceCenterGear extends AutoAction {	
	public PlaceCenterGear(Path path){
		super(path);
		
		addSequential(new FollowPathCommand("PlaceGearPath", getPath(), true));
		addSequential(new PlaceGearCommand());
	}
}
