package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.chassis.TurnCommand;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.hopper.SetHopperState;
import org.usfirst.frc.team2046.robot.shooter.SetShooterSpeed;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;

public class ShootFuelFromHopper extends AutoAction {
//	private final static double BACK_UP_POWER = -1.0;
//	private final static double BACK_UP_TIME = 0.5;
//	private final static double FORWARDS_POWER = 2.0;
//	private final static double FORWARDS_TIME = 0.8;
	private final static double FORWARDS_DISTANCE = 24 * 0.0254;
	private final static double SPIN_DISTANCE_FUDGE_FACTOR = -7.0;
	
	public ShootFuelFromHopper(Path path, boolean reversed, boolean shootFirst){
		super(path);
		
//		DriveStage firstStage = new DriveStage(BACK_UP_TIME * 1000, BACK_UP_POWER);
//		DriveStage secondStage = new DriveStage(FORWARDS_TIME * 1000, FORWARDS_POWER);
		
		addSequential(new StartShooting.Prep());
//		commands.add(new DriveByPowerCommand(BACK_UP_TIME, BACK_UP_POWER, BACK_UP_POWER));
//		commands.add(new TurnCommand.AlignByDeadReckoning(false, Vision.Mode.BOILER_TARGETING));
		addSequential(new FollowPathCommand("ShootFuelPath", getPath(), true, false));
		addSequential(new CollectorCommand(Collector.State.SLOW));
//		commands.add(new AlignWithDriveStagesCommand.ByDeadReckoning(Vision.Mode.BOILER_TARGETING, secondStage));
//		commands.add(new DriveByPowerCommand(FORWARDS_TIME, FORWARDS_POWER, FORWARDS_POWER));
//		commands.add(new StartShooting.Prep(true));
//		commands.add(new AlignWithDriveStagesCommand.ByDeadReckoning(Vision.Mode.BOILER_TARGETING, firstStage, secondStage));
		addSequential(new TurnCommand.AlignByVision(false, true, FORWARDS_DISTANCE));
		addSequential(new SetShooterSpeed.ByVision(true, SPIN_DISTANCE_FUDGE_FACTOR));
		addSequential(new SetHopperState(Hopper.State.ON));
		addSequential(new CollectorCommand(Collector.State.FAST));
		addSequential(new SetShooterSpeed.ByVision(true));
		addSequential(new TurnCommand.AlignByVision(false, true));
	}
}
