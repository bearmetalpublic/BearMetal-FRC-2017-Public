package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.FollowPathCommand;
import org.tahomarobotics.robot.path.Path;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;

public class ShootFuelFromSideGearPeg extends AutoAction {

	private static final double SPIN_DISTANCE_FUDGE_FACTOR = -12.0;
	
	public ShootFuelFromSideGearPeg(Path path){
		super(path);
		
		addSequential(new StartShooting.Prep());
		addSequential(new FollowPathCommand("ShootFuelPath", getPath(), false, false));
		addSequential(new StartShooting(false, false, SPIN_DISTANCE_FUDGE_FACTOR));
	}
}