package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.hopper.SetHopperState;
import org.usfirst.frc.team2046.robot.util.WaitCommand;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class ReleaseHopper extends CommandGroup {
	private static final double RELEASE_HOPPER_WAIT = 0.1;
	
	public ReleaseHopper(){
		addSequential(new SetHopperState(Hopper.State.ON));
		addSequential(new WaitCommand(RELEASE_HOPPER_WAIT));
		addSequential(new SetHopperState(Hopper.State.REVERSED));
		addSequential(new WaitCommand(RELEASE_HOPPER_WAIT));
		addSequential(new SetHopperState(Hopper.State.OFF));
	}
}
