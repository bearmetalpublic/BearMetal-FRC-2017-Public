package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.Path;
import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;

import edu.wpi.first.wpilibj.command.CommandGroup;

public abstract class AutoAction extends CommandGroup{
	private final Path path;
	public AutoAction(Path path){
		this.path = path;
	}
	
	public AutoAction(){
		this(null);
	}
	
	public Path getPath(){
		return path;
	}
	
	public Pose2D getStartingPose(StartingSide side, boolean reversed) throws Exception{
		return new StartingPose2D(side, this, reversed);
	}
}
