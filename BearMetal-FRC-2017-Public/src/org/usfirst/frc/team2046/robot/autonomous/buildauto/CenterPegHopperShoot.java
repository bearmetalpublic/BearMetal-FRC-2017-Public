package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.Robot;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.chassis.DriveByPowerCommand;
import org.usfirst.frc.team2046.robot.chassis.DriveForwardCommand;
import org.usfirst.frc.team2046.robot.chassis.DrivePreciselyCommand;
import org.usfirst.frc.team2046.robot.chassis.DrivePreciselyCommand.DriveStage;
import org.usfirst.frc.team2046.robot.gear.PlaceGearCommand;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;
import org.usfirst.frc.team2046.robot.util.WaitCommand;

/**
 *
 */
public class CenterPegHopperShoot extends AutoAction {
	private static final double STARTING_POSE_X = 18.25 * 0.0254;
	private static final double STARTING_POSE_Y = 161.890 * 0.0254;
	private static final double STARTING_POSE_HEADING = Math.PI;
	
	private static final double BUMP_TIME = 0.5;
	private static final double BUMP_POWER = 1.0;
	private static final double HOPPER_WAIT = 2.0;

	private static final double TO_GEAR_SPEED = 1.0;
	private static final double TO_GEAR_DISTANCE = 78.0 * 0.0254;
	private static final double STRAIGHT_DISTANCE = 24.9 * 0.0254;
	
	private static final double CURVE_SPEED = 1.0;
	private static final double FIRST_CURVE_RADIUS = 50.0 * 0.0254;
	private static final double FIRST_CURVE_ARC = Math.toRadians(150.7);
	private static final double FIRST_CURVE_DISTANCE = FIRST_CURVE_RADIUS * FIRST_CURVE_ARC;
	
	private static final double SECOND_CURVE_RADIUS = 26.4 * 0.0254;
	private static final double SECOND_CURVE_ARC = Math.toRadians(153.7);
	private static final double SECOND_CURVE_DISTANCE = SECOND_CURVE_RADIUS * SECOND_CURVE_ARC;
	
	public CenterPegHopperShoot() {
    	addSequential(new DriveForwardCommand(TO_GEAR_SPEED, TO_GEAR_DISTANCE, true));
    	addSequential(new PlaceGearCommand());
    	
    	double firstTurn = Robot.getSide() == StartingSide.BlueAlliance ? -1.0 : 1.0;
    	double secondTurn = -firstTurn;
    	
    	addSequential(new DrivePreciselyCommand(
    			CURVE_SPEED,
    			new DriveStage(STRAIGHT_DISTANCE, 0.0),
    			new DriveStage(FIRST_CURVE_DISTANCE, firstTurn * FIRST_CURVE_ARC),
    			new DriveStage(SECOND_CURVE_DISTANCE, secondTurn * SECOND_CURVE_ARC)
    	));
    	
    	double bumpLeft   = secondTurn * BUMP_POWER;
    	double bumpRight = -bumpLeft;
		addSequential(new DriveByPowerCommand(BUMP_TIME, bumpLeft, bumpRight));

		addSequential(new WaitCommand(HOPPER_WAIT));
		addSequential(new StartShooting(true, true));
    }
	
	@Override
	public Pose2D getStartingPose(StartingSide side, boolean reversed){
		return new Pose2D(Math.abs(side.xOffset - STARTING_POSE_X), STARTING_POSE_Y, side.headingOffset + STARTING_POSE_HEADING);
	}
}
