package org.usfirst.frc.team2046.robot.autonomous.buildauto;

import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.usfirst.frc.team2046.robot.vision.CalibrateRobotStateCommand;

import edu.wpi.first.wpilibj.command.Scheduler;

@Deprecated
public class UpdateRobotPoseOnWaypointListener implements CompletionListener {
	private final double baseLocationX, baseLocationY;
	
	public UpdateRobotPoseOnWaypointListener(double baseLocationX, double baseLocationY){
		this.baseLocationX = baseLocationX;
		this.baseLocationY = baseLocationY;
	}
	
	@Override
	public void onCompletion() {
		Scheduler.getInstance().add(new CalibrateRobotStateCommand(baseLocationX, baseLocationY));
	}

}
