package org.usfirst.frc.team2046.robot.collector;

import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;

import edu.wpi.first.wpilibj.command.Command;

public class CollectorCommand extends Command implements TogglableCommand {

	private final Collector.State state;
	private final Collector collector = Collector.getInstance();
	
	public CollectorCommand(Collector.State state){
		this.state = state;
	}
	
	@Override
	protected boolean isFinished() {
		collector.setSpeed(state);
		return true;
	}
	@Override
	public boolean isToggled() {
		return collector.getCurrentState() == state;
	}
	
	@Override
	public Command toCommand() {
		return this;
	}

}
