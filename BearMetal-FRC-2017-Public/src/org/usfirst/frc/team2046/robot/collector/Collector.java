/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.collector;

import java.util.logging.Level;

import org.usfirst.frc.team2046.robot.util.BearSubsystem;

import com.ctre.CANTalon;
import com.ctre.CANTalon.TalonControlMode;

public class Collector extends BearSubsystem {

	private static final int CURRENT_LIMIT = 20;
	
	public enum State{
		SLOW (0.5),
		FAST (1.0),
		OFF (0.0);
		
		private final double percentVbus;
		private State(double percentVbus){
			this.percentVbus = percentVbus;
		}
	}
	
	public static Collector getInstance() {
		return collector;
	}
	private final CANTalon collectorMotorFront;
	private final CANTalon collectorMotorBack;
	
	private State currentState = State.OFF;

	private static final Collector collector = new Collector();
	private Collector() {
		super(Collector.class);
		
		CANTalon collectorMotorFront = null;
		CANTalon collectorMotorBack = null;
		
		try{
			collectorMotorFront = new CANTalon(RobotMap.COLLECTOR_MOTOR_FRONT);
			collectorMotorFront.setCurrentLimit(CURRENT_LIMIT);
			collectorMotorFront.EnableCurrentLimit(true);
			collectorMotorFront.changeControlMode(TalonControlMode.PercentVbus);
			collectorMotorFront.setInverted(true);
			collectorMotorFront.enable();
			
			collectorMotorBack = new CANTalon(RobotMap.COLLECTOR_MOTOR_BACK);
			collectorMotorBack.setCurrentLimit(CURRENT_LIMIT);
			collectorMotorBack.EnableCurrentLimit(true);
			collectorMotorBack.setInverted(true);
			collectorMotorBack.changeControlMode(TalonControlMode.PercentVbus);
			collectorMotorBack.enable();
		}
		catch(Exception e){
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}
		
		this.collectorMotorFront = collectorMotorFront;
		this.collectorMotorBack = collectorMotorBack;
	}
	
	
	@Override
	public void initSubsystem() {	
	}
	
	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void runningUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void debugUpdate() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void startDebug() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void stopDebug() {
		// TODO Auto-generated method stub
		
	}
	
	public void setSpeed(State state) {
		collectorMotorFront.set(state.percentVbus);
		collectorMotorBack.set(state.percentVbus);
		currentState = state;
	}

	public State getCurrentState(){
		return currentState;
	}


	@Override
	public void reset() {
		setSpeed(State.OFF);
	}
}