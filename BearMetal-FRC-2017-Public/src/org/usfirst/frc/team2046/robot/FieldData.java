package org.usfirst.frc.team2046.robot;

import java.util.ArrayList;

import org.tahomarobotics.robot.path.Path;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.autonomous.buildauto.StartingPose2D.StartingSide;
import org.usfirst.frc.team2046.robot.util.SubsystemIF;

import edu.wpi.first.wpilibj.networktables.NetworkTable;

public class FieldData implements SubsystemIF {
	public static class Map {
		public static final double LENGTH_OF_FIELD = 16.54;
		public static final double WIDTH_OF_FIELD = 8.23;
		public static final double FEEDING_STATION_LINE_TO_X_AXIS = 2.34;
		public static final double CENTER_LINE_TO_X_AXIS = 4.1045;
		public static final double BOILER_KEY_LINE_TO_X_AXIS = 5.90;
		
		//All x and y are relative to blue alliance station
		private static final double RIGHT_PEG_X = 3.29;
		private static final double RIGHT_PEG_Y = 3.257;
		private static final double RIGHT_PEG_ANGLE = Math.PI * -2.0 / 3.0;
		private static final double MIDDLE_PEG_X = 2.801;
		private static final double MIDDLE_PEG_Y = 4.112;
		private static final double MIDDLE_PEG_ANGLE = Math.PI;
		private static final double LEFT_PEG_X = 3.29;
		private static final double LEFT_PEG_Y = 4.972;
		private static final double LEFT_PEG_ANGLE = Math.PI * 2.0 / 3.0;
		
		public static double GetBoilerPegX(){
			return Math.abs(LEFT_PEG_X - Robot.getSide().xOffset); 
		}
		public static double GetBoilerPegY(){
			return LEFT_PEG_Y;
		}
		public static double GetBoilerPegAngle(){
			return LEFT_PEG_ANGLE / (Robot.getSide() == StartingSide.BlueAlliance ? 1 : 2);
		}
		
		public static double GetFeederPegX(){
			return Math.abs(RIGHT_PEG_X - Robot.getSide().xOffset); 
		}
		public static double GetFeederPegY(){
			return RIGHT_PEG_Y;
		}
		public static double GetFeederPegAngle(){
			return RIGHT_PEG_ANGLE / (Robot.getSide() == StartingSide.BlueAlliance ? 1 : 2);
		}
		
		public static double GetCenterPegX(){
			return Math.abs(MIDDLE_PEG_X - Robot.getSide().xOffset); 
		}
		public static double GetCenterPegY(){
			return MIDDLE_PEG_Y;
		}
		public static double GetCenterPegAngle(){
			return Math.abs(MIDDLE_PEG_ANGLE - Robot.getSide().headingOffset);
		}
		
		private static final double BOILER_X = 0.0;
		private static final double BOILER_Y = 8.23;
		public static final double BOILER_RADIUS = 7.375 * 0.0254;
		
		public static double GetBoilerX(){
			return Math.abs(BOILER_X - Robot.getSide().xOffset);
		}
		public static double GetBoilerY(){
			return BOILER_Y;
		}
		
		public static final double HOPPER_BOILER_X = 3.048;
		public static final double HOPPER_BOILER_Y = WIDTH_OF_FIELD;
		public static final double HOPPER_CENTER_X = 8.28;
		public static final double HOPPER_CENTER_Y = WIDTH_OF_FIELD;
		public static final double HOPPER_NEAR_X = 5.261;
		public static final double HOPPER_NEAR_Y = 0.0;
		public static final double HOPPER_FAR_X = LENGTH_OF_FIELD - HOPPER_NEAR_X;
		public static final double HOPPER_FAR_Y = 0.0;
	}
	
	private final NetworkTable table;
	private final Pose2D pose;
	public ArrayList<Path> displayPaths = new ArrayList<Path>();
	private Waypoint lookahead = new Waypoint();
	
	private static FieldData singleton = new FieldData();
	public static FieldData getInstance() {
		return singleton;
	}
	
	private FieldData() {
		table = NetworkTable.getTable("FieldData");
		pose = new Pose2D(0, 0, 0);
	}

	@Override
	public void update() {
		RobotState.getInstance().getPose(pose);
		
		table.putNumber("Robot X", pose.x);
		table.putNumber("Robot Y", pose.y);
		table.putNumber("Robot Heading", pose.heading);
		table.putNumber("Robot Path Count", displayPaths.size());
		for (int i = 0; i < displayPaths.size(); i++) {
			table.putString("Robot Path " + i, displayPaths.get(i).serialize());
		}
		table.putNumber("Waypoint Lookahead X", lookahead.x);
		table.putNumber("Waypoint Lookahead Y", lookahead.y);
	}
	
	public void setLookahead(Waypoint p) {
		lookahead = p;
	}

	@Override
	public void reset() {
	}

	@Override
	public void init() {
	}
}
