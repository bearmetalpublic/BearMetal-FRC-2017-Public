/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.usfirst.frc.team2046.robot.chassis.Chassis;
import org.usfirst.frc.team2046.robot.chassis.DriveByPowerCommand;
import org.usfirst.frc.team2046.robot.chassis.TurnCommand;
import org.usfirst.frc.team2046.robot.climber.ClimbCommand;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.gear.AcquireGear;
import org.usfirst.frc.team2046.robot.gear.DeployGobbler;
import org.usfirst.frc.team2046.robot.gear.GearManipulator;
import org.usfirst.frc.team2046.robot.gear.PlaceGearCommand;
import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.hopper.SetHopperState;
import org.usfirst.frc.team2046.robot.shooter.SetShooterSpeed;
import org.usfirst.frc.team2046.robot.shooter.Shooter;
import org.usfirst.frc.team2046.robot.shooter.StartShooting;
import org.usfirst.frc.team2046.robot.util.SubsystemIF;
import org.usfirst.frc.team2046.robot.util.ToggleCommand;
import org.usfirst.frc.team2046.robot.util.XboxController;
import org.usfirst.frc.team2046.robot.vision.Vision.Mode;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI implements SubsystemIF {

	private static final long RUMBLE_NO_EXPIRE_TIME = 100000000;
	private static final int DRIVER_PORT = 0;
	private static final int MANIPULATOR_PORT = 1;
	private static final OI instance = new OI();

	private static final Logger logger = Logger.getLogger(OI.class.getName());

	private boolean isReversed;

	public final XboxController driverController;
	public final XboxController manipulatorController;

	private long driverLeftRumbleExpireTime;
	private boolean driverLeftRumbleActive;
	private long driverRightRumbleExpireTime;
	private boolean driverRightRumbleActive;
	private long manipulatorLeftRumbleExpireTime;
	private boolean manipulatorLeftRumbleActive;
	private long manipulatorRightRumbleExpireTime;
	private boolean manipulatorRightRumbleActive;

	public enum Controller {
		DRIVER, MANIPULATOR
	}

	private OI() {
		XboxController driverController = null;
		XboxController manipulatorController = null;

		try {
			driverController = new XboxController(DRIVER_PORT);
			manipulatorController = new XboxController(MANIPULATOR_PORT);

		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct OI instance", e);
		}

		this.driverController = driverController;
		this.manipulatorController = manipulatorController;
	}

	public static OI getInstance() {
		return instance;
	}

	@Override
	public void init() {
		// chassis is initially reversed (gear gobbler is seen as front)
		isReversed = true;

		// Driver Controller:
		driverController.buttonStart.whenPressed(new ToggleCommand(new ClimbCommand(1.0), new ClimbCommand(0.0)));

		driverController.buttonB.whenPressed(new StartShooting.Stop());
		driverController.buttonA.whenPressed(new StartShooting.Tele());
		driverController.buttonX.whenPressed(new TurnCommand.AlignByVision.To(Mode.BOILER_TARGETING));
		driverController.buttonY.whenPressed(new TurnCommand.AlignByVision.To(Mode.GEAR_TARGETING));
		driverController.leftBumper.whenPressed(new PlaceGearCommand());
		driverController.rightBumper.whenPressed(new Command() {
			protected boolean isFinished() {
				isReversed = !isReversed;
				return true;
			}
		});

		// Manipulator Controller:
		manipulatorController.buttonStart.whenPressed(new ToggleCommand(new CollectorCommand(Collector.State.SLOW),
				new CollectorCommand(Collector.State.OFF)));
		manipulatorController.buttonBack.whenPressed(new ToggleCommand(new SetHopperState(Hopper.State.REVERSED), new SetHopperState(Hopper.State.OFF)));

		manipulatorController.buttonX.whenPressed(new DeployGobbler(true, false));
		manipulatorController.buttonY
				.whenPressed(new ToggleCommand(new SetShooterSpeed.Tele(), new SetShooterSpeed.Off()));
		manipulatorController.buttonB.whenPressed(new DeployGobbler(false, false));
		manipulatorController.buttonA.whenPressed(
				new ToggleCommand(new SetHopperState(Hopper.State.ON), new SetHopperState(Hopper.State.OFF)));

		manipulatorController.leftBumper.whenPressed(new AcquireGear());
		manipulatorController.rightBumper.whenPressed(new PlaceGearCommand());
		
		manipulatorController.buttonLeftStick.whenPressed(new Command(){
			@Override
			protected boolean isFinished() {
				GearManipulator.getInstance().toggleGobOverride();
				return true;
			}
		});

		SmartDashboard.putBoolean("Shooter Fudge Factor Locked", true);
	}

	public double getLeftDrive() {
		return isReversed ? -driverController.getAxisValue(XboxController.RIGHT_LONGITUDIAL_AXIS)
				: driverController.getAxisValue(XboxController.LEFT_LONGITUDIAL_AXIS);
	}

	public double getRightDrive() {
		return isReversed ? -driverController.getAxisValue(XboxController.LEFT_LONGITUDIAL_AXIS)
				: driverController.getAxisValue(XboxController.RIGHT_LONGITUDIAL_AXIS);
	}

	public void startDriverLeftRumble(long expireMs) {
		driverLeftRumbleActive = true;
		driverLeftRumbleExpireTime = System.currentTimeMillis() + expireMs;
		driverController.setLeftRumble(1.0f);
		logger.log(Level.INFO, "Starting driver left rumble.");
	}

	public void startDriverLeftRumble() {
		startDriverLeftRumble(RUMBLE_NO_EXPIRE_TIME);
	}

	public void stopDriverLeftRumble() {
		driverLeftRumbleActive = false;
		driverController.setLeftRumble(0.0f);
	}

	public void startDriverRightRumble(long expireMs) {
		driverRightRumbleActive = true;
		driverRightRumbleExpireTime = System.currentTimeMillis() + expireMs;
		driverController.setRightRumble(1.0f);
		logger.log(Level.INFO, "Starting driver right rumble.");
	}

	public void startDriverRightRumble() {
		startDriverRightRumble(RUMBLE_NO_EXPIRE_TIME);
	}

	public void stopDriverRightRumble() {
		driverRightRumbleActive = false;
		driverController.setRightRumble(0.0f);
	}

	public void startManipulatorLeftRumble(long expireMs) {
		manipulatorLeftRumbleActive = true;
		manipulatorLeftRumbleExpireTime = System.currentTimeMillis() + expireMs;
		manipulatorController.setLeftRumble(1.0f);
		logger.log(Level.INFO, "Starting manipulator left rumble.");
	}

	public void startManipulatorLeftRumble() {
		startManipulatorLeftRumble(RUMBLE_NO_EXPIRE_TIME);
	}

	public void stopManipulatorLeftRumble() {
		manipulatorLeftRumbleActive = false;
		manipulatorController.setLeftRumble(0.0f);
	}

	public void startManipulatorRightRumble(long expireMs) {
		manipulatorRightRumbleActive = true;
		manipulatorRightRumbleExpireTime = System.currentTimeMillis() + expireMs;
		manipulatorController.setRightRumble(1.0f);
		logger.log(Level.INFO, "Starting manipulator right rumble.");
	}

	public void startManipulatorRightRumble() {
		startManipulatorRightRumble(RUMBLE_NO_EXPIRE_TIME);
	}

	public void stopManipulatorRightRumble() {
		manipulatorRightRumbleActive = false;
		manipulatorController.setRightRumble(0.0f);
	}

	public void setFrontTargetVisible(boolean visible) {
		driverController.setLeftRumble(visible ? 1.0f : 0.0f);
	}

	public void setBackTargetVisible(boolean visible) {
		driverController.setRightRumble(visible ? 1.0f : 0.0f);
	}

	private int ManipulatorDPadCurrentValue = -1;
	private int ManipulatorDPadPastValue = -1;
	private int DriverDPadCurrentValue = -1;
	private int DriverDPadPastValue = -1;

	private Command driveAdjustmentCommand;
	private static final double ADJUSTMENT_SPEED_TRANSLATIONAL = 0.35, ADJUSTMENT_SPEED_ROTATIONAL = 0.45;

	@Override
	public void update() {
		SmartDashboard.putBoolean("Drive Reversed", isReversed);
		SmartDashboard.putBoolean("Drive Not Reversed", !isReversed);

		ManipulatorDPadCurrentValue = manipulatorController.getPOV();
		if (ManipulatorDPadPastValue != ManipulatorDPadCurrentValue && !SmartDashboard.getBoolean("Shooter Fudge Factor Locked", true)) {
			switch (ManipulatorDPadCurrentValue) {
			case 0:
				logger.log(Level.INFO,
						"Increasing shooter offset. New offset: " + Shooter.getInstance().increaseInchesOffset());
				break;
			case 90:
				logger.log(Level.INFO,
						"Decrease turn offset. New Offset: " + Chassis.getInstance().decreaseTurnCommandFudgeFactor());
				break;
			case 180:
				logger.log(Level.INFO,
						"Decreasing shooter offset. New offset: " + Shooter.getInstance().decreaseInchesOffset());
				break;
			case 270:
				logger.log(Level.INFO, "Increasing turn offset. New Offset: "
						+ Chassis.getInstance().increaseTurnCommandFudgeFactor());
				break;
			case -1:
				break;
			default:
				logger.log(Level.WARNING, "Invalid DPad Value" + ManipulatorDPadCurrentValue);
				break;
			}
		}
		ManipulatorDPadPastValue = ManipulatorDPadCurrentValue;

		DriverDPadCurrentValue = driverController.getPOV();
		if (DriverDPadPastValue != DriverDPadCurrentValue) {
			switch (DriverDPadCurrentValue) {
			case 0:
				driveAdjustmentCommand = new DriveByPowerCommand(0.0, ADJUSTMENT_SPEED_TRANSLATIONAL, ADJUSTMENT_SPEED_TRANSLATIONAL, true);
				Scheduler.getInstance().add(driveAdjustmentCommand);
				break;
			case 90:
				driveAdjustmentCommand = new DriveByPowerCommand(0.0, ADJUSTMENT_SPEED_ROTATIONAL, -ADJUSTMENT_SPEED_ROTATIONAL, true);
				Scheduler.getInstance().add(driveAdjustmentCommand);
				break;
			case 180:
				driveAdjustmentCommand = new DriveByPowerCommand(0.0, -ADJUSTMENT_SPEED_TRANSLATIONAL, -ADJUSTMENT_SPEED_TRANSLATIONAL, true);
				Scheduler.getInstance().add(driveAdjustmentCommand);
				break;
			case 270:
				driveAdjustmentCommand = new DriveByPowerCommand(0.0, -ADJUSTMENT_SPEED_ROTATIONAL, ADJUSTMENT_SPEED_ROTATIONAL, true);
				Scheduler.getInstance().add(driveAdjustmentCommand);
				break;
			case -1:
				if(driveAdjustmentCommand != null && driveAdjustmentCommand.isRunning()){
					driveAdjustmentCommand.cancel();
				}
				break;
			default:
				logger.log(Level.WARNING, "Invalid DPad Value" + DriverDPadCurrentValue);
				break;
			}
		}
		DriverDPadPastValue = DriverDPadCurrentValue;

		if (driverLeftRumbleActive && (driverLeftRumbleExpireTime < System.currentTimeMillis())) {
			driverController.setLeftRumble(0.0f);
			driverLeftRumbleActive = false;
		}
		if (driverRightRumbleActive && (driverRightRumbleExpireTime < System.currentTimeMillis())) {
			driverController.setRightRumble(0.0f);
			driverRightRumbleActive = false;
		}
		if (manipulatorLeftRumbleActive && (manipulatorLeftRumbleExpireTime < System.currentTimeMillis())) {
			manipulatorController.setLeftRumble(0.0f);
			manipulatorLeftRumbleActive = false;
		}
		if (manipulatorRightRumbleActive && (manipulatorRightRumbleExpireTime < System.currentTimeMillis())) {
			manipulatorController.setRightRumble(0.0f);
			manipulatorRightRumbleActive = false;
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}
}
