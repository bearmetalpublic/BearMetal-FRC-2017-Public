/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.climber;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.usfirst.frc.team2046.robot.OI;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;
import org.usfirst.frc.team2046.robot.util.XboxController;

import com.ctre.CANTalon;
import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Climber extends BearSubsystem {
	private static final Logger logger = Logger.getLogger(Climber.class.getSimpleName());

//	private static final int CURRENT_LIMIT = 70;
	public static Climber getInstance() {
		return instance;
	}

	private static final Climber instance = new Climber();
	
	private static final double CLIMBER_DEADBAND = 50 / 127.0;
	private final CANTalon motorTop;
	private final CANTalon motorBottom;

	private double climberSetpoint;
	private Climber() {
		super(Climber.class);
		
		CANTalon motorTop = null;
		CANTalon motorBottom = null;

		try {
			motorTop = new CANTalon(RobotMap.CLIMBER_MOTOR_TOP);
			motorBottom = new CANTalon(RobotMap.CLIMBER_MOTOR_BOTTOM);

			motorTop.changeControlMode(TalonControlMode.PercentVbus);
			//motorTop.setCurrentLimit(CURRENT_LIMIT);
			//motorTop.EnableCurrentLimit(true);
			motorBottom.changeControlMode(TalonControlMode.Follower);
			//motorBottom.setCurrentLimit(CURRENT_LIMIT);
			//motorBottom.EnableCurrentLimit(true);

			motorBottom.set(motorTop.getDeviceID());
			
			motorTop.setInverted(false);
			
			motorTop.enable();
			motorBottom.enable();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}

		this.motorTop = motorTop;
		this.motorBottom = motorBottom;
		}

	public void setClimbSetpoint(double setpoint) {
		if(climberSetpoint != setpoint){
			setpoint = Math.max(0.0, setpoint);
			logger.log(Level.INFO, "Setting climber to: " + setpoint + "%V");
			motorTop.set(setpoint);
			climberSetpoint = setpoint;	
		}
	}
	
	public double getClimbSetpoint(){
		return climberSetpoint;
	}

	public double getMotorCurrent() {
		double currentTop = motorTop.getOutputCurrent();
		double currentBottom = motorBottom.getOutputCurrent();

		double currentSum = currentTop + currentBottom;
		double currentAverage = currentSum / 2.0;

		return currentAverage;
	}

	@Override
	protected void initDefaultCommand() {
		setDefaultCommand(new Command(){
			{
				requires(Climber.getInstance());
			}
			
			protected void execute(){
				setClimbSetpoint(applyDeadband(OI.getInstance().manipulatorController.getAxisValue(XboxController.RIGHT_LONGITUDIAL_AXIS)));
			}
			
			@Override
			protected boolean isFinished() {
				return false;
			}
		});
	}
	
	private double applyDeadband(double input){
		return (Math.abs(input) > CLIMBER_DEADBAND) ? input : 0.0;
	}

	@Override
	public void initSubsystem() {
	}

	@Override
	protected void runningUpdate() {
	}

	@Override
	protected void debugUpdate() {
		SmartDashboard.putNumber("Climber Current Top", motorTop.getOutputCurrent());
		SmartDashboard.putNumber("Climber Current Bottom", motorBottom.getOutputCurrent());
		SmartDashboard.putNumber("Climber Voltage Top", motorTop.getOutputVoltage());
		SmartDashboard.putNumber("Climber Voltage Bottom", motorBottom.getOutputVoltage());
	}

	@Override
	protected void startDebug() {
	}

	@Override
	protected void stopDebug() {
	}

	@Override
	public void reset() {
		motorTop.set(0.0);
	}
}
