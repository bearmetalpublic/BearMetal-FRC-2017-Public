package org.usfirst.frc.team2046.robot.climber;

import org.usfirst.frc.team2046.robot.util.DelayedCallback;
import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
@Deprecated
public class ClimbByStagesCommand extends Command implements TogglableCommand{
	private enum State{
		PRECLIMB(0.208, 1.0),
		STAGE_ONE_CLIMB(0.375, 3.0),
		STAGE_TWO_CLIMB(0.625, 5.0),
		STAGE_THREE_CLIMB(1.0, 25.0),
		OFF(0.0, Double.MAX_VALUE);
		
		private final double finishCurrent;
		private final double percentVoltage;
		
		private State(double percentVoltage, double finishCurrent){
			this.percentVoltage = percentVoltage;
			this.finishCurrent = finishCurrent;
		}
	}
	
	private volatile static State currentState = State.OFF;
	private final Climber climber;
	private final Command checkCommand;
	public ClimbByStagesCommand(){
		this.climber = Climber.getInstance();
		
		this.checkCommand = new DelayedCallback(0.50){
			public void onCallBack() {
				if(stateIsFinished()){
					currentState = nextState();
					climber.setClimbSetpoint(currentState.percentVoltage);
				}
			}
		};
		
		requires(climber);
	}
	
	private State nextState(){
		synchronized(this){
			State[] values = State.values();
			State current = currentState;
			int valueIndex = 0;
			
			for(int i = 0; i < values.length; i++){
				if(Double.compare(current.finishCurrent, values[i].finishCurrent) == 0){
					valueIndex = i;
					break;
				}
			}
			
			return (valueIndex + 1) < values.length ? values[valueIndex + 1] : State.PRECLIMB;
		}
	}
	
	private boolean stateIsFinished(){
		synchronized(this){
			return climber.getMotorCurrent() > currentState.finishCurrent;
		}
	}
	
	@Override
	protected void initialize(){
		currentState = State.PRECLIMB;
		climber.setClimbSetpoint(currentState.percentVoltage);
	}
	
	@Override
	protected void execute(){
		if(stateIsFinished() && !checkCommand.isRunning()){
			Scheduler.getInstance().add(checkCommand);
		}
	}
	
	@Override
	protected boolean isFinished() {
		return currentState == State.OFF;
	}

	@Override
	protected void end(){
		climber.setClimbSetpoint(0.0);
		currentState = State.OFF;
	}
	
	@Override
	protected void interrupted(){
		end();
	}

	@Override
	public boolean isToggled() {
		return currentState != State.OFF;
	}

	@Override
	public Command toCommand() {
		return this;
	}
}
