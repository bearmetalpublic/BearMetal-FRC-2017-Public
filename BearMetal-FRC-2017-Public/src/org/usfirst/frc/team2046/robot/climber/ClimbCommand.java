package org.usfirst.frc.team2046.robot.climber;

import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;

import edu.wpi.first.wpilibj.command.Command;

public class ClimbCommand extends Command implements TogglableCommand {

	private final double setpoint;
	private final Climber climber;
	public ClimbCommand(double setpoint){
		this.setpoint = setpoint;
		climber = Climber.getInstance();
		requires(climber);
	}
	
	@Override
	public boolean isToggled() {
		return climber.getClimbSetpoint() == setpoint;
	}

	@Override
	public Command toCommand() {
		return this;
	}

	@Override
	protected boolean isFinished() {
		climber.setClimbSetpoint(setpoint);
		return true;
	}

}
