/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.chassis.Chassis;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;

import com.analog.adis16448.frc.ADIS16448_IMU;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class RobotState extends BearSubsystem implements RobotPoseIF {

	public enum RotationMode{
		IMU_ONLY, BLENDED;
	}
	
	private static final RobotState instance = new RobotState();

	private static final long UPDATE_PERIOD_MS = 10;

	private double x = 0;
	private double y = 0;
	private double heading = 0;
	private RobotSpeed speed = new RobotSpeed();

	private final Timer timer;
	private long prevTime = System.currentTimeMillis();

	private final ADIS16448_IMU imu;
	private final Chassis chassis;
	private final RobotStateEstimator estimator;

	public static RobotState getInstance() {
		return instance;
	}

	private RobotState() {
		super(RobotState.class);

		imu = new ADIS16448_IMU();
		timer = new Timer(RobotState.class.getSimpleName());
		chassis = Chassis.getInstance();
		estimator = new RobotStateEstimator(chassis, imu);
		timer.schedule(estimator, 0, UPDATE_PERIOD_MS);
	}

	public Pose2D getPose(Pose2D pose) {
		synchronized (instance) {
			pose.x = x;
			pose.y = y;
			pose.heading = heading;
			return pose;
		}
	}

	public void getSpeed(RobotSpeed speed) {
		synchronized (instance) {
			speed.forward = this.speed.forward;
			speed.rotational = this.speed.rotational;
		}
	}

	private void setRobotState(double x, double y, double heading) {
		synchronized (instance) {
			this.x = x;
			this.y = y;
			this.heading = heading;
		}
	}
	

	private class RobotStateEstimator extends TimerTask {
		private final static double FREQ_CUTOFF = 1.0; // Hz
		private final static double TIME_CONSTANT = 1.0 / (2.0 * Math.PI * FREQ_CUTOFF);
		private static final int CALIBRATION_COUNT = 500;
		private static final long WAIT_CALIBRATION_FAIL = 500;
		private final Chassis chassis;
//		private final ADIS16448_IMU imu;
		private int calibrationCount = 0;
		private double imuYawRateAccum = 0;
		public double yawRateOffset = 0;
		private RotationMode rotationMode;

		public RobotStateEstimator(Chassis chassis, ADIS16448_IMU imu) {
			this.chassis = chassis;
//			this.imu = imu;
		}

		@Override
		public void run() {
			// delta time
			long time = System.currentTimeMillis();
			double dTime = (time - prevTime) * 0.001;
			if (dTime < 0.001) {
				return;
			}
			prevTime = time;

			// sensor inputs
			RobotSpeed odometrySpeed = chassis.getOdometrySpeed();
			RobotSpeed imuSpeed = getIMUSpeed(speed.forward, dTime);

			if (!isCalibrated()) {
				if(chassis.isStopped()){
					attemptCalibration(imuSpeed);
					logger.log(Level.FINE, "Attempting to calibrate.");
				}
				else{
					logger.log(Level.WARNING, "Moved while calibrating. Restarting calibration.");
					resetCalibration();
					try {
						Thread.sleep(WAIT_CALIBRATION_FAIL);
					} catch (InterruptedException e) {
						logger.log(Level.WARNING, "Calibration failure wait interrupted.");
					}
				}
				wasCalibrated = false;
			}
			else{
				if(!wasCalibrated){
					logger.log(Level.INFO, "Calibrated successfully.");
				}
				
				updateRobotState(dTime, odometrySpeed, imuSpeed);
				wasCalibrated = true;
			}
		}

		private boolean wasCalibrated;
		public boolean isCalibrated() {
			return calibrationCount >= CALIBRATION_COUNT;
		}

		private void attemptCalibration(RobotSpeed imuSpeed) {
			calibrationCount++;
			imuYawRateAccum += imuSpeed.rotational;
			yawRateOffset = imuYawRateAccum / calibrationCount;
		}
		
		public void resetCalibration(){
			calibrationCount = 0;
			imuYawRateAccum = 0;
			yawRateOffset = 0;
		}

		private void updateRobotState(double dTime, RobotSpeed odometrySpeed, RobotSpeed imuSpeed) {
			// speed complementary filter
			double alpha = 1.0;
			switch(rotationMode){
			case BLENDED:
				alpha = dTime / (TIME_CONSTANT + dTime);
				break;
			case IMU_ONLY:
				alpha = 0.0;
				break;
			default:
				break;
			}
			double deltaHeading = complementryFilter(dTime * odometrySpeed.rotational,
					dTime * (imuSpeed.rotational - yawRateOffset), alpha);
			double blendedHeading = normalizeAngle(heading + deltaHeading);
			double rotationalSpeed = deltaHeading / dTime;

			// delta pose
			double dX = odometrySpeed.forward * dTime * Math.cos(heading);
			double dY = odometrySpeed.forward * dTime * Math.sin(heading);

			// update robot state
			synchronized (instance) {
				speed.forward = odometrySpeed.forward;
				speed.rotational = rotationalSpeed;
				x = x + dX;
				y = y + dY;
				heading = blendedHeading;
			}
		}
		
		private void setRotationMode(RotationMode rotationMode){
			logger.log(Level.INFO, "Setting rotation mode to: " + rotationMode.toString());
			
			this.rotationMode = rotationMode;
		}
		
		private RotationMode getRotationMode(){
			return rotationMode;
		}
	}

	private double complementryFilter(double low, double high, double alpha) {
		return low * alpha + high * (1 - alpha);
	}

	private static final double TWO_PI = 2.0 * Math.PI;

	public static double normalizeAngle(double angle) {
		return (angle + TWO_PI) % TWO_PI;
	}

	public RobotSpeed getIMUSpeed(double forwardSpeed, double dTime) {
		double rotationalSpeed = Math.toRadians(imu.getRateZ());
		rotationalSpeed = Math.signum(rotationalSpeed) * Math.min(Math.abs(rotationalSpeed), 10.0);

		return new RobotSpeed(forwardSpeed + imu.getAccelX() * dTime, -rotationalSpeed);
	}
	
	public RotationMode getRotationMode(){
		return estimator.getRotationMode();
	}
	
	public void setRotationMode(RotationMode rotationMode){
		estimator.setRotationMode(rotationMode);
	}

	@Override
	public void initSubsystem() {
		setRotationMode(RotationMode.BLENDED);
		//imu.calibrate();
	}

	public static class ResetRobotState extends Command {

		private final double x, y, heading;

		public ResetRobotState() {
			this(0, 0, 0);
		}

		public ResetRobotState(double x, double y, double heading) {
			this.setRunWhenDisabled(true);
			this.x = x;
			this.y = y;
			this.heading = heading;
		}

		public ResetRobotState(Pose2D pose) {
			this(pose.x, pose.y, pose.heading);
		}

		@Override
		protected boolean isFinished() {
			RobotState.getInstance().setRobotState(x, y, heading);
			return true;
		}

	}

	private Pose2D distancePose = new Pose2D(0, 0, 0);

	public double getDistance(Pose2D other) {
		Pose2D currentPose = getPose(distancePose);
		double dX = currentPose.x - other.x;
		double dY = currentPose.y - other.y;
		return Math.sqrt(dX * dX + dY * dY);
	}

	@Override
	protected void runningUpdate() {
	}

	@Override
	protected void debugUpdate() {
		SmartDashboard.putNumber("Robot Heading (Degrees)", Math.toDegrees(heading));
	}

	@Override
	protected void startDebug() {
		SmartDashboard.putData("Set Robot Position", new Command() {
			@Override
			protected boolean isFinished() {
				Scheduler.getInstance()
						.add(new ResetRobotState(SmartDashboard.getNumber("New Robot X", 0.0),
								SmartDashboard.getNumber("New Robot Y", 0.0),
								SmartDashboard.getNumber("New Robot Heading", 0.0)));
				return true;
			}
		});
		SmartDashboard.putNumber("New Robot X", 0.0);
		SmartDashboard.putNumber("New Robot Y", 0.0);
		SmartDashboard.putNumber("New Robot Heading", 0.0);
	}

	@Override
	protected void stopDebug() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void initDefaultCommand() {
		// TODO Auto-generated method stub

	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub

	}

}
