/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.util;

import java.util.logging.Level;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public abstract class BearSubsystem extends Subsystem implements SubsystemIF{
	public final Logger logger;
	
	public BearSubsystem(Class clazz){
		logger = Logger.getLogger(clazz.getSimpleName());
		
		SmartDashboard.putData(clazz.getSimpleName() + " Toggle Debug", new ToggleDebugCommand());
	}
	
	public void logDebug(Level level, String message){
		if(debugEnabled || level.intValue() >= Level.WARNING.intValue()){
			logger.log(level, message);
		}
	}
	
	public void logDebug(String message){
		logDebug(Level.INFO, message);
	}
	
	public void init(){
		logger.log(Level.INFO, "Starting initialization...");
		initSubsystem();
		logger.log(Level.INFO, "Initialization finished.");
	}
	protected abstract void initSubsystem();
	
	protected abstract void runningUpdate();
	protected abstract void debugUpdate();
	
	private boolean debugEnabled;
	public void update(){
		runningUpdate();
		
		if(debugEnabled){
			debugUpdate();
		}
	}
	
	public synchronized void setDebugEnabled(boolean debugEnabled){
		if(debugEnabled){
			logger.log(Level.INFO, "Starting debug...");
			startDebug();
			logger.log(Level.INFO, "Debug started.");
		}
		else{
			logger.log(Level.INFO, "Stopping debug...");
			stopDebug();
			logger.log(Level.INFO, "Debug stopped.");
		}

		this.debugEnabled = debugEnabled;
	}
	
	protected abstract void startDebug();
	protected abstract void stopDebug();
	
	private class ToggleDebugCommand extends Command{

		private ToggleDebugCommand(){
			this.setRunWhenDisabled(true);
		}
		
		@Override
		protected boolean isFinished() {
			setDebugEnabled(!debugEnabled);
			
			return true;
		}
	}
}
