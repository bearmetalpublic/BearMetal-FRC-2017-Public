package org.usfirst.frc.team2046.robot.util;

public interface SubsystemIF {
	void update();
	void init();
	void reset();
}
