package org.usfirst.frc.team2046.robot.util;

public class Vector2D implements Comparable<Vector2D>{
	private volatile double x, y;
	
	public Vector2D(double angle){
		this.x = Math.cos(angle);
		this.y = Math.sin(angle);
	}
	
	public Vector2D(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	public Vector2D(Vector2D from, Vector2D to){
		this.x = to.x - from.x;
		this.y = to.y - from.y;
	}
	
	public double length(){
		synchronized(this){
			return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
		}
	}
	
	public double direction(){
		synchronized(this){
			return Math.atan2(y, x);
		}
	}
	
	public Vector2D normalize(){
		double tempLength = length();
		double newX = x / tempLength;
		double newY = y / tempLength;
		
		return new Vector2D(newX, newY);
	}
	
	public double getX(){
		synchronized(this){
			return x;
		}
	}
	
	public double getY(){
		synchronized(this){
			return y;
		}
	}
	
	public void setX(double newX){
		synchronized(this){
			x = newX;
		}
	}
	
	public void setY(double newY){
		synchronized(this){
			y = newY;
		}
	}
	
	public double getDot(Vector2D other){
		return x*other.x + y*other.y;
	}
	
	public Vector2D getAddition(Vector2D other){
		return new Vector2D(x + other.x, y + other.y);
	}
	
	public Vector2D getSubtraction(Vector2D other){
		return new Vector2D(this, other);
	}

	@Override
	public int compareTo(Vector2D o) {
		return (int) Math.round(length() - o.length());
	}
}
