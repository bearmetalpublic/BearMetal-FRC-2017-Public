package org.usfirst.frc.team2046.robot.util;

import java.util.LinkedList;
import java.util.Queue;

public class ToleranceBuffer {
	private final Queue<Double> toleranceBuffer;
	private final int length;
	private volatile boolean onTarget;
	private final double tolerance;
	private double totalError;
	public ToleranceBuffer(int length, double tolerance){
		this.length = length;
		this.tolerance = tolerance;
		toleranceBuffer = new LinkedList<Double>();
	}
	
	public void addError(double newError){
		toleranceBuffer.add(Math.abs(newError));
		totalError += Math.abs(newError);
		
		if(toleranceBuffer.size() > length){
			totalError -= toleranceBuffer.remove();
		}
		
		if(toleranceBuffer.size() > 0 && toleranceBuffer.size() >= length){
			onTarget = totalError / toleranceBuffer.size() < tolerance;
		}
		else{
			onTarget = false;
		}
	}
	
	public boolean onTarget(){		
		return onTarget;
	}
	
	public void clear(){
		toleranceBuffer.clear();
		totalError = 0;
	}
}
