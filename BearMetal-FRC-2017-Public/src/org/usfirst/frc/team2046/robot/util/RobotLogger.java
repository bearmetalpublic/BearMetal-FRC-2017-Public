package org.usfirst.frc.team2046.robot.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import edu.wpi.first.wpilibj.DriverStation;

public class RobotLogger extends Formatter {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	
	public static void initialize() {
		Logger root = Logger.getLogger("");
		for (Handler handler : root.getHandlers()) {
			root.removeHandler(handler);
		}
		Handler handler = new ConsoleHandler();
		handler.setFormatter(new RobotLogger());
		root.addHandler(handler);
	}
	
	@Override
	public String format(LogRecord record) {
		
		
		StringBuffer sb = new StringBuffer();
		sb.append(record.getMillis());

		String message = String.format("%s %s [%s] %s -> %s\n",
				dateFormat.format(new Date(record.getMillis())),
				record.getLevel().getName(),
				Thread.currentThread().getName(),
				record.getLoggerName(),
				record.getMessage());
		
		Level level = record.getLevel();
		
		if (level == Level.SEVERE) {
			DriverStation.reportError(message, false);
		} else if (level == Level.WARNING) {
			DriverStation.reportWarning(message, false);
		}
		
		return message;
	}
}
