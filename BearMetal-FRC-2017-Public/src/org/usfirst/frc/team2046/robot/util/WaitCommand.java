package org.usfirst.frc.team2046.robot.util;

import edu.wpi.first.wpilibj.command.Command;

public class WaitCommand extends Command {

	public WaitCommand(double timeout){
		super(timeout);
	}
	
	@Override
	protected boolean isFinished() {
		return isTimedOut();
	}

}
