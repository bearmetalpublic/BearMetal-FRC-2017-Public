package org.usfirst.frc.team2046.robot.util;

import edu.wpi.first.wpilibj.command.Command;

public abstract class DelayedCallback extends Command {
	
	public DelayedCallback(double delay){
		super(delay);
	}
	
	public abstract void onCallBack();
	
	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return isTimedOut();
	}
	
	protected void isInterrupted(){
		end();
	}
	
	protected void end(){
		onCallBack();
	}

}
