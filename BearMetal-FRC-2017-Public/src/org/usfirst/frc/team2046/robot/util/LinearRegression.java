package org.usfirst.frc.team2046.robot.util;

import java.util.List;

import org.opencv.core.Point;

public class LinearRegression{ 

	public final double beta1, beta0;
    public LinearRegression(List<Point> points) {
        // first pass: read in data, compute xbar and ybar
        double sumx = 0.0, sumy = 0.0, sumx2 = 0.0;
        for(int i = 0; i < points.size(); i++){
            sumx  += points.get(i).x;
            sumx2 += points.get(i).x * points.get(i).x;
            sumy  += points.get(i).y;
        }
        double xbar = sumx / points.size();
        double ybar = sumy / points.size();

        // second pass: compute summary statistics
        double xxbar = 0.0, yybar = 0.0, xybar = 0.0;
        for (int i = 0; i < points.size(); i++) {
            xxbar += (points.get(i).x - xbar) * (points.get(i).x - xbar);
            yybar += (points.get(i).y - ybar) * (points.get(i).y - ybar);
            xybar += (points.get(i).x - xbar) * (points.get(i).y - ybar);
        }
        beta1 = xybar / xxbar;
        beta0 = ybar - beta1 * xbar;

        System.out.println("y   = " + beta1 + " * x + " + beta0);
        // analyze results
        int df = points.size() - 2;
        double rss = 0.0;      // residual sum of squares
        double ssr = 0.0;      // regression sum of squares
        for (int i = 0; i < points.size(); i++) {
            double fit = beta1*points.get(i).x + beta0;
            rss += (fit - points.get(i).y) * (fit - points.get(i).y);
            ssr += (fit - ybar) * (fit - ybar);
        }
        double R2    = ssr / yybar;
        double svar  = rss / df;
        double svar1 = svar / xxbar;
        double svar0 = svar/points.size() + xbar*xbar*svar1;
        System.out.println("R^2                 = " + R2);
        System.out.println("std error of beta_1 = " + Math.sqrt(svar1));
        System.out.println("std error of beta_0 = " + Math.sqrt(svar0));
        svar0 = svar * sumx2 / (points.size() * xxbar);
        System.out.println("std error of beta_0 = " + Math.sqrt(svar0));

        System.out.println("SSTO = " + yybar);
        System.out.println("SSE  = " + rss);
        System.out.println("SSR  = " + ssr);
    }
}