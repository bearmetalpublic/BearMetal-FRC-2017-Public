package org.usfirst.frc.team2046.robot.chassis;

import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.util.SubsystemIF;

import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public abstract class ChassisController implements SubsystemIF {

	//Forward Controller PIDF Values
	private static final double FORWARD_CONTROLLER_P = 0.2;
    private static final double FORWARD_CONTROLLER_I = 0.0;
    private static final double FORWARD_CONTROLLER_D = 15.0;
    private static final double FORWARD_CONTROLLER_F = 2.5;
    
    //Rotation Controller PIDF Values
    private static final double ROTATION_CONTROLLER_P = 1;
    private static final double ROTATION_CONTROLLER_I = 0.0;
    private static final double ROTATION_CONTROLLER_D = 10.0;
    
    private static final double ROTATION_CONTROLLER_CALCULATE_FEED_FORWARD_MULTIPLIER = 21.0;
    private static final double ROTATION_CONTROLLER_CALCULATE_FEED_FORWARD_POWER = 1.0 / 1.6;
    
    //Timer period value
    private static final double CONTROLLER_PERIOD = 0.01;
	
	private final PIDController forwardController;
	private final PIDController rotationalController;
	private volatile double forwardOutput = 0;
	private volatile double rotationalOutput = 0;
	
	public ChassisController() {
		
		PIDSource forwardPIDSource = new PIDSource() {
			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kRate;
			}

			@Override
			public double pidGet() {
				return getSpeed().forward;
			}
		};

		PIDSource rotationalPIDSource = new PIDSource() {
			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kRate;
			}

			@Override
			public double pidGet() {
				return getSpeed().rotational;
			}
		};
		
		PIDOutput forwardPIDOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				forwardOutput = output;
				setOutput(forwardOutput, rotationalOutput);
			}
		};
		PIDOutput rotationalPIDOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				rotationalOutput = output;
				setOutput(forwardOutput, rotationalOutput);
			}
		};

		forwardController = new PIDController(FORWARD_CONTROLLER_P, FORWARD_CONTROLLER_I, 
				FORWARD_CONTROLLER_D, FORWARD_CONTROLLER_F, 
				forwardPIDSource, forwardPIDOutput, CONTROLLER_PERIOD);
		forwardController.setOutputRange(-30.0, 30.0);
		
		rotationalController = new PIDController(ROTATION_CONTROLLER_P, ROTATION_CONTROLLER_I,
				ROTATION_CONTROLLER_D, 0, 
				rotationalPIDSource, rotationalPIDOutput, CONTROLLER_PERIOD) {
			
			protected double calculateFeedForward(){
				return (Math.signum(getSetpoint()) * ROTATION_CONTROLLER_CALCULATE_FEED_FORWARD_MULTIPLIER
						* Math.pow(Math.abs(getSetpoint()), ROTATION_CONTROLLER_CALCULATE_FEED_FORWARD_POWER)) /* * (12.0/131.0)*/;
			}
		};
		rotationalController.setOutputRange(-60.0, 60.0);
		
	}
	
	public void setSetpoint(RobotSpeed speedSetpoint) {
		forwardController.setSetpoint(speedSetpoint.forward);
		rotationalController.setSetpoint(speedSetpoint.rotational);
	}
	
	protected abstract RobotSpeed getSpeed();
	
	protected abstract void changeControlMode(TalonControlMode controlMode);

	protected abstract void setOutput(double forwardOutput, double rotationalOutput);

	public void enable(boolean enable) {
		if (enable) { 
			enable(); 
		} else {
			disable();
		}
	}
	
	public void disable() {
		if (forwardController.isEnabled() || rotationalController.isEnabled()) {
			forwardController.disable();
			rotationalController.disable();
			setSetpoint(new RobotSpeed());
			changeControlMode(TalonControlMode.PercentVbus);
		}
	}

	public void enable() {
		if (!forwardController.isEnabled() || !rotationalController.isEnabled()) {
			forwardController.enable();
			rotationalController.enable();
			changeControlMode(TalonControlMode.Current);
		}
	}
	
	public void init(){
		SmartDashboard.putNumber("ForwardControllerP", forwardController.getP());
		SmartDashboard.putNumber("ForwardControllerI", forwardController.getI());
		SmartDashboard.putNumber("ForwardControllerD", forwardController.getD());
		SmartDashboard.putNumber("ForwardControllerF", forwardController.getF());
		
		SmartDashboard.putNumber("RotationalControllerP", rotationalController.getP());
		SmartDashboard.putNumber("RotationalControllerI", rotationalController.getI());
		SmartDashboard.putNumber("RotationalControllerD", rotationalController.getD());
		SmartDashboard.putNumber("RotationalControllerF", ROTATION_CONTROLLER_CALCULATE_FEED_FORWARD_MULTIPLIER);
	}
	
	public void update(){
		forwardController.setPID(SmartDashboard.getNumber("ForwardControllerP", forwardController.getP()),
				SmartDashboard.getNumber("ForwardControllerI", forwardController.getI()),
				SmartDashboard.getNumber("ForwardControllerD", forwardController.getD()),
				SmartDashboard.getNumber("ForwardControllerF", forwardController.getF()));
		
		rotationalController.setPID(SmartDashboard.getNumber("RotationalControllerP", rotationalController.getP()),
				SmartDashboard.getNumber("RotationalControllerI", rotationalController.getI()),
				SmartDashboard.getNumber("RotationalControllerD", rotationalController.getD()));
		
		SmartDashboard.putNumber("Forward Output Current", forwardOutput);
		SmartDashboard.putNumber("Rotational Output Current", rotationalOutput);
		SmartDashboard.putNumber("Forward PID Error", forwardController.getError());
		SmartDashboard.putNumber("Rotational PID Error", rotationalController.getError());
	}
}
