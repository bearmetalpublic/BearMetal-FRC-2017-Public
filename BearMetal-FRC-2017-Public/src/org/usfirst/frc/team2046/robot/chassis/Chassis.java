/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.chassis;

import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.chassis.CANDrive.MotorParameter;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;
import org.usfirst.frc.team2046.robot.util.RobotProperties;

import com.ctre.CANTalon.TalonControlMode;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Chassis extends BearSubsystem implements RobotSpeedCommandIF {
	private static final double TURN_COMMAND_FUDGE_FACTOR_DEFAULT = Math.toRadians(0.5);
	
	// Encoder Conversions
	private static final double WHEEL_DIA = 3.05; // inches
	private static final double WHEEL_CIRCUM = WHEEL_DIA * Math.PI * 0.0254; // meters
	private static final double RPM_TO_MPS = WHEEL_CIRCUM / 60.0;
	private static final double WHEELBASE_WIDTH = 27.336 * 0.0254 / 1.333;

	private static final double HORIZONTAL_POWER = 1.6;
	private static final double FORWARD_POWER = 1.5;

	public static final double DEFAULT_MAX_ACCELERATION = 2.0;

	private static final int CURRENT_LIMIT = 25;

	private double forwardPowerCurve = FORWARD_POWER;
	private double rotationalPowerCurve = HORIZONTAL_POWER;

	public double getForwardPowerCurve() {
		return forwardPowerCurve;
	}

	public double getRotationalPowerCurve() {
		return rotationalPowerCurve;
	}

	// Singleton Pattern
	private static Chassis singleton = new Chassis();

	public static Chassis getInstance() {
		return singleton;
	}

	private final CANDrive leftDrive;
	private final CANDrive rightDrive;
	private ChassisController chassisController = null;
	private RobotSpeed currentSpeed = new RobotSpeed();
	private RobotProperties properties = new RobotProperties(Chassis.class.getSimpleName());

	private Chassis() {
		super(Chassis.class);

		CANDrive leftDrive = null;
		CANDrive rightDrive = null;

		try {
			leftDrive = new CANDrive(
					new MotorParameter[] { new MotorParameter(RobotMap.MOTOR_CHASSIS_LEFT_MIDDLE, false),
							new MotorParameter(RobotMap.MOTOR_CHASSIS_LEFT_FRONT, true),
							new MotorParameter(RobotMap.MOTOR_CHASSIS_LEFT_BACK, false), });
			leftDrive.invert();
			leftDrive.setCurrentLimit(CURRENT_LIMIT);

			rightDrive = new CANDrive(
					new MotorParameter[] { new MotorParameter(RobotMap.MOTOR_CHASSIS_RIGHT_MIDDLE, false),
							new MotorParameter(RobotMap.MOTOR_CHASSIS_RIGHT_FRONT, true),
							new MotorParameter(RobotMap.MOTOR_CHASSIS_RIGHT_BACK, false), });
			rightDrive.reverseSensor();
			rightDrive.setCurrentLimit(CURRENT_LIMIT);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}

		this.leftDrive = leftDrive;
		this.rightDrive = rightDrive;
	}

	@Override
	public void initSubsystem() {
		enableControl(false);
		if(!properties.load()){
			setTurnCommandFudgeFactor(TURN_COMMAND_FUDGE_FACTOR_DEFAULT);
		}
	}
	
	public double getTurnCommandFudgeFactor(){
		return Double.parseDouble(properties.getProperty("turnCommandFudgeFactor"));
	}
	
	public void setTurnCommandFudgeFactor(double fudgeFactor){
		properties.setProperty("turnCommandFudgeFactor", Double.toString(fudgeFactor));
		properties.save();
	}
	
	public double increaseTurnCommandFudgeFactor(){
		setTurnCommandFudgeFactor(getTurnCommandFudgeFactor() + Math.toRadians(0.5));
		return getTurnCommandFudgeFactor();
	}
	
	public double decreaseTurnCommandFudgeFactor(){
		setTurnCommandFudgeFactor(getTurnCommandFudgeFactor() - Math.toRadians(0.5));
		return getTurnCommandFudgeFactor();
	}

	public void setSpeed(RobotSpeed speedSetpoint) {
		setSpeed(speedSetpoint, DEFAULT_MAX_ACCELERATION);
	}

	@Override
	public void setSpeed(RobotSpeed speedSetpoint, double maximumAcceleration) {
		setSpeed(speedSetpoint, maximumAcceleration, 0.01);
	}

	public void setSpeed(RobotSpeed speedSetpoint, double maximumAcceleration, double dT) {
		double reduce = checkMaximumAcceleration(speedSetpoint, maximumAcceleration, dT);
		speedSetpoint.reduce(reduce);
		enableControl(true);

		logDebug("Setting speed to: " + speedSetpoint.forward + "m/s" + speedSetpoint.rotational + "rad/s");

		currentSpeed.copyFrom(speedSetpoint);
		chassisController.setSetpoint(speedSetpoint);
	}

	private double checkMaximumAcceleration(RobotSpeed newSpeed, double maximumAcceleration, double dT) {
		double percentOver = 1.0;

		if (Double.compare(newSpeed.forward, 0.0) != 0) {
			double dV = newSpeed.forward - currentSpeed.forward;

			double accelerationConstant = maximumAcceleration * dT;
			double velocityOut = currentSpeed.forward
					+ Math.min(Math.max(dV, -accelerationConstant), accelerationConstant);
			percentOver = velocityOut / newSpeed.forward;

		}

		return percentOver;
	}

	private void enableControl(boolean enable) {
		if (chassisController == null) {
			chassisController = new ChassisController() {

				private final RobotState robotState = RobotState.getInstance();
				private final RobotSpeed speed = new RobotSpeed();

				@Override
				protected RobotSpeed getSpeed() {
					robotState.getSpeed(speed);
					return speed;
				}

				@Override
				protected void setOutput(double forwardOutput, double rotationalOutput) {
					double outputDelta = rotationalOutput * WHEELBASE_WIDTH;
					leftDrive.set(forwardOutput - outputDelta);
					rightDrive.set(forwardOutput + outputDelta);
				}

				@Override
				public void reset() {

				}

				@Override
				public void changeControlMode(TalonControlMode controlMode) {
					leftDrive.changeControlMode(controlMode);
					rightDrive.changeControlMode(controlMode);

					if (controlMode == TalonControlMode.PercentVbus) {
						currentSpeed = new RobotSpeed(0, 0);
						leftDrive.set(0.0);
						rightDrive.set(0.0);
					}
				}
			};
		}
		chassisController.enable(enable);
	}

	public RobotSpeed getOdometrySpeed() {
		double leftSpeed = leftDrive.getSpeed() * RPM_TO_MPS;
		double rightSpeed = rightDrive.getSpeed() * RPM_TO_MPS;
		double forwardSpeed = (rightSpeed + leftSpeed) / 2.0;
		double rotationalSpeed = (rightSpeed - leftSpeed) / 2.0 / WHEELBASE_WIDTH;

		return new RobotSpeed(forwardSpeed, rotationalSpeed);
	}

	public double getAverageEncoderValue() {
		return (leftDrive.getPosition() + rightDrive.getPosition()) / 2.0;
	}

	public double getAverageEncoderDistanceInMeters() {
		return getAverageEncoderValue() * WHEEL_CIRCUM;
	}

	public void stopController() {
		enableControl(false);
	}

	public void initDefaultCommand() {
		setDefaultCommand(new TankDriveCommand());
	}

	public void setDrivePower(double lpower, double rpower) {
		stopController();
		leftDrive.set(lpower);
		rightDrive.set(rpower);
	}

	public void setLeftMotor(double lpower) {
		stopController();
		leftDrive.set(lpower);
	}

	public void setRightMotor(double rpower) {
		stopController();
		leftDrive.set(rpower);
	}

	public boolean isStopped() {
		return leftDrive.getSpeed() == 0.0 && rightDrive.getSpeed() == 0.0;
	}
	
	public void zeroEncoderDistance(){
		leftDrive.zeroEncoderDistance();
		rightDrive.zeroEncoderDistance();
	}

	@Override
	protected void runningUpdate() {
	}

	RobotSpeed speed = new RobotSpeed();

	@Override
	protected void debugUpdate() {
		SmartDashboard.putNumber("Left Drive Current", leftDrive.getOutputCurrent());
		SmartDashboard.putNumber("Right Drive Current", rightDrive.getOutputCurrent());

		RobotState.getInstance().getSpeed(speed);

		SmartDashboard.putNumber("ForwardSpeed", speed.forward);
		SmartDashboard.putNumber("RotationalSpeed", speed.rotational);

		DriveForwardCommand.DISTANCE_CONTROLLER_P = SmartDashboard.getNumber("DistanceControllerP",
				DriveForwardCommand.DISTANCE_CONTROLLER_P);
		DriveForwardCommand.DISTANCE_CONTROLLER_I = SmartDashboard.getNumber("DistanceControllerI",
				DriveForwardCommand.DISTANCE_CONTROLLER_I);
		DriveForwardCommand.DISTANCE_CONTROLLER_D = SmartDashboard.getNumber("DistanceControllerD",
				DriveForwardCommand.DISTANCE_CONTROLLER_D);
		DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_P = SmartDashboard.getNumber("KeepFromTurningControllerP",
				DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_P);
		DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_I = SmartDashboard.getNumber("KeepFromTurningControllerI",
				DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_I);
		DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_D = SmartDashboard.getNumber("KeepFromTurningControllerD",
				DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_D);

		TurnCommand.ALIGN_PID_CONTROLLER_P = SmartDashboard.getNumber("AlignPIDControllerP",
				TurnCommand.ALIGN_PID_CONTROLLER_P);
		TurnCommand.ALIGN_PID_CONTROLLER_I = SmartDashboard.getNumber("AlignPIDControllerI",
				TurnCommand.ALIGN_PID_CONTROLLER_I);
		TurnCommand.ALIGN_PID_CONTROLLER_D = SmartDashboard.getNumber("AlignPIDControllerD",
				TurnCommand.ALIGN_PID_CONTROLLER_D);
		TurnCommand.KEEP_PID_CONTROLLER_P = SmartDashboard.getNumber("KeepPIDControllerP",
				TurnCommand.KEEP_PID_CONTROLLER_P);
		TurnCommand.KEEP_PID_CONTROLLER_I = SmartDashboard.getNumber("KeepPIDControllerI",
				TurnCommand.KEEP_PID_CONTROLLER_I);
		TurnCommand.KEEP_PID_CONTROLLER_D = SmartDashboard.getNumber("KeepPIDControllerD",
				TurnCommand.KEEP_PID_CONTROLLER_D);

		chassisController.update();
	}

	@Override
	protected void startDebug() {
		chassisController.init();

		SmartDashboard.putNumber("RotationalPIDSetpoint", 0.0);
		SmartDashboard.putNumber("ForwardPIDSetpoint", 0.0);
		SmartDashboard.putData("ChassisControllerPIDRun", new Command() {
			{
				requires(singleton);
			}

			@Override
			protected void initialize() {
				setSpeed(new RobotSpeed(SmartDashboard.getNumber("ForwardPIDSetpoint", 0.0),
						SmartDashboard.getNumber("RotationalPIDSetpoint", 0.0)));
			}

			@Override
			protected boolean isFinished() {
				return false;
			}

			@Override
			protected void interrupted() {
				end();
			}

			@Override
			protected void end() {
				stopController();
			}
		});

		SmartDashboard.putNumber("Rotate setpoint", 0.0);
		SmartDashboard.putData("TurnToCommand", new TurnCommand(false, false) {
			@Override
			protected double getAngle() {
				Pose2D robotPose = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));

				return robotPose.heading + Math.toRadians(SmartDashboard.getNumber("Rotate setpoint", 0.0));
			}
		});

		SmartDashboard.putData("DrivePreciselyCommand", new Command() {
			@Override
			protected boolean isFinished() {
				Scheduler.getInstance()
						.add(new DrivePreciselyCommand(
								new DrivePreciselyCommand.DriveStage(
										SmartDashboard.getNumber("DrivePreciselyArcLengthFirst", 0.0),
										SmartDashboard.getNumber("DrivePreciselyArcAngleFirst", 0.0)),
								new DrivePreciselyCommand.DriveStage(
										SmartDashboard.getNumber("DrivePreciselyArcLengthSecond", 0.0),
										SmartDashboard.getNumber("DrivePreciselyArcAngleSecond", 0.0))));
				return true;
			}
		});

		SmartDashboard.putNumber("DrivePreciselyArcLengthFirst", 0.0);
		SmartDashboard.putNumber("DrivePreciselyArcAngleFirst", 0.0);
		SmartDashboard.putNumber("DrivePreciselyArcLengthSecond", 0.0);
		SmartDashboard.putNumber("DrivePreciselyArcAngleSecond", 0.0);

		SmartDashboard.putNumber("DistanceControllerP", DriveForwardCommand.DISTANCE_CONTROLLER_P);
		SmartDashboard.putNumber("DistanceControllerI", DriveForwardCommand.DISTANCE_CONTROLLER_I);
		SmartDashboard.putNumber("DistanceControllerD", DriveForwardCommand.DISTANCE_CONTROLLER_D);
		SmartDashboard.putNumber("KeepFromTurningControllerP", DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_P);
		SmartDashboard.putNumber("KeepFromTurningControllerI", DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_I);
		SmartDashboard.putNumber("KeepFromTurningControllerD", DriveForwardCommand.KEEP_FROM_TURNING_CONTROLLER_D);

		SmartDashboard.putNumber("AlignPIDControllerP", TurnCommand.ALIGN_PID_CONTROLLER_P);
		SmartDashboard.putNumber("AlignPIDControllerI", TurnCommand.ALIGN_PID_CONTROLLER_I);
		SmartDashboard.putNumber("AlignPIDControllerD", TurnCommand.ALIGN_PID_CONTROLLER_D);
		SmartDashboard.putNumber("KeepPIDControllerP", TurnCommand.KEEP_PID_CONTROLLER_P);
		SmartDashboard.putNumber("KeepPIDControllerI", TurnCommand.KEEP_PID_CONTROLLER_I);
		SmartDashboard.putNumber("KeepPIDControllerD", TurnCommand.KEEP_PID_CONTROLLER_D);
	}

	@Override
	protected void stopDebug() {

	}

	@Override
	public void reset() {
		stopController();
	}
}