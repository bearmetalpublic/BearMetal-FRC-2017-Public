package org.usfirst.frc.team2046.robot.chassis;

import edu.wpi.first.wpilibj.command.Command;

public class ZeroEncoderDistance extends Command {
	@Override
	protected boolean isFinished() {
		Chassis.getInstance().zeroEncoderDistance();
		return true;
	}

}
