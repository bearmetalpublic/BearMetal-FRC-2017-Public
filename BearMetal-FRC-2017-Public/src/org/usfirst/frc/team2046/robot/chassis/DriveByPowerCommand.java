package org.usfirst.frc.team2046.robot.chassis;

import java.util.logging.Level;

import edu.wpi.first.wpilibj.command.Command;

public class DriveByPowerCommand extends Command {

	private final double lpower, rpower;
	private final Chassis chassis;
	private final boolean noTimeout;
	public DriveByPowerCommand(double timeout, double lpower, double rpower){
		this(timeout, lpower, rpower, false);
	}
	
	public DriveByPowerCommand(double timeout, double lpower, double rpower, boolean noTimeout){
		super(timeout);
		this.lpower = lpower;
		this.rpower = rpower;
		this.noTimeout = noTimeout;
		
		chassis = Chassis.getInstance();
		requires(chassis);
	}
	
	@Override
	protected void initialize(){
		chassis.logger.log(Level.INFO, "Starting drive by power command.");
		
		chassis.setDrivePower(lpower, rpower);
	}
	
	@Override
	protected boolean isFinished() {
		return isTimedOut() && !noTimeout;
	}
	
	@Override
	protected void end(){
		chassis.logger.log(Level.INFO, "Finished drive by power command.");
		
		chassis.setDrivePower(0.0, 0.0);
	}
}
