package org.usfirst.frc.team2046.robot.chassis;

import java.util.ArrayList;
import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.FieldData;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.util.ToleranceBuffer;
import org.usfirst.frc.team2046.robot.util.Vector2D;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public abstract class AlignWithDriveStagesCommand extends Command {
	private static final int ALIGN_CONTROLLER_TOLERANCE_BUFFER_LENGTH = 2;
	private static final double ALIGN_CONTROLLER_ABSOLUTE_TOLERANCE = Math.toRadians(0.5);
	private static final double ALIGN_CONTROLLER_PERIOD = 0.02;
	
	private static final double ALIGN_CONTROLLER_P = 8.0;
	private static final double ALIGN_CONTROLLER_I = 0.0;
	private static final double ALIGN_CONTROLLER_D = 0.0;
	private static final double ALIGN_CONTROLLER_F = 0.0;
	private final PIDController alignController;
	protected final Chassis chassis;
	
	private DriveStage[] stages;
	private volatile int currentStageIndex;
	private double pastTime;
	
	private ToleranceBuffer toleranceBuffer;
	public AlignWithDriveStagesCommand(DriveStage...stages){
		this.stages = stages;
		currentStageIndex = 0;
		
		PIDSource alignSource = new PIDSource(){
			Pose2D pose = new Pose2D(0, 0, 0);

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}

			@Override
			public double pidGet() {
				return RobotState.getInstance().getPose(pose).heading;
			}
		};
		
		PIDOutput alignOutput = new PIDOutput(){
			@Override
			public void pidWrite(double output) {
				if(currentStageIndex < stages.length){
					System.out.println(output + " " + alignController.getError());
					
					RobotSpeed speed = getCurrentStage().speed;
					speed = new RobotSpeed(speed.forward, output);
					chassis.setSpeed(speed, 2.0, 0.5);
				}
				else{
					chassis.setSpeed(new RobotSpeed(0, output));
				}
				
				toleranceBuffer.addError(alignController.getError());
			}			
		};
		
		chassis = Chassis.getInstance();
		alignController = new PIDController(ALIGN_CONTROLLER_P, ALIGN_CONTROLLER_I, ALIGN_CONTROLLER_D, ALIGN_CONTROLLER_F, alignSource, alignOutput, ALIGN_CONTROLLER_PERIOD);
		alignController.setAbsoluteTolerance(ALIGN_CONTROLLER_ABSOLUTE_TOLERANCE);
		alignController.setInputRange(0.0, 2.0 * Math.PI);
		alignController.setContinuous();
		
		toleranceBuffer = new ToleranceBuffer(ALIGN_CONTROLLER_TOLERANCE_BUFFER_LENGTH, ALIGN_CONTROLLER_ABSOLUTE_TOLERANCE);
		
		requires(chassis);
	}
	
	@Override
	protected void initialize(){
		chassis.logger.log(Level.INFO, "Starting AlignWithDriveStagesCommand.");
		pastTime = System.currentTimeMillis();
		
		alignController.setSetpoint(getEndingAngle());
		alignController.enable();
	}
	
	private DriveStage getCurrentStage(){
		return stages[currentStageIndex];
	}
	
	private void updateCurrentStage(double timeInMillis){
		if(stages[currentStageIndex].delayInMillis - timeInMillis < 0){
			chassis.logger.log(Level.INFO, "Changing stage of AlignWithDriveStagesCommand. StageIndex: " + currentStageIndex + 1);
			currentStageIndex++;
		}
		else{
			stages[currentStageIndex].delayInMillis -= timeInMillis;
		}
	}
	
	protected abstract double getEndingAngle();
	
	@Override
	protected void execute(){
		if(currentStageIndex < stages.length){
			double currentTime = System.currentTimeMillis();
			updateCurrentStage(currentTime - pastTime);
			pastTime = currentTime;
		}
	}
	
	@Override
	protected boolean isFinished() {
		return currentStageIndex >= stages.length;
	}
	
	@Override
	protected void end(){
		chassis.logger.log(Level.INFO, "Finished AlignWithDriveStagesCommand.");
		currentStageIndex = 0;
		alignController.disable();
		chassis.stopController();
		toleranceBuffer.clear();
	}
	
	public static class DriveStage{
		private double delayInMillis;
		private final RobotSpeed speed;
		public DriveStage(double delayInMillis, double forwardSpeed){
			this(delayInMillis, new RobotSpeed(forwardSpeed, 0.0));
		}
		
		public DriveStage(double delayInMillis, RobotSpeed speed){
			this.delayInMillis = delayInMillis;
			this.speed = speed;
		}
	}
	
	public static abstract class TurnTo extends AlignWithDriveStagesCommand {
		public TurnTo(DriveStage...driveStages) {
			super(driveStages);
		}

		@Override
		protected double getEndingAngle() {
			Pose2D robotPose = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));

			chassis.logger.log(Level.INFO,
					"Y: " + getY() + "| robotY: " + robotPose.y + "| X: " + getX() + "| robotX: " + robotPose.x);

			return Math.atan2(getY() - robotPose.y, getX() - robotPose.x);
		}
		
		protected abstract double getX();
		protected abstract double getY();
	}
	
	public static class ByDeadReckoning extends TurnTo {
		private final Vision.Mode mode;

		public ByDeadReckoning(Vision.Mode mode, DriveStage...driveStages) {
			super(driveStages);

			this.mode = mode;
		}

		@Override
		protected double getX() {
			double toReturn = 0.0;

			switch (mode) {
			case BOILER_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the boiler x.");
				toReturn = FieldData.Map.GetBoilerX();
				break;
			case GEAR_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the gear peg x.");
				toReturn = getClosestGearPeg().getX();
				break;
			case OFF:
			default:
				chassis.logger.log(Level.WARNING, "Invalid vision mode.");
				break;
			}

			return toReturn;
		}

		@Override
		protected double getY() {
			double toReturn = 0.0;

			switch (mode) {
			case BOILER_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the boiler y.");
				toReturn = FieldData.Map.GetBoilerY();
				break;
			case GEAR_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the gear peg y.");
				toReturn = getClosestGearPeg().getY();
				break;
			case OFF:
			default:
				chassis.logger.log(Level.WARNING, "Invalid vision mode.");
				break;
			}

			return toReturn;
		}

		private Vector2D getClosestGearPeg() {
			Pose2D current = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));

			Vector2D currentPose = new Vector2D(current.x, current.y);

			ArrayList<Vector2D> toPegVectors = new ArrayList<Vector2D>();
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetBoilerPegX() - current.x, FieldData.Map.GetBoilerPegY() - current.y));
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetCenterPegX() - current.x, FieldData.Map.GetCenterPegY() - current.y));
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetFeederPegX() - current.x, FieldData.Map.GetFeederPegY() - current.y));

			toPegVectors.sort(null);
			Vector2D closestPeg = null;

			if (toPegVectors.get(0).normalize().getDot(currentPose.normalize()) < toPegVectors.get(1).normalize()
					.getDot(currentPose.normalize())) {
				closestPeg = toPegVectors.get(0);
			} else {
				closestPeg = toPegVectors.get(1);
			}

			return closestPeg.getAddition(currentPose);
		}
	}
}
