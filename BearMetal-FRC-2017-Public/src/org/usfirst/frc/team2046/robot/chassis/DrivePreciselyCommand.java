package org.usfirst.frc.team2046.robot.chassis;

import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.RobotState.RotationMode;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public class DrivePreciselyCommand extends Command {
	public static class DriveStage {
		private final double distance;
		private final double distanceToAngleRatio;
		private final double angle;

		public DriveStage(double distance, double angle) {
			this.distance = distance;
			this.angle = angle;

			if (distance != 0.0)
				distanceToAngleRatio = angle / distance;
			else
				distanceToAngleRatio = 0.0;
		}

		public double getProgress(double currentDistance) {
			return Math.min(currentDistance / distance, 1.0);
		}

		public boolean isComplete(double currentDistance) {
			return getProgress(currentDistance) >= 1.0;
		}

		public double getAngleSetpoint(double currentDistance, double startingHeading) {
			return RobotState.normalizeAngle(startingHeading + currentDistance * distanceToAngleRatio);
		}
	}

	private static final double MAX_ACCELERATION_DEFAULT = 2.0;
	private static final double SPEED_DEFAULT = 1.0;

	private static final double ANGLE_CONTROLLER_P = 2.0;
	private static final double ANGLE_CONTROLLER_I = 0.0;
	private static final double ANGLE_CONTROLLER_D = 5.0;
	private static final double ANGLE_CONTROLLER_F = 0.0;
	private static final double ANGLE_CONTROLLER_PERIOD = 0.020;

	private final DriveStage[] stages;
	private final PIDController angleController;
	private final Chassis chassis;
	private final double maxAccel;
	private final double totalDistance;

	private volatile int currentDriveStage;
	private volatile double currentAbsoluteDistance, startingDistance, currentDistanceInStage, startingAngle;
	private volatile double angleControllerOutput;

	public DrivePreciselyCommand(DriveStage... stages) {
		this(SPEED_DEFAULT, MAX_ACCELERATION_DEFAULT, stages);
	}

	public DrivePreciselyCommand(double speed, DriveStage... stages) {
		this(speed, MAX_ACCELERATION_DEFAULT, stages);
	}

	public DrivePreciselyCommand(double speed, double maxAcceleration, DriveStage... stages) {
		this.stages = stages;

//		this.speed = speed;
		this.maxAccel = maxAcceleration;

		this.totalDistance = getTotalDistance();

		PIDSource angleSource = new PIDSource() {
			Pose2D robotPose = new Pose2D(0, 0, 0);

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}

			@Override
			public double pidGet() {
				return RobotState.getInstance().getPose(robotPose).heading;
			}
		};

		PIDOutput angleOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				angleControllerOutput = output;

				update();
				
				if(currentDriveStage < stages.length){
					chassis.logger.log(Level.INFO,
							String.format(
									"Speed: %6.3f AngleOutput: %6.3f CurrentDistance: %6.3f RotationControllerSetpoint: %6.3f RotationControllerError: %6.3f",
									speed, output, currentDistanceInStage, angleController.getSetpoint(), angleController.getError()));
					
					chassis.setSpeed(getScaledSpeed(new RobotSpeed(speed, angleControllerOutput)), maxAccel, ANGLE_CONTROLLER_PERIOD);
				}
			}
		};

		angleController = new PIDController(ANGLE_CONTROLLER_P, ANGLE_CONTROLLER_I, ANGLE_CONTROLLER_D,
				ANGLE_CONTROLLER_F, angleSource, angleOutput, ANGLE_CONTROLLER_PERIOD);
		angleController.setOutputRange(-10.0, 10.0);
		angleController.setInputRange(0.0, Math.PI * 2);
		angleController.setContinuous();

		chassis = Chassis.getInstance();
		requires(chassis);
	}

	private double getTotalDistance() {
		double totalDistance = 0;
		for (DriveStage stage : stages) {
			totalDistance += Math.abs(stage.distance);
		}
		return totalDistance;
	}

	private DriveStage getCurrentDriveStage() {
		return (currentDriveStage < stages.length) ? stages[currentDriveStage] : stages[stages.length - 1];
	}

	private void updateCurrentDriveStage() {
		if (currentDriveStage < stages.length && getCurrentDriveStage().isComplete(currentDistanceInStage)) {
			chassis.logger.log(Level.INFO, "Changing Driver Stage to: " + (currentDriveStage + 1));
			startingAngle += getCurrentDriveStage().angle;
			currentDriveStage++;
		}
	}

	public double getCurrentDistanceInStage() {
		double currentDistance = Math.abs(this.currentAbsoluteDistance - this.startingDistance);

		for (int i = 0; i < currentDriveStage; i++) {
			currentDistance -= stages[i].distance;
		}

		return currentDistance;
	}

	public double getRemainingDistance() {
		return totalDistance - Math.abs(this.currentAbsoluteDistance - this.startingDistance);
	}

	private void updateSetpoints() {
		DriveStage currentStage = getCurrentDriveStage();

		angleController.setSetpoint(currentStage.getAngleSetpoint(currentDistanceInStage, startingAngle));
	}

	private void update() {
		currentAbsoluteDistance = chassis.getAverageEncoderDistanceInMeters();
		currentDistanceInStage = getCurrentDistanceInStage();
		updateCurrentDriveStage();
		updateSetpoints();
	}

	private RobotSpeed getScaledSpeed(RobotSpeed askedSpeed) {
		double maxVelocity = Math.sqrt(2 * maxAccel * getRemainingDistance());

		askedSpeed.forward *= Math.min(1.0, Math.abs(maxVelocity / askedSpeed.forward));
		return askedSpeed;
	}

	@Override
	protected void initialize() {
		RobotState.getInstance().setRotationMode(RotationMode.IMU_ONLY);
		currentDriveStage = 0;
		startingDistance = chassis.getAverageEncoderDistanceInMeters();
		startingAngle = RobotState.getInstance().getPose(new Pose2D(0, 0, 0)).heading;
		updateSetpoints();
		angleController.enable();
	}

	@Override
	protected boolean isFinished() {
		return currentDriveStage >= stages.length;
	}

	@Override
	protected void end() {
		RobotState.getInstance().setRotationMode(RotationMode.BLENDED);
		angleController.disable();
		chassis.stopController();
	}

}
