/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.chassis;

import java.util.ArrayList;
import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.FieldData;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.RobotState.RotationMode;
import org.usfirst.frc.team2046.robot.util.ToleranceBuffer;
import org.usfirst.frc.team2046.robot.util.Vector2D;
import org.usfirst.frc.team2046.robot.vision.SetVisionCommand;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;

/**
 *
 */
public abstract class TurnCommand extends Command {
	private static final double ALIGN_SPEED_LIMIT = 4.0;
	private static final double ALIGN_STOP_THRESHOLD = Math.toRadians(0.5);
	private static final int ALIGN_TOLERANCE_BUFFER = 5;
	private static final double ALIGN_TIMEOUT = 5.0;
	private static final double ALIGN_PID_CONTROLLER_PERIOD = 0.02;
	private static final double KEEP_PID_CONTROLLER_PERIOD = 0.02;
	
	public static double ALIGN_PID_CONTROLLER_P = 8.0;
	public static double ALIGN_PID_CONTROLLER_I = 0.0;
	public static double ALIGN_PID_CONTROLLER_D = 8.0;
	public static double ALIGN_PID_CONTROLLER_F = 0.0;

	public static double KEEP_PID_CONTROLLER_P = 2.0;
	public static double KEEP_PID_CONTROLLER_I = 0.0;
	public static double KEEP_PID_CONTROLLER_D = 0.0;
	public static double KEEP_PID_CONTROLLER_F = 0.0;

	protected final ToleranceBuffer toleranceBuffer;
	protected final Chassis chassis;
	private final PIDController alignController, keepInPlaceController;
	private final boolean noTurnInPlace, useFudgeFactor;
	private final double forwardDistance;

	private volatile double alignPIDOutput, keepInPlacePIDOutput;
	
	public TurnCommand(boolean noTurnInPlace, boolean useFudgeFactor){
		this(noTurnInPlace, useFudgeFactor, 0.0);
	}

	public TurnCommand(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance) {
		super(ALIGN_TIMEOUT);

		this.forwardDistance = forwardDistance;
		this.noTurnInPlace = noTurnInPlace;
		this.useFudgeFactor = useFudgeFactor;
		toleranceBuffer = new ToleranceBuffer(ALIGN_TOLERANCE_BUFFER, ALIGN_STOP_THRESHOLD);

		PIDSource alignSource = new PIDSource() {
			private Pose2D robotPose = new Pose2D(0, 0, 0);

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}

			@Override
			public double pidGet() {
				return RobotState.getInstance().getPose(robotPose).heading;
			}

		};

		PIDOutput alignOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				alignPIDOutput = output;

				System.out.println(alignPIDOutput + " " + alignController.getError() + " " + keepInPlacePIDOutput + " " + keepInPlaceController.getError());

				toleranceBuffer.addError(alignController.getError());
				
				chassis.setSpeed(new RobotSpeed(keepInPlacePIDOutput, alignPIDOutput));
			}
		};

		alignController = new PIDController(ALIGN_PID_CONTROLLER_P, ALIGN_PID_CONTROLLER_I, ALIGN_PID_CONTROLLER_D,
				ALIGN_PID_CONTROLLER_F, alignSource, alignOutput, ALIGN_PID_CONTROLLER_PERIOD);
		alignController.setAbsoluteTolerance(ALIGN_STOP_THRESHOLD);
		alignController.setOutputRange(-ALIGN_SPEED_LIMIT, ALIGN_SPEED_LIMIT);
		alignController.setInputRange(0.0, 2.0 * Math.PI);
		alignController.setContinuous();

		PIDSource keepSource = new PIDSource() {

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {

			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}

			@Override
			public double pidGet() {
				return chassis.getAverageEncoderDistanceInMeters();
			}
		};

		PIDOutput keepOutput = new PIDOutput() {

			@Override
			public void pidWrite(double output) {
				keepInPlacePIDOutput = output;

				chassis.setSpeed(new RobotSpeed(keepInPlacePIDOutput, alignPIDOutput));
			}
		};

		keepInPlaceController = new PIDController(KEEP_PID_CONTROLLER_P, KEEP_PID_CONTROLLER_I, KEEP_PID_CONTROLLER_D,
				KEEP_PID_CONTROLLER_F, keepSource, keepOutput, KEEP_PID_CONTROLLER_PERIOD);
		keepInPlaceController.setOutputRange(-ALIGN_SPEED_LIMIT, ALIGN_SPEED_LIMIT);

		this.chassis = Chassis.getInstance();
		requires(chassis);
	}

	protected void initialize() {
		alignController.setPID(ALIGN_PID_CONTROLLER_P, ALIGN_PID_CONTROLLER_I, ALIGN_PID_CONTROLLER_D, ALIGN_PID_CONTROLLER_F);
		
		double turnTo = RobotState.normalizeAngle(getAngle());

		chassis.logger.log(Level.INFO, "Turning to " + turnTo);
		alignController.setSetpoint(turnTo + (useFudgeFactor ? chassis.getTurnCommandFudgeFactor() : 0.0));
		if (!alignController.isEnabled()) {
			RobotState.getInstance().setRotationMode(RotationMode.IMU_ONLY);
			alignController.enable();
		}

		if (!noTurnInPlace) {
			keepInPlaceController.setSetpoint(chassis.getAverageEncoderDistanceInMeters() + forwardDistance);
			if (!keepInPlaceController.isEnabled()) {
				keepInPlaceController.enable();
			}
		} else {
			chassis.logger.log(Level.WARNING, "Keep in place controller turned off.");
		}
	}

	protected abstract double getAngle();

	protected boolean isFinished() {
		return (alignController.onTarget() && toleranceBuffer.onTarget()) || isTimedOut();
	}

	protected void end() {
		chassis.logger.log(Level.INFO, "Turn command ended. Reason: " + (isTimedOut() ? "is timed out."
				: "is aligned within 0.5 degrees. Current align error: " + alignController.getError() + "| keep error: " + keepInPlaceController.getError()));
		RobotState.getInstance().setRotationMode(RotationMode.BLENDED);
		toleranceBuffer.clear();
		alignController.disable();
		keepInPlaceController.disable();
		chassis.stopController();
	}

	protected void interrupted() {
		end();
	}
	
	public static class ToAngle extends TurnCommand{
		private final double angle;
		
		public ToAngle(boolean noTurnInPlace, boolean useFudgeFactor, double angle){
			this(noTurnInPlace, useFudgeFactor, 0.0, angle);
		}
		
		public ToAngle(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance, double angle){
			super(noTurnInPlace, useFudgeFactor, forwardDistance);
			
			this.angle = angle;
		}
		
		@Override
		protected double getAngle(){
			return angle;
		}
	}
	
	public static abstract class TurnTo extends TurnCommand {
		public TurnTo(boolean noTurnInPlace, boolean useFudgeFactor) {
			super(noTurnInPlace, useFudgeFactor);
		}
		
		public TurnTo(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance){
			super(noTurnInPlace, useFudgeFactor, forwardDistance);
		}

		@Override
		protected double getAngle() {
			Pose2D robotPose = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));

			chassis.logger.log(Level.INFO,
					"Y: " + getY() + "| robotY: " + robotPose.y + "| X: " + getX() + "| robotX: " + robotPose.x);

			return Math.atan2(getY() - robotPose.y, getX() - robotPose.x);
		}
		
		protected abstract double getX();
		protected abstract double getY();
	}

	public static class AlignByVision extends TurnCommand {
		private static final int ALIGN_THROW_OUT_IMAGE_COUNT_DEFAULT = 1;
		private static final int ALIGN_TIMES_TO_ALIGN_COUNT_DEFAULT = 0;

		private final int numImagesToThrowOut, numTimesToReAlign;
		private int currentNumImages, currentNumTimesReAligned;
		private double timeStamp;
		private boolean actuallyRan;
		private final Vision vision;

		public AlignByVision(boolean noTurnInPlace, boolean useFudgeFactor) {
			this(noTurnInPlace, useFudgeFactor, 0.0, ALIGN_THROW_OUT_IMAGE_COUNT_DEFAULT, ALIGN_TIMES_TO_ALIGN_COUNT_DEFAULT);
		}
		
		public AlignByVision(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance){
			this(noTurnInPlace, useFudgeFactor, forwardDistance, ALIGN_THROW_OUT_IMAGE_COUNT_DEFAULT, ALIGN_TIMES_TO_ALIGN_COUNT_DEFAULT);
		}

		public AlignByVision(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance, int numImagesToThrowOut, int numTimesToAlign) {
			super(noTurnInPlace, useFudgeFactor, forwardDistance);

			actuallyRan = false;
			this.numImagesToThrowOut = numImagesToThrowOut;
			this.numTimesToReAlign = numTimesToAlign;
			vision = Vision.getInstance();

			requires(vision);
		}

		@Override
		protected void initialize() {
			timeStamp = vision.getTargetData().getTimeStamp();
		}

		@Override
		protected void execute() {
			if (!actuallyRan && vision.getTargetData().isNewImage(timeStamp) && vision.getTargetData().isValid()) {
				if (currentNumImages >= numImagesToThrowOut) {
					chassis.logger.log(Level.INFO, "Now targeting the rebel base.");

					super.initialize();
					actuallyRan = true;
				} else {
					currentNumImages++;
					timeStamp = Vision.getInstance().getTargetData().getTimeStamp();
				}
			}
		}
		
		@Override
		protected double getAngle(){
			Pose2D robotPose = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));
			
			return robotPose.heading + vision.getTargetData().getDeltaYaw();
		}

		@Override
		protected boolean isFinished() {
			if (super.isFinished() && (actuallyRan || isTimedOut())) {
				if (numTimesToReAlign < currentNumTimesReAligned && (Math.abs(vision.getTargetData().getDeltaYaw()) > ALIGN_STOP_THRESHOLD)) {
					chassis.logger.log(Level.INFO, "Those sneaky rebels have gotten away! Re-initializing...");
					actuallyRan = false;
					currentNumTimesReAligned++;
				} else {
					return true;
				}
			}
			return false;
		}

		@Override
		protected void end() {
			chassis.logger.log(Level.INFO, "The rebel base is within firing range.");
			super.end();
			actuallyRan = false;
			currentNumTimesReAligned = 0;
			currentNumImages = 0;
		}

		@Override
		protected void interrupted() {
			chassis.logger.log(Level.INFO, "We have been interrupted by those rebel scum.");
			end();
		}

		public static class To extends CommandGroup {
			public To(Vision.Mode mode) {
				addSequential(new SetVisionCommand(mode, true));
				addSequential(new TurnCommand.AlignByVision(false, true));
			}
		}
	}
	
	public static class AlignByDeadReckoning extends TurnTo {
		private final Vision.Mode mode;

		public AlignByDeadReckoning(boolean noTurnInPlace, boolean useFudgeFactor, double forwardDistance, Vision.Mode mode) {
			super(noTurnInPlace, useFudgeFactor, forwardDistance);

			this.mode = mode;
		}
		
		public AlignByDeadReckoning(boolean noTurnInPlace, boolean useFudgeFactor, Vision.Mode mode){
			this(noTurnInPlace, useFudgeFactor, 0.0, mode);
		}

		@Override
		protected double getX() {
			double toReturn = 0.0;

			switch (mode) {
			case BOILER_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the boiler x.");
				toReturn = FieldData.Map.GetBoilerX();
				break;
			case GEAR_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the gear peg x.");
				toReturn = getClosestGearPeg().getX();
				break;
			case OFF:
			default:
				chassis.logger.log(Level.WARNING, "Invalid vision mode.");
				break;
			}

			return toReturn;
		}

		@Override
		protected double getY() {
			double toReturn = 0.0;

			switch (mode) {
			case BOILER_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the boiler y.");
				toReturn = FieldData.Map.GetBoilerY();
				break;
			case GEAR_TARGETING:
				chassis.logger.log(Level.INFO, "Looking for the gear peg y.");
				toReturn = getClosestGearPeg().getY();
				break;
			case OFF:
			default:
				chassis.logger.log(Level.WARNING, "Invalid vision mode.");
				break;
			}

			return toReturn;
		}

		private Vector2D getClosestGearPeg() {
			Pose2D current = RobotState.getInstance().getPose(new Pose2D(0, 0, 0));

			Vector2D currentPose = new Vector2D(current.x, current.y);

			ArrayList<Vector2D> toPegVectors = new ArrayList<Vector2D>();
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetBoilerPegX() - current.x, FieldData.Map.GetBoilerPegY() - current.y));
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetCenterPegX() - current.x, FieldData.Map.GetCenterPegY() - current.y));
			toPegVectors.add(
					new Vector2D(FieldData.Map.GetFeederPegX() - current.x, FieldData.Map.GetFeederPegY() - current.y));

			toPegVectors.sort(null);
			Vector2D closestPeg = null;

			if (toPegVectors.get(0).normalize().getDot(currentPose.normalize()) < toPegVectors.get(1).normalize()
					.getDot(currentPose.normalize())) {
				closestPeg = toPegVectors.get(0);
			} else {
				closestPeg = toPegVectors.get(1);
			}

			return closestPeg.getAddition(currentPose);
		}
	}
}
