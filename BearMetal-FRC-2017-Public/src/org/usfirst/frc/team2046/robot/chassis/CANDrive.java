/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.chassis;

import com.ctre.CANTalon;
import com.ctre.CANTalon.FeedbackDevice;
import com.ctre.CANTalon.TalonControlMode;

public class CANDrive {
    private static final double CURRENT_CONTROLLER_P = 0.13;
    private static final double CURRENT_CONTROLLER_I = 0.001;
    private static final double CURRENT_CONTROLLER_D = 0;
    private static final double CURRENT_CONTROLLER_F = 0.06975;

    private double zeroSensorPosition = 0.0;
	private final CANTalon motors[];
	
	public static class MotorParameter {
		int deviceId;
		boolean reversed;
		MotorParameter(int deviceId, boolean inverted) {
			this.deviceId = deviceId;
			this.reversed = inverted;
		}
	}
	
	public CANDrive(MotorParameter motorParameter[]) {
		
		motors = new CANTalon[motorParameter.length];
		for (int i = 0; i < motorParameter.length; i++) {
			
			motors[i] = new CANTalon(motorParameter[i].deviceId);
			if (i == 0) {
				motors[0].setFeedbackDevice(FeedbackDevice.CtreMagEncoder_Relative);
				
			} else {
				motors[i].changeControlMode(CANTalon.TalonControlMode.Follower);
				motors[i].set(motors[0].getDeviceID());
			}
			motors[i].reverseOutput(motorParameter[i].reversed);
			motors[i].enableBrakeMode(true);
			motors[i].enable();
		}
		changeControlMode(CANTalon.TalonControlMode.PercentVbus);
	}
	
	public void changeControlMode(TalonControlMode controlMode) {
		if (motors[0].getControlMode() == controlMode) {
			return;
		}
		
		if (controlMode == CANTalon.TalonControlMode.Current) {
			motors[0].setP(CURRENT_CONTROLLER_P);
			motors[0].setI(CURRENT_CONTROLLER_I);
			motors[0].setD(CURRENT_CONTROLLER_D);
			motors[0].setF(CURRENT_CONTROLLER_F);
		} else {
			motors[0].setF(0.0);
		}
		motors[0].changeControlMode(controlMode);
	}
	
	public void invert() {
		motors[0].setInverted(true);
	}
	
	public void reverseSensor() {
		motors[0].reverseSensor(true);
	}
	
	public void enableControl() {
		motors[0].enableControl();
	}
	
	public void disableControl() {
		motors[0].disableControl();
	}
	
	public void setPID(double p, double i, double d, double f) {
		motors[0].setP(p);
		motors[0].setI(i);
		motors[0].setD(d);
		motors[0].setF(f);
	}
	
	
	public void set(double value) {
		motors[0].set(value);
	}

	public double getSpeed() {
		return motors[0].getSpeed();
	}
	
	public double getPosition(){
		return zeroSensorPosition + motors[0].getPosition();
	}

	public double getOutputCurrent() {
		double current = 0;
		for(CANTalon motor : motors) {
			current += motor.getOutputCurrent();
		}
		return current;
	}

	public void setF(double f) {
		motors[0].setF(f);
	}
	
	public void setCurrentLimit(int currentLimit) {
		motors[0].setCurrentLimit(currentLimit);
		motors[0].EnableCurrentLimit(true);
	}
	
	public void zeroEncoderDistance(){
		zeroSensorPosition = getPosition();
	}
	
}
