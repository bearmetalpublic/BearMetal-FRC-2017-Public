package org.usfirst.frc.team2046.robot.chassis;

import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.RobotState;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.PIDSourceType;
import edu.wpi.first.wpilibj.command.Command;

public class DriveForwardCommand extends Command {
	private static final double DISTANCE_ABSOLUTE_THRESHOLD = 0.05;
	private static final double KEEP_FROM_TURNING_CONTROLLER_PERIOD = 0.02;

	private static final double MAX_ACCELERATION = 2.0;
	private static final double TIMEOUT_FUDGE_FACTOR = 1.0;
	
	public static double DISTANCE_CONTROLLER_P = 2.0;
	public static double DISTANCE_CONTROLLER_I = 0.0;
	public static double DISTANCE_CONTROLLER_D = 0.0;
	public static double DISTANCE_CONTROLLER_F = 0.0;

	public static double KEEP_FROM_TURNING_CONTROLLER_P = 1.0;
	public static double KEEP_FROM_TURNING_CONTROLLER_I = 0.0;
	public static double KEEP_FROM_TURNING_CONTROLLER_D = 0.0;
	public static double KEEP_FROM_TURNING_CONTROLLER_F = 0.0;

	private final double distance;
	private final PIDController keepFromTurningController;
	private final Chassis chassis;
	private final boolean isReversed, noDeceleration;

	protected volatile double startingDistance;
	private volatile double rotationalSpeed;

	public DriveForwardCommand(double speed, double distance, boolean isReversed){
		this(speed, distance, MAX_ACCELERATION, isReversed, false);
	}
	
	public DriveForwardCommand(double speed, double distance, boolean isReversed, boolean noDeceleration){
		this(speed, distance, MAX_ACCELERATION, isReversed, noDeceleration);
	}
	
	public DriveForwardCommand(double speed, double distance, double maxAccel, boolean isReversed, boolean noDeceleration) {
		super(getTimeout(speed, distance));
		
		this.distance = distance;
		this.isReversed = isReversed;
		this.noDeceleration = noDeceleration;
		this.chassis = Chassis.getInstance();

		PIDSource keepSource = new PIDSource() {
			private Pose2D pose = new Pose2D(0, 0, 0);

			@Override
			public void setPIDSourceType(PIDSourceType pidSource) {
			}

			@Override
			public PIDSourceType getPIDSourceType() {
				return PIDSourceType.kDisplacement;
			}

			@Override
			public double pidGet() {
				return RobotState.getInstance().getPose(pose).heading;
			}
		};

		PIDOutput keepOutput = new PIDOutput() {
			@Override
			public void pidWrite(double output) {
				rotationalSpeed = output;
				
				chassis.logger.log(Level.INFO, "Current encoder distance: " + chassis.getAverageEncoderDistanceInMeters() + ", current encoder error: " + getRemainingDistance() + ", current output: " + output);

				chassis.setSpeed(getScaledSpeed(new RobotSpeed((isReversed ? -speed : speed), rotationalSpeed), maxAccel, getRemainingDistance()), maxAccel, KEEP_FROM_TURNING_CONTROLLER_PERIOD);
			}
		};

		keepFromTurningController = new PIDController(KEEP_FROM_TURNING_CONTROLLER_P, KEEP_FROM_TURNING_CONTROLLER_I,
				KEEP_FROM_TURNING_CONTROLLER_D, KEEP_FROM_TURNING_CONTROLLER_F, keepSource, keepOutput, KEEP_FROM_TURNING_CONTROLLER_PERIOD);
		keepFromTurningController.setOutputRange(-10.0, 10.0);
		keepFromTurningController.setInputRange(0.0, 2.0 * Math.PI);
		keepFromTurningController.setContinuous();

		requires(chassis);
	}

	@Override
	protected void initialize() {
		keepFromTurningController.setPID(DISTANCE_CONTROLLER_P, DISTANCE_CONTROLLER_I, DISTANCE_CONTROLLER_D,
				DISTANCE_CONTROLLER_F);

		startingDistance = chassis.getAverageEncoderDistanceInMeters();
		keepFromTurningController.setSetpoint(RobotState.getInstance().getPose(new Pose2D(0, 0, 0)).heading);

		keepFromTurningController.enable();
	}
	
	private static double getTimeout(double speed, double distance){
		return Math.abs(distance / speed) + TIMEOUT_FUDGE_FACTOR;
	}
	
	private RobotSpeed getScaledSpeed(RobotSpeed speed, double maxAccel, double remainingDistance){
		if(!noDeceleration){
			double decelerationScalar = Math.sqrt(2 * maxAccel * Math.abs(remainingDistance)) / speed.forward;
			speed.reduce(decelerationScalar);	
		}
		return speed;
	}
	
	private double getRemainingDistance(){
		return startingDistance + (isReversed ? -distance : distance) - chassis.getAverageEncoderDistanceInMeters();
	}
	
	private boolean isOnTarget(){
		return (isReversed ? getRemainingDistance() > -DISTANCE_ABSOLUTE_THRESHOLD : getRemainingDistance() < DISTANCE_ABSOLUTE_THRESHOLD);
	}

	@Override
	protected boolean isFinished() {
		return isOnTarget() || isTimedOut();
	}

	@Override
	protected void end() {
		chassis.logger.log(Level.INFO, "Finished driving forward.");
		
		keepFromTurningController.disable();
		chassis.stopController();
	}

	@Override
	protected void interrupted() {
		chassis.logger.log(Level.WARNING, "Drive forward interrupted.");
		
		end();
	}
	
	public static class AbsoluteDistance extends DriveForwardCommand{
		public AbsoluteDistance(double speed, double distance, boolean isReversed){
			this(speed, distance, isReversed, false);
		}
		
		public AbsoluteDistance(double speed, double distance, boolean isReversed, boolean noDeceleration){
			super(speed, distance, isReversed, noDeceleration);
		}
		
		@Override
		public void initialize(){
			super.initialize();
			startingDistance = 0.0;
		}
	}
}
