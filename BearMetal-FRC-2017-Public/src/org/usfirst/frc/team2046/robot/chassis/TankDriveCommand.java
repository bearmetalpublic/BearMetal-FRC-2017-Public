/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.chassis;

import org.usfirst.frc.team2046.robot.OI;

import edu.wpi.first.wpilibj.command.Command;

public class TankDriveCommand extends Command {
	
	private OI oi = OI.getInstance();
	private Chassis chassis = Chassis.getInstance();

    public TankDriveCommand() {
    	requires(chassis);
    }

    protected void execute() {
    	double rotationalPower = chassis.getRotationalPowerCurve();
		double forwardPower =  chassis.getForwardPowerCurve();
		double setLeftMotor = oi.getLeftDrive();
		double setRightMotor = oi.getRightDrive();
		double halfSum = 0.5*(setLeftMotor+setRightMotor);
		double halfDifference = 0.5*(setLeftMotor-setRightMotor);
		double forward = Math.pow(Math.abs(halfSum),forwardPower-1.0) * halfSum;
		double rotational = Math.pow(Math.abs(halfDifference),rotationalPower-1.0) * halfDifference * (1 - 0.5 * Math.abs(forward));
    	double lpower = forward+rotational;
    	double rpower = forward-rotational;
    	chassis.setDrivePower(lpower, rpower);

    }

    protected boolean isFinished() {
        return false;
    }
}
