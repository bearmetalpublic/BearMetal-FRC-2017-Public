package org.usfirst.frc.team2046.robot.shooter;

import java.util.logging.Level;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.usfirst.frc.team2046.robot.FieldData;
import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.util.TogglableCommandGroup;
import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;
import org.usfirst.frc.team2046.robot.vision.SetVisionCommand;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.command.Command;

public class SetShooterSpeed extends Command implements TogglableCommand {
	public static final double FALLBACK_RPM = 3000;

	private final boolean waitUntilSpunup;
	protected final Shooter shooter;

	protected double RPM;
	protected boolean actuallyRan;
	public SetShooterSpeed(double RPM) {
		this(RPM, false);
	}
	
	public SetShooterSpeed(double RPM, boolean waitUntilSpunup) {
		this.RPM = RPM;
		this.waitUntilSpunup = waitUntilSpunup;

		shooter = Shooter.getInstance();
		requires(shooter);
	}

	@Override
	protected void initialize() {
		shooter.logger.log(Level.INFO, "Setting speed to: " + RPM);
		
		shooter.setShooterSpeed(RPM);
		actuallyRan = true;
	}

	@Override
	protected boolean isFinished() {
		return actuallyRan && (shooter.onTarget() || !waitUntilSpunup);
	}
	
	@Override
	protected void end(){
		actuallyRan = false;
	}

	@Override
	public boolean isToggled() {
		return (Shooter.getInstance().isShooting() && RPM > 0) || (!Shooter.getInstance().isShooting() && RPM == 0);
	}

	@Override
	public Command toCommand() {
		return this;
	}
	
	private static abstract class ByDistance extends SetShooterSpeed {
		private static final double DISTANCE_LONG_THRESHOLD = 120.0 * 0.0254;
		private static final double DISTANCE_LONG_FUDGE_FACTOR = -9.0 * 0.0254;
		private double additionalInchesOffset;
		
		private ByDistance(boolean waitUntilSpunup, double additionalInchesOffset){
			super(FALLBACK_RPM, waitUntilSpunup);

			this.additionalInchesOffset = additionalInchesOffset;
		}
		
		private ByDistance(boolean waitUntilSpunup) {
			this(waitUntilSpunup, 0.0);
		}

		private double getRPMByDistance(double distanceInMeters){
			double RPM = 0.0;
			if (shooter.isDistanceSane(distanceInMeters)) {
				RPM = ((distanceInMeters / 0.0254) + shooter.getInchesOffset() + additionalInchesOffset) * shooter.getInchesMultiplier()
						+ shooter.getInchesIntercept();
				shooter.logger.log(Level.INFO, "Distance detected: " + distanceInMeters + ", RPM set" + RPM);
			} else {
				RPM = FALLBACK_RPM;
				shooter.logger.log(Level.WARNING,
						"Sanity check failed. Using fallback RPM. Estimated distance: " + distanceInMeters);
			}
			return RPM;
		}
		
		@Override
		protected void initialize() {
			double distanceInMeters = 0.0;
			shooter.logger.log(Level.INFO, "Setting RPM based off of distance.");
			
			distanceInMeters = getDistance();
			if(distanceInMeters > DISTANCE_LONG_THRESHOLD){
				distanceInMeters += DISTANCE_LONG_FUDGE_FACTOR;
			}
			RPM = getRPMByDistance(distanceInMeters);
			
			super.initialize();
		}
		
		protected abstract double getDistance();
	}
	
	public static class ByDeadReckoning extends ByDistance {
		public ByDeadReckoning(boolean waitUntilSpunup, double additionalInchesOffset){
			super(waitUntilSpunup, additionalInchesOffset);
		}
		
		public ByDeadReckoning(boolean waitUntilSpunup) {
			super(waitUntilSpunup);
		}
		
		@Override
		protected double getDistance(){
			Pose2D currentPose = RobotState.getInstance().getPose(new Pose2D(0,0,0));
			double deltaX = FieldData.Map.GetBoilerX() - currentPose.x;
			double deltaY = FieldData.Map.GetBoilerY() - currentPose.y;
			
			return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
		}
	}

	public static class ByVision extends ByDistance {
		private static final int DEFAULT_NUM_IMAGES_TO_THROW_OUT = 2;
		
		private double timeStamp;
		private final int numImagesToThrowOut;
		private int numImages;
		public ByVision(boolean waitUntilSpunup, double additionalInchesOffset){
			super(waitUntilSpunup, additionalInchesOffset);
			
			this.numImagesToThrowOut = DEFAULT_NUM_IMAGES_TO_THROW_OUT;
		}
		
		public ByVision(boolean waitUntilSpunup){
			this(waitUntilSpunup, DEFAULT_NUM_IMAGES_TO_THROW_OUT);
		}
		
		public ByVision(boolean waitUntilSpunup, int numImagesToThrowOut) {
			super(waitUntilSpunup);
			
			this.numImagesToThrowOut = numImagesToThrowOut;
		}

		@Override
		protected void initialize() {
			timeStamp = Vision.getInstance().getTargetData().getTimeStamp();
		}
		
		@Override
		protected void execute(){
			if (Vision.getInstance().getTargetData().isNewImage(timeStamp)
					&& Vision.getInstance().getTargetData().isValid()
					&& !actuallyRan) {
				if(numImagesToThrowOut > numImages){
					numImages++;
				}
				else{
					shooter.logger.log(Level.INFO, "Vision being used to set RPM.");
					super.initialize();	
				}
			}
		}
		
		@Override
		protected double getDistance(){
			return Vision.getInstance().getTargetData().getDistance();
		}
		
		@Override
		protected void end(){
			numImages = 0;
			super.end();
		}
	}
	
	public static class ByGivenPosition extends ByDistance {
		private final double x, y;
		public ByGivenPosition(double x, double y, boolean waitUntilSpunup) {
			super(waitUntilSpunup);
			
			this.x = x;
			this.y = y;
		}

		@Override
		protected void initialize() {
			shooter.logger.log(Level.INFO, "Given position being used to set RPM.");
			super.initialize();
		}
		
		@Override
		protected double getDistance(){
			double deltaX = FieldData.Map.GetBoilerX() - x;
			double deltaY = FieldData.Map.GetBoilerY() - y;
			
			return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
		}
	}

	public static class Tele extends TogglableCommandGroup {
		public Tele() {
			addTogglableSequential(new StartShooting.Prep(true));
			addTogglableSequential(new SetShooterSpeed.ByVision(true));
		}
	}

	public static class Off extends TogglableCommandGroup {
		public Off() {
			addTogglableSequential(new SetShooterSpeed(0.0, false));
			addTogglableSequential(new SetVisionCommand(Vision.Mode.OFF));
		}
	}
}
