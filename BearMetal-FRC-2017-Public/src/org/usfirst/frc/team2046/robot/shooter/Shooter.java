/**
4 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.usfirst.frc.team2046.robot.shooter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.opencv.core.Point;
import org.usfirst.frc.team2046.robot.util.BearSubsystem;
import org.usfirst.frc.team2046.robot.util.LinearRegression;
import org.usfirst.frc.team2046.robot.util.RobotProperties;
import org.usfirst.frc.team2046.robot.vision.Vision;

import com.ctre.CANTalon;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * 
 */
public class Shooter extends BearSubsystem {
	private static final Shooter instance = new Shooter();
	private static final RobotProperties properties = new RobotProperties(Shooter.class.getSimpleName());

	private static final double SPEED_ABSOLUTE_TOLERANCE = 10;
	private static final double SPEED_INCHES_OFFSET_CHANGE = 3;

	private static final double DISTANCE_EQUATION_INTERCEPT_DEFAULT = 2279;
	private static final double DISTANCE_EQUATION_MULTIPLIER_DEFAULT = 8.518;
	public static final double DISTANCE_EQUATION_OFFSET_DEFAULT = -30.0;

	private static final double CNTRL_P = 0.400;
	private static final double CNTRL_I = 0.000;
	private static final double CNTRL_D = 0.000;
	private static final double CNTRL_F = 0.028;

	public static Shooter getInstance() {
		return instance;
	}

	private final CANTalon shooterMotorMaster;
	private final CANTalon shooterMotorSlave;

	private double rpmSetpoint;

	private Shooter() {
		super(Shooter.class);

		CANTalon shooterMotorMaster = null;
		CANTalon shooterMotorSlave = null;

		try {
			shooterMotorMaster = new CANTalon(RobotMap.SHOOTER_MOTOR_LEFT);
			shooterMotorMaster.setFeedbackDevice(CANTalon.FeedbackDevice.CtreMagEncoder_Relative);
			shooterMotorMaster.changeControlMode(CANTalon.TalonControlMode.Speed);
			shooterMotorMaster.enableControl();
			shooterMotorMaster.setCurrentLimit(30);
			shooterMotorMaster.EnableCurrentLimit(true);
			shooterMotorMaster.enableBrakeMode(true);
			shooterMotorMaster.setPID(CNTRL_P, CNTRL_I, CNTRL_D);
			shooterMotorMaster.setF(CNTRL_F);

			shooterMotorSlave = new CANTalon(RobotMap.SHOOTER_MOTOR_RIGHT);
			shooterMotorSlave.changeControlMode(CANTalon.TalonControlMode.Follower);
			shooterMotorSlave.set(shooterMotorMaster.getDeviceID());
			shooterMotorSlave.reverseOutput(true);
			shooterMotorSlave.enableBrakeMode(true);
			shooterMotorSlave.setCurrentLimit(30);
			shooterMotorSlave.EnableCurrentLimit(true);
			shooterMotorSlave.enableControl();
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to construct.", e);
		}

		this.shooterMotorMaster = shooterMotorMaster;
		this.shooterMotorSlave = shooterMotorSlave;
		rpmSetpoint = 0.0;
	}

	/**
	 * Sets the speed of the shooter
	 */
	public void setShooterSpeed(double rpm) {
		logDebug("Setting shooter rpm to: " + rpm + "rpm.");

		enable(rpm != 0.0);
		shooterMotorMaster.set(rpm);
		rpmSetpoint = rpm;
	}

	public double getShooterSpeed() {
		return shooterMotorMaster.getSpeed();
	}

	public boolean onTarget() {
		return Math.abs(shooterMotorMaster.getSpeed() - rpmSetpoint) < SPEED_ABSOLUTE_TOLERANCE;
	}

	public boolean isShooting() {
		return shooterMotorMaster.isControlEnabled();
	}

	/**
	 * Enable/disable allow for smooth spin down
	 */
	private void enable(boolean enable) {
		if (enable) {
			if (!shooterMotorMaster.isControlEnabled()) {
				logDebug("Shooter motor enabling.");
				shooterMotorMaster.enableBrakeMode(true);
				shooterMotorMaster.enableControl();
			}
		} else {
			if (shooterMotorMaster.isControlEnabled()) {
				logDebug("Shooter motor disabling");
				shooterMotorMaster.enableBrakeMode(false);
				shooterMotorMaster.disableControl();
			}
		}
	}

	public void initDefaultCommand() {
	}

	@Override
	public void initSubsystem() {
		if (!properties.load()) {
			setInchesOffset(DISTANCE_EQUATION_OFFSET_DEFAULT);
			setInchesMultiplier(DISTANCE_EQUATION_MULTIPLIER_DEFAULT);
			setInchesIntercept(DISTANCE_EQUATION_INTERCEPT_DEFAULT);
		}
	}

	@Override
	public void runningUpdate() {
		SmartDashboard.putBoolean("Is Shooting", isShooting());
	}

	@Override
	public void debugUpdate() {
		SmartDashboard.putNumber("Shooter Speed", shooterMotorMaster.getSpeed());
		SmartDashboard.putNumber("Shooter Voltage",
				Math.max(shooterMotorSlave.getOutputVoltage(), shooterMotorMaster.getOutputVoltage()));
	}

	public boolean isDistanceSane(double distanceInMeters) {
		return distanceInMeters > RobotMap.BOILER_MIN_DISTANCE
				&& distanceInMeters < RobotMap.BOILER_MAX_DISTANCE;
	}

	public double getInchesIntercept() {
		return Double.parseDouble(properties.getProperty("distanceEquationIntercept"));
	}

	public void setInchesIntercept(double inchesIntercept) {
		properties.setProperty("distanceEquationIntercept", Double.toString(inchesIntercept));
		properties.save();
	}

	public double getInchesMultiplier() {
		return Double.parseDouble(properties.getProperty("distanceEquationMultiplier"));
	}

	public void setInchesMultiplier(double inchesMultiplier) {
		properties.setProperty("distanceEquationMultiplier", Double.toString(inchesMultiplier));
		properties.save();
	}

	public double getInchesOffset() {
		return Double.parseDouble(properties.getProperty("distanceEquationOffset"));
	}

	public void setInchesOffset(double inchesOffset) {
		properties.setProperty("distanceEquationOffset", Double.toString(inchesOffset));
		properties.save();
	}

	public double increaseInchesOffset() {
		setInchesOffset(getInchesOffset() + SPEED_INCHES_OFFSET_CHANGE);
		return getInchesOffset();
	}

	public double decreaseInchesOffset() {
		setInchesOffset(getInchesOffset() - SPEED_INCHES_OFFSET_CHANGE);
		return getInchesOffset();
	}

	@Override
	protected void startDebug() {
		SmartDashboard.putNumber("Shooter Setpoint", 0.0);
		SmartDashboard.putData("Shooter Setspeed", new Command(){
			@Override
			protected boolean isFinished() {
				setShooterSpeed(SmartDashboard.getNumber("Shooter Setpoint", 0.0));
				return false;
			}
			
			@Override
			protected void end(){
				setShooterSpeed(0.0);
			}
		});
		
		SmartDashboard.putData("Shooter Calibration", new Command(){
			private static final double CALIBRATION_POINT_SIZE = 5;
			private List<Point> calibrationPoints = new ArrayList<Point>();
			
			private final Vision vision;
			private boolean readyToCalibrate;
			
			{
				SmartDashboard.putData("Shooter Calibration Take Data Point", new Command(){
					@Override
					protected boolean isFinished() {
						return readyToCalibrate && vision.getTargetData().isValid() && vision.getMode() == Vision.Mode.BOILER_TARGETING;
					}
					
					@Override
					protected void end(){
						double visionSeenDistance = vision.getTargetData().getDistance() / 0.0254;
						double shooterRPM = SmartDashboard.getNumber("Shooter Setpoint", 0.0);
						
						logger.log(Level.INFO, "Shooter RPM: " + shooterRPM + "| Vision Seen Distance: " + visionSeenDistance);
						
						calibrationPoints.add(new Point(visionSeenDistance, shooterRPM));
					}
					
					@Override
					protected void interrupted(){
						
					}
				});
				
				vision = Vision.getInstance();
			}
			
			@Override
			protected void initialize(){
				if(vision.getTargetData().isValid() && vision.getMode() == Vision.Mode.BOILER_TARGETING){
					calibrationPoints.clear();
					readyToCalibrate = true;
				}
			}
			
			@Override
			protected boolean isFinished() {
				return calibrationPoints.size() >= CALIBRATION_POINT_SIZE && readyToCalibrate;
			}
			
			@Override
			protected void end(){
				readyToCalibrate = false;
				
				LinearRegression linearRegression = new LinearRegression(calibrationPoints);
				setInchesMultiplier(linearRegression.beta1);
				setInchesIntercept(linearRegression.beta0);
			}
			
			@Override
			protected void interrupted(){
				
			}
		});
	}

	@Override
	protected void stopDebug() {

	}

	@Override
	public void reset() {
		setShooterSpeed(0.0);
	}
}