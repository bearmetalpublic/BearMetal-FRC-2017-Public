package org.usfirst.frc.team2046.robot.shooter;

import org.usfirst.frc.team2046.robot.chassis.TurnCommand;
import org.usfirst.frc.team2046.robot.collector.Collector;
import org.usfirst.frc.team2046.robot.collector.CollectorCommand;
import org.usfirst.frc.team2046.robot.hopper.Hopper;
import org.usfirst.frc.team2046.robot.hopper.SetHopperState;
import org.usfirst.frc.team2046.robot.util.TogglableCommandGroup;
import org.usfirst.frc.team2046.robot.util.ToggleCommand.TogglableCommand;
import org.usfirst.frc.team2046.robot.vision.SetVisionCommand;
import org.usfirst.frc.team2046.robot.vision.Vision;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.CommandGroup;

public class StartShooting extends CommandGroup implements TogglableCommand {
	public StartShooting(boolean useVision, boolean shootFirst, double additionalInchesOffset){
		if(useVision){
			if(shootFirst)
				addParallel(new TurnCommand.AlignByVision(false, true));
			addSequential(new SetShooterSpeed.ByVision(true, additionalInchesOffset));
			if(!shootFirst)
				addSequential(new TurnCommand.AlignByVision(false, true));
		}
		else{
			if(shootFirst)
				addParallel(new TurnCommand.AlignByDeadReckoning(false, true, Vision.Mode.BOILER_TARGETING));
			addSequential(new SetShooterSpeed.ByDeadReckoning(true, additionalInchesOffset));
			if(!shootFirst)
				addSequential(new TurnCommand.AlignByDeadReckoning(false, true, Vision.Mode.BOILER_TARGETING));
		}
		addSequential(new CollectorCommand(Collector.State.FAST));
		addSequential(new SetHopperState(Hopper.State.ON));
		addParallel(new TurnCommand.AlignByVision(false, true));
		addSequential(new SetShooterSpeed.ByVision(false));
	}
	
	public StartShooting(boolean useVision, boolean shootFirst){
		this(useVision, shootFirst, 0.0);
	}

	@Override
	public boolean isToggled() {
		return Shooter.getInstance().isShooting();
	}

	@Override
	public Command toCommand() {
		return this;
	}
	
	public static class Tele extends TogglableCommandGroup {
		public Tele(){
			addTogglableSequential(new Prep(true));
			addTogglableSequential(new StartShooting(true, true));
		}
	}
	
	public static class Prep extends TogglableCommandGroup {
		public Prep(){
			this(false);
		}
		
		public Prep(boolean waitUntilSwitched){
			this(waitUntilSwitched, SetShooterSpeed.FALLBACK_RPM);
		}
		
		public Prep(boolean waitUntilSwitched, double speed){
			addTogglableParallel(new SetVisionCommand(Vision.Mode.BOILER_TARGETING, waitUntilSwitched));
			addTogglableSequential(new SetShooterSpeed(speed, waitUntilSwitched));
		}
	}
	
	public static class Stop extends TogglableCommandGroup {
		public Stop(){
			addTogglableSequential(new SetShooterSpeed(0.0, false));
			addTogglableSequential(new CollectorCommand(Collector.State.OFF));
			addTogglableSequential(new SetHopperState(Hopper.State.OFF));
			addTogglableSequential(new SetVisionCommand(Vision.Mode.OFF));
		}
	}

}
