/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.path;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SplinePath extends Path {
	
	private static final Logger logger = Logger.getLogger(SplinePath.class.getSimpleName());

	public static final double CENTRIPETAL = 0.5;
	public static final double UNIFORM     = 0.0;
	public static final double CHORDAL     = 1.0;
	
	private static final double ALPHA = CENTRIPETAL;
	
	private final int numOfPoints;
	private final double entryAngle;
	private final double exitAngle;
	
	/**
	 * Creates a path which will be smoothed with this Catmull-Rom spline.
	 * Default number of interpolation points, entry and exit angle.
	 * 
	 */
	public SplinePath() {
		this(10, 0, 0);
	}

	/**
	 * Creates a path which will be smoothed with this Catmull-Rom spline.
	 * Default entry and exit angle.
	 * 
	 * @param numOfPoints - number of interpolation points used between waypoints
	 */
	public SplinePath(int numOfPoints) {
		this(numOfPoints, 0, 0);
	}
	
	/**
	 * Creates a path which will be smoothed with the Catmull-Rom spline.
	 * 
	 * @param numOfPoints - number of interpolation points used between waypoints
	 * @param entryAngle - entry angle from the first point to the next (radians)
	 * @param exitAngle - exit angle from next to last point to the last (radians)
	 */
	public SplinePath(int numOfPoints, double entryAngle, double exitAngle) {
		this.numOfPoints = numOfPoints;
		this.entryAngle = entryAngle;
		this.exitAngle = exitAngle;
	}
	
	/**
	 * Initializes the splined path by creating all of the segments
	 * on the path.  Assumes that all waypoints have been added by this
	 * time.
	 */
	@Override
	protected void start(CompletionListener listener) {
		int size;
		
		// add points on both ends to aid interpolation
		List<Waypoint> waypoints = getWaypoints();
		List<Waypoint> points = new ArrayList<>();		
		points.add(createEntryExitWaypoint(waypoints.get(0), waypoints.get(1), entryAngle)); 
		for(Waypoint waypoint : waypoints) {
			points.add(waypoint);
		}
		size = waypoints.size();
		points.add(createEntryExitWaypoint(waypoints.get(size-1), waypoints.get(size-2), exitAngle));
		
		
		// using four point at a time, generate spline segments
		LinkedList<Waypoint> interopolatedPoints = new LinkedList<>();
		size = points.size();
		for(int i = 0; i < size - 3; i++) {
			interopolatedPoints.addAll(interpolate(
					points.get(i), points.get(i+1), 
					points.get(i+2), points.get(i+3)));
		}
		// finish the path by adding the last real point
		interopolatedPoints.add(points.get(size-2));
		
		super.start(listener, interopolatedPoints);
	}
	
	public Path finalizeSpline() {
		if(getWaypoints().size() > 0){
			int size;
			
			// add points on both ends to aid interpolation
			List<Waypoint> waypoints = getWaypoints();
			List<Waypoint> points = new ArrayList<>();		
			points.add(createEntryExitWaypoint(waypoints.get(0), waypoints.get(1), entryAngle)); 
			for(Waypoint waypoint : waypoints) {
				points.add(waypoint);
			}
			size = waypoints.size();
			points.add(createEntryExitWaypoint(waypoints.get(size-1), waypoints.get(size-2), exitAngle));
			
			
			// using four point at a time, generate spline segments
			Path p = new Path();
			size = points.size();
			for(int i = 0; i < size - 3; i++) {
				p.addWaypoints(interpolate(
						points.get(i), points.get(i+1), 
						points.get(i+2), points.get(i+3)));
			}
			// finish the path by adding the last real point
			p.addWaypoint(points.get(size-2));
			return p;
		}
		return this;
	}
	
	/**
	 * create a waypoint extending the end waypoint in direction of the 
	 * current direction to the next waypoint.
	 * 
	 * @param p1 - point to extend from
	 * @param p2 - point next to the p1
	 * @param angle - angle off of p1 to p2 direction
	 * @return waypoint
	 */
	private Waypoint createEntryExitWaypoint(Waypoint p1, Waypoint p2, double angle) {
		double dx = p2.x - p1.x;
		double dy = p2.y - p1.y;
		double len = Math.sqrt(dx*dx+dy*dy);
		angle = angle + Math.atan2(dy, dx);
		return new Waypoint(p1.x - len*Math.cos(angle), p1.y - len*Math.sin(angle), p1.speed);
	}
	
	/**
	 * Return a Catmull-Rom interpolated spline by return p1 a number of intermidiate points
	 * up to but not including p2. 
	 */
	private List<Waypoint> interpolate(Waypoint p0, Waypoint p1, Waypoint p2, Waypoint p3) {
		
		double t0 = 0;
		double t1 = tj(t0, p0, p1);
		double t2 = tj(t1, p1, p2);
		double t3 = tj(t2, p2, p3);
		
		double t1t0 = t1-t0;
		double t2t1 = t2-t1;
		double t3t2 = t3-t2;
		double t3t1 = t3-t1;
		double t2t0 = t2-t0;
		
		List<Waypoint> out = new ArrayList<>();
		
		// 1st point is passed in point one
		out.add(p1);
		for (int i = 1; i<numOfPoints; i++) {
			double t = t1 + t2t1/numOfPoints*i;
			
			double t0t = t0-t;
			double t1t = t1-t;
			double t2t = t2-t;
			double t3t = t3-t;
			
			Waypoint a1 = p0.mult(t1t).add(p1.mult(-t0t)).div(t1t0);
			Waypoint a2 = p1.mult(t2t).add(p2.mult(-t1t)).div(t2t1);
			Waypoint a3 = p2.mult(t3t).add(p3.mult(-t2t)).div(t3t2);
			
			Waypoint b1 = a1.mult(t2t).add(a2.mult(-t0t)).div(t2t0);
			Waypoint b2 = a2.mult(t3t).add(a3.mult(-t1t)).div(t3t1);

			Waypoint c = b1.mult(t2t).add(b2.mult(-t1t)).div(t2t1);
			
			out.add(c);
		}
		return out;
	}

	private static double tj(double ti, Waypoint pi, Waypoint pj) {
		double dx = pj.x - pi.x;
		double dy = pj.y - pi.y;
		return ti + Math.pow(Math.sqrt(dx * dx + dy * dy), ALPHA);
	}

	public void deserialize(String data, double speed) {
		if (!data.startsWith("Spline{")) {
			logger.log(Level.WARNING, "Trying to serialize an invalid path: \"" + data + "\"");
			return;
		}
		if (data.equals("Spline{}")) return;
		data = data.substring(7, data.length() - 1); // Cuts off the "Spline{" part and ending close-brace
		// Trim off each waypoint as it is processed
		while (data.length() > 0) {
			// Cut off commas
			if (data.charAt(0) == ',') data = data.substring(1);
			
			String params = data.substring(9, data.indexOf("}"));
			String[] paramlist = params.split(",");
			Waypoint w = new Waypoint(Double.parseDouble(paramlist[0]), Double.parseDouble(paramlist[1]), speed);
			addWaypoint(w);
			//logger.log(Level.INFO, "Decoded waypoint (" + w.x + ", " + w.y + ").");
			data = data.substring(data.indexOf("}") + 1);
		}
	}
	
	@Override
	public SplinePath getReverse(){
		List<Waypoint> waypoints = getWaypoints();
		
		SplinePath newPath = new SplinePath();
		for (int i = waypoints.size() - 1; i >= 0; i--){
			newPath.addWaypoint(waypoints.get(i));
		}
		
		return newPath;
	}

}
