package org.tahomarobotics.robot.path;

public interface RobotSpeedCommandIF {

	public class RobotSpeed {
		public double forward;
		public double rotational;
		
		public RobotSpeed() {
			this(0,0);
		}
		
		public RobotSpeed(RobotSpeed speed) {
			this.forward = speed.forward;
			this.rotational = speed.rotational;
		}
		
		public RobotSpeed(double forward, double rotational) {
			this.forward = forward;
			this.rotational = rotational;
		}
		
		public void reduce(double scale) {
			scale = Math.abs(scale);
			if (scale < 1.0) {
				forward *= scale;
				rotational *= scale;
			}
		}
		
		public void copyFrom(RobotSpeed other){
			forward = other.forward;
			rotational = other.rotational;
		}
	}
	
	void setSpeed(RobotSpeed speed, double maxAccel);
}
