package org.tahomarobotics.robot.path;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.Path.Waypoint;


public class TestFollowPath extends FollowPathCommand {

	private static final Logger logger = Logger.getLogger(TestFollowPath.class.getSimpleName());
	
	private static final double DEFAULT_SPEED = 1.0; // 3.28 feet/second 

	public TestFollowPath() {
		super("Test Follow Path", createPath(), true);
	}

	private static class WaypointListener implements CompletionListener {
		private final String msg;
		public WaypointListener(String msg) {
			this.msg = msg;
		}
		@Override
		public void onCompletion() {
			logger.log(Level.INFO, msg);
		}
	}
	
	private static Path createPath() {
		Path path = new SplinePath();
		path.addWaypoint(new Waypoint(0.455, 2.24, DEFAULT_SPEED, new WaypointListener("Waypoint 1 done")));
		path.addWaypoint(new Waypoint(1.086, 2.171, DEFAULT_SPEED, new WaypointListener("Waypoint 2 done")));
		path.addWaypoint(new Waypoint(1.998, 1.925, DEFAULT_SPEED, new WaypointListener("Waypoint 3 done")));
		path.addWaypoint(new Waypoint(2.803, 2.418, DEFAULT_SPEED, new WaypointListener("Waypoint 3 done")));
		path.addWaypoint(new Waypoint(3.207, 3.027, DEFAULT_SPEED, new WaypointListener("All waypoints done")));

		return path;
	}
}
