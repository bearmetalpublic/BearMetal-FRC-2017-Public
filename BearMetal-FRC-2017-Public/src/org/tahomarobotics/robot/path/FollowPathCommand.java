/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.path;

import org.usfirst.frc.team2046.robot.RobotState;
import org.usfirst.frc.team2046.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;

/**
 * FollowPathCommand can be used in autonomous programming to take the Robot through
 * a order list of Waypoints.  These Waypoints have the starting position and the ending
 * position and any number of intermediate point to control the route of the robot. The
 * speed is also specified at each Waypoint.  As Waypoints are completed, notification are
 * sent out to allow additional action to occur.
 *
 */
public class FollowPathCommand extends Command {

	public static final double LOOKAHEAD_DISTANCE = 0.8; // meters
	public static final double MAX_VELOCITY       = 3.0; // meters/second
	public static final double MAX_ACCELERATION   = Chassis.DEFAULT_MAX_ACCELERATION; // meters/second^2
	public static final double NO_TIMEOUT = -1;
	
	private final AdaptivePurePursuitController controller;
	private final Chassis chassis;
	private final boolean waitForChassisStop;
	
	public FollowPathCommand(String name, Path path, boolean reversed) {
		this(name, path, reversed, true, LOOKAHEAD_DISTANCE, NO_TIMEOUT);
	}
	
	public FollowPathCommand(String name, Path path, boolean reversed, boolean waitForChassisStop){
		this(name, path, reversed, waitForChassisStop, LOOKAHEAD_DISTANCE, NO_TIMEOUT);
	}
	
	public FollowPathCommand(String name, Path path, boolean reversed, double lookaheadDistance){
		this(name, path, reversed, true, lookaheadDistance, NO_TIMEOUT);
	}
	
	public FollowPathCommand(String name, Path path, boolean reversed, boolean waitForChassisStop, double lookaheadDistance, double timeout){
		this(name, path, reversed, waitForChassisStop, lookaheadDistance, timeout, MAX_ACCELERATION);
	}
	
	/**
	 * Command to follow the given path with the robot going forward and in reverse.
	 * 
	 * @param path - a order list of Waypoints with specified speeds.
	 * @param reversed - follow path driving in reverse, otherwise drive forward
	 */
	public FollowPathCommand(String name, Path path, boolean reversed, boolean waitForChassisStop, double lookaheadDistance, double timeout, double maxAccel) {
		super(name);
		if (timeout != NO_TIMEOUT) {
			super.setTimeout(timeout);
		}
		chassis = Chassis.getInstance();
		requires(chassis);

		controller = new AdaptivePurePursuitController(path, 
				LOOKAHEAD_DISTANCE, maxAccel, reversed, 
				RobotState.getInstance(), chassis);
		
		this.waitForChassisStop = waitForChassisStop;
	}

	/**
	 * Initialize the command by starting the controller that will follow the specified path.
	 */
	@Override
	protected void initialize() {
		controller.start();
	}

	/**
	 * This command is finished when the controller states it is complete.
	 */
	@Override
	protected boolean isFinished() {
		if(controller.isComplete() && !ended){
			end();
		}
		
		return ended && (chassis.isStopped() || !waitForChassisStop);
	}
	
	private boolean ended = false;
	@Override
	protected void end() {
		controller.end();
		chassis.stopController();
		ended = true;
	}

}
