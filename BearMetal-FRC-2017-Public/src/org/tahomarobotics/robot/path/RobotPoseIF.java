package org.tahomarobotics.robot.path;

public interface RobotPoseIF {

	public class Pose2D {
		public double x, y, heading;

		public Pose2D(double x, double y, double heading) {
			this.x = x;
			this.y = y;
			this.heading = heading;
		}
		
		public Pose2D(Pose2D pose){
			this(pose.x, pose.y, pose.heading);
		}
	}
	
	public Pose2D getPose(Pose2D pose);
}
