/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.path;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.tahomarobotics.robot.path.Path.CompletionListener;
import org.tahomarobotics.robot.path.Path.Waypoint;
import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;
import org.tahomarobotics.robot.path.RobotSpeedCommandIF.RobotSpeed;
import org.usfirst.frc.team2046.robot.FieldData;

/**
 * Adaptive Pure Pursuit Controller is proportional controller which follows a path or sequence of points.  The
 * gain of the controller is determined by the look ahead distance where the robot is command to drive to.
 * Using the current robot position, the closest point on the path to the robot is determined.  This value is
 * then used to calculate and cross track error and a new look ahead point placed the a distance in advance of 
 * the current location.  The distance is a constant with the error added to it.
 */
public class AdaptivePurePursuitController {

	private static final Logger logger = Logger.getLogger(AdaptivePurePursuitController.class.getSimpleName());
	private static final long UPDATE_PERIOD_MS = 10;

	private final Path path;
	private final double lookAheadDistance;
	private final double maxAccel;
	private final boolean reversed;
	private final RobotPoseIF robotPoseInput;
	private final RobotSpeedCommandIF robotSpeedOutput;
	private Timer timer;
	private boolean complete = false;
	private final Pose2D pose = new Pose2D(0,0,0);
	private final RobotSpeed speedSetpoint = new RobotSpeed();
	
	/**
	 * Constructs the path following controller.
	 * 
	 * @param path
	 * @param lookAheadDistance - look ahead distance used to tune the gain of the controller
	 * @param maxAccel - acceleration limit used to ramp up and down the speed
	 * @param reversed - indicates if the robot needs to follow the path going in reverse, otherwise forward
	 * @param robotPoseInput - current location (x,y) and heading
	 * @param robotSpeedOutput - the output of the controller which drive the speed of the robot
	 */
	public AdaptivePurePursuitController(Path path, double lookAheadDistance, 
			double maxAccel, boolean reversed, 
			RobotPoseIF robotPoseInput, RobotSpeedCommandIF robotSpeedOutput) {

		this.path = path;
		this.lookAheadDistance = lookAheadDistance;
		this.maxAccel = maxAccel;
		this.reversed = reversed;
		this.robotPoseInput = robotPoseInput;
		this.robotSpeedOutput = robotSpeedOutput;

		// periodic timer to update the controller
		timer = new Timer(AdaptivePurePursuitController.class.getSimpleName());
	}

	/**
	 * Start the path following controller
	 */
	public void start() {
		//SmartDashboard.putString("Robot Path", path.serialize());
		logger.log(Level.INFO, "Starting path.");
		
		// start and listen for the path completion 
		path.start(new CompletionListener() {
			@Override
			public void onCompletion() {
				logger.log(Level.INFO, "Finished path.");
				
				complete = true;
				timer.cancel();
			}
		});

		// schedule the update of the path navigation
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
				update();
			}
		}, UPDATE_PERIOD_MS, UPDATE_PERIOD_MS);
	}

	/**
	 * Using the current position, progress the current path segment and 
	 * calculate the cross track error.  Command the vehicle to join the
	 * path at the look ahead point (look ahead distance and errors added).
	 */
	private void update() {
				
		robotPoseInput.getPose(pose);
		if (reversed) pose.heading += Math.PI;

		// update path segment with the current position 
		double pathError = path.update(pose);

		if (isComplete()) {
			speedSetpoint.forward = 0;
			speedSetpoint.rotational = 0;
			robotSpeedOutput.setSpeed(speedSetpoint, maxAccel);
			return;
		}

		// find look ahead point on path
		Waypoint lookAheadPoint = path.getLookAheadPoint(lookAheadDistance + pathError);
		FieldData.getInstance().setLookahead(lookAheadPoint);

		// create curve to join path at the look ahead point
		double curvature = getJoinCurvature(pose, lookAheadPoint);
		speedSetpoint.forward = reversed ? -lookAheadPoint.speed : lookAheadPoint.speed;
		speedSetpoint.rotational = lookAheadPoint.speed * curvature;

		// slow down if nearing end of path
		double remaining = path.getRemainingDistance();
		double maxSpeed = Math.sqrt(2 * maxAccel * remaining);
		speedSetpoint.reduce(maxSpeed / speedSetpoint.forward);
		
		// command the speed
		robotSpeedOutput.setSpeed(speedSetpoint, maxAccel);
		
		
		counter = (counter + 1) % 50;
		if (counter == 0) {
			Waypoint closest = path.getClosestPoint();
			logger.log(Level.INFO, String.format("Cmd: %7.3f %7.3f Robot: %7.3f %7.3f %7.3f Path: %7.3f %7.3f Remaining: %7.3f Lookahead Point %7.3f %7.3f", 
					speedSetpoint.forward, speedSetpoint.rotational,
					pose.x, pose.y, pose.heading, 
					closest.x, closest.y, remaining,
					lookAheadPoint.x, lookAheadPoint.y));
		}
	}

	int counter = 0;

	/**
	 * Rejoin the path with a curve tangent to the current heading to
	 * the look ahead point.
	 * 
	 * @return curvature ( = 1/radius ) 
	 */
	private double getJoinCurvature(Pose2D pose, Waypoint lookAheadPoint) {
		
		double dx = lookAheadPoint.x - pose.x;
		double dy = lookAheadPoint.y - pose.y;
		double x = dy * Math.cos(pose.heading) - dx * Math.sin(pose.heading);
		double curvature = 2.0 * x / (dx*dx+dy*dy);
		
		return curvature;
	}


	/**
	 * Returns true if all path segments have completed.
	 */
	public boolean isComplete() {
		return complete;
	}

	public void end() {
		timer.cancel();
	}
	

}
