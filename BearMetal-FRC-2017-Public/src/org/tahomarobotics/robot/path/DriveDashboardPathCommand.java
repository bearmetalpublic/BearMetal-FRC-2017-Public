package org.tahomarobotics.robot.path;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.usfirst.frc.team2046.robot.chassis.Chassis;

import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 */
public class DriveDashboardPathCommand extends Command {
	
	private static final Logger logger = Logger.getLogger(DriveDashboardPathCommand.class.getSimpleName());

    public DriveDashboardPathCommand() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    	requires(Chassis.getInstance());
    	SmartDashboard.putString("Robot Path", "Spline{}");
    	SmartDashboard.putNumber("Robot Path Speed", 0.2);
    	SmartDashboard.putBoolean("Robot Path Reverse", false);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	String pathdata = SmartDashboard.getString("Robot Path", "Spline{}");
    	SplinePath p = new SplinePath();
    	p.deserialize(pathdata, SmartDashboard.getNumber("Robot Path Speed", 0.2));
    	if (p.getWaypoints().size() < 2) {
    		logger.log(Level.WARNING, "Path needs to have more than one point!");
    	}
    	else {
	    	Scheduler.getInstance().add(new FollowPathCommand("Dashboard Path", p, SmartDashboard.getBoolean("Robot Path Reverse", false), true, FollowPathCommand.LOOKAHEAD_DISTANCE, FollowPathCommand.NO_TIMEOUT));
    	}
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    	
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return true;
    }

    // Called once after isFinished returns true
    protected void end() {
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
