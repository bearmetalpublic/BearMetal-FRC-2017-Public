/**
 * Copyright 2017 Tahoma Robotics - http://tahomarobotics.org - Bear Metal 2046 FRC Team
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without 
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the 
 * Software, and to permit persons to whom the Software is furnished to do so, subject to the following 
 * conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package org.tahomarobotics.robot.path;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;

import org.tahomarobotics.robot.path.RobotPoseIF.Pose2D;


public class Path {

	private static final Logger logger = Logger.getLogger(Path.class.getSimpleName());
	
	private final LinkedList<Segment> segments = new LinkedList<>();

	public interface CompletionListener {
		void onCompletion();
	}

	public static class Waypoint {

		private final List<CompletionListener> listeners = new ArrayList<>();

		public double x;
		public double y;
		public double speed;

		public Waypoint() {	
		}
		
		public Waypoint(double x, double y, double speed) {
			this(x, y, speed, null);
		}
		
		public Waypoint(double x, double y, double speed, CompletionListener listener) {
			this.x = x;
			this.y = y;
			this.speed = Math.signum(speed) * Math.min(Math.abs(speed), FollowPathCommand.MAX_VELOCITY);
			if (listener != null) {
				this.addCompletionListener(listener);
			}
		}
		
		public void addCompletionListener(CompletionListener listener) {
			listeners.add(listener);
		}

		protected void fireCaptureEvent() {
			for (CompletionListener listener : listeners) {
				listener.onCompletion();
			}
		}

		protected Waypoint mult(double a) {
			return new Waypoint(x*a, y*a, speed);
		}

		protected Waypoint add(Waypoint other) {
			return new Waypoint(x+other.x,y+other.y,speed);
		}

		protected Waypoint div(double a) {
			return new Waypoint(x/a, y/a, speed);
		}

		@Override
		public String toString() {
			return String.format("%6.3f %6.3f %6.3f", x, y, speed);
		}
		
		
	}

	private final List<Waypoint> waypoints = new ArrayList<>();

	private final Waypoint closestPoint = new Waypoint();
	
	public Path add(Waypoint waypoint) {
		addWaypoint(waypoint);
		return this;
	}
	
	public void addWaypoint(Waypoint waypoint) {
		waypoints.add(waypoint);
	}
	
	public void addWaypoints(Collection<Waypoint> waypoints) {
		this.waypoints.addAll(waypoints);
	}

	public List<Waypoint> getWaypoints() {
		return new ArrayList<>(waypoints);
	}
	
	public Path getReverse(){
		List<Waypoint> waypoints = getWaypoints();
		
		Path newPath = new Path();
		for (int i = waypoints.size() - 1; i >= 0; i--){
			newPath.addWaypoint(waypoints.get(i));
		}
		
		return newPath;
	}
	
	public Path getMirrorX(double xOffset){
		List<Waypoint> waypoints = getWaypoints();
		
		Path newPath = new SplinePath();
		for (Waypoint waypoint : waypoints){
			double newX = xOffset - (waypoint.x - xOffset);
			newPath.addWaypoint(new Waypoint(newX, waypoint.y, waypoint.speed));
		}
		
		return newPath;
	}
	
	public Path getMirrorY(double yOffset){
		List<Waypoint> waypoints = getWaypoints();
		
		Path newPath = new SplinePath();
		for (Waypoint waypoint : waypoints){
			double newY = yOffset - (waypoint.y - yOffset);
			newPath.addWaypoint(new Waypoint(waypoint.x, newY, waypoint.speed));
		}
		
		return newPath;
	}
	
	public double getXCenter(){
		return Math.abs(waypoints.get(0).x - waypoints.get(waypoints.size()).x);
	}
	
	public double getYCenter(){
		return Math.abs(waypoints.get(0).y - waypoints.get(waypoints.size()).y);
	}

	private class Segment {
		private final Waypoint start;
		private final Waypoint end;

		private double progress = 0;
		private boolean first = true;
		private boolean complete = false;
		private final double dx;
		private final double dy;
		private final double lengthSquared;
		private final double length;
		private final Waypoint closest = new Waypoint();
		private double pathError = 0;

		public Segment(Waypoint start, Waypoint end) {
			this.start = start;
			this.end = end;
			dx = end.x - start.x;
			dy = end.y - start.y;
			lengthSquared = dx * dx + dy * dy;
			length = Math.sqrt(lengthSquared);
		}

		protected Waypoint update(Pose2D currentPosition) {

			// determine closest path location
			double dx = currentPosition.x - start.x;
			double dy = currentPosition.y - start.y;
			
			double calculatedProgress = (this.dx * dx + this.dy * dy) / lengthSquared;
			
			// check for negative progress
			if (!first && calculatedProgress < progress) {
				logger.warning("Progress is slipping, current segment deleted");
				complete = true;
				end.fireCaptureEvent();
				return closest;
			}
			first = false;
			progress = calculatedProgress;
			
			calculatedProgress = Math.max(0.0, calculatedProgress);
			closest.x = start.x + calculatedProgress * this.dx;
			closest.y = start.y + calculatedProgress * this.dy;

			// determine distance to path
			double xerror = closest.x - currentPosition.x;
			double yerror = closest.y - currentPosition.y;
			pathError = Math.sqrt(xerror * xerror + yerror * yerror);

			// check for waypoint capture/completion
			if (calculatedProgress >= 1.0) {
				complete = true;
				end.fireCaptureEvent();
			}
			return closest;
		}

		public boolean isComplete() {
			return complete;
		}

		public double getPathError() {
			return pathError;
		}

		public double getRemainingLength() {
			return (1.0 - progress) * length;
		}

		public Waypoint getPoint(double distance) {
			double portion = progress + distance / length;

			Waypoint pt = new Waypoint();
			pt.x = start.x + portion * this.dx;
			pt.y = start.y + portion * this.dy;
			pt.speed = end.speed;

			return pt;
		}
	}

	protected void start(CompletionListener listener) {
		start(listener, getWaypoints());
	}

	protected void start(CompletionListener listener, List<Waypoint> waypoints) {
		// create the segments from the list of waypoints
		Waypoint prev = null;
		for(Waypoint next : waypoints) {
			if (prev != null) {
				segments.add(new Segment(prev, next));
//				logger.log(Level.INFO, prev.toString());
			}
			prev = next;
		}
//		logger.log(Level.INFO, prev.toString());
		prev.addCompletionListener(listener);
	}

	/**
	 * Calculate a new look ahead waypoint which is on the path positioned the
	 * provided distance from the current location.
	 * 
	 */
	Waypoint getLookAheadPoint(double lookAheadDistance) {

		Segment segment = null;

		ListIterator<Segment> iter = segments.listIterator();
		while (iter.hasNext()) {
			segment = iter.next();

			double remainingDistance = segment.getRemainingLength();

			// current segment long enough
			if (lookAheadDistance < remainingDistance) {
				break;
			}

			// subtract remaining distance of segment and
			// get the additional from the next segment
			if (iter.hasNext()){
				lookAheadDistance -= remainingDistance;
			}
		}

		// return look ahead point
		return segment == null ? new Waypoint() : segment.getPoint(lookAheadDistance);
	}

	/**
	 * Update the progress of the current path segment. Advance to the next path
	 * segment if current path is complete. Return the cross track error.
	 * 
	 * @param currentPosition
	 * @return distance from path
	 */
	protected double update(Pose2D currentPosition) {

		ListIterator<Segment> iter = segments.listIterator();
		while (iter.hasNext()) {

			Segment segment = iter.next();
			Waypoint closest = segment.update(currentPosition);
			this.closestPoint.x = closest.x;
			this.closestPoint.y = closest.y;
			this.closestPoint.speed = closest.speed;
			
			if (segment.isComplete()) {
				//System.out.println("removing size=" + segments.size());
				iter.remove();

				// loop around to test the next segment
				continue;
			}

			return segment.getPathError();
		}

		return 0;
	}

	protected Waypoint getClosestPoint() {
		return closestPoint;
	}
	
	/**
	 * Cycle through the path segments, adding up the segments remaining
	 * lengths.
	 */
	double getRemainingDistance() {
		double distance = 0;
		ListIterator<Segment> iter = segments.listIterator();
		while (iter.hasNext()) {
			distance += iter.next().getRemainingLength();
		}
		return distance;
	}
	
	public String serialize() {
		// The output should look something like "Spline{Waypoint{0,1},Waypoint{8,15.36520}}"
		String result = "Spline{";
		for (Waypoint w : getWaypoints()) {
			result += "Waypoint{" + w.x + "," + w.y + "}";
		}
		return result + "}";
	}
	
}
